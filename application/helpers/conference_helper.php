<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
{
	function get_conference_details($country){
		$CI =& get_instance();
	    $CI->db->select('a.*,b.last_date');
	    $CI->db->select("DATE_FORMAT(a.start_date, '%d') AS start ", FALSE);
	    $CI->db->select("DATE_FORMAT(a.end_date, '%d %M,%Y') AS end ", FALSE);
	    $CI->db->select("DATE_FORMAT(b.last_date, '%d %M,%Y') AS last ", FALSE);
	    
	    $CI->db->from('conference a');
	    $CI->db->join('conference_important_dates b','a.id = b.conference_id','left');
	    $CI->db->where('a.country',$country);
	    $result = $CI->db->get();
	    $row = $result->result();
	    return $row;
	}
	function get_conference_logo($id){
		$CI =& get_instance();
	    $CI->db->select('*');
	    $CI->db->from('conference_logo');
	    $CI->db->where('conference_id',$id);
	    $CI->db->limit('2');
	    $result = $CI->db->get();
	    $row = $result->result();
	    return $row;
	}
	
}

?>