<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	Class Manage_role extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/Roles_model');
			if(empty($_SESSION['admin_logged_in']['user_id'])){
				redirect('admin'); 
			}
		}

		public function index(){

          	$data['roles'] = $this->Roles_model->get_all_roles();
          	 
			$this->load->view('admin/common/top_header'); 
	        $this->load->view('admin/common/header');
	        $this->load->view('admin/common/leftbar');
	        $this->load->view('admin/manage_role/index',$data);
	        $this->load->view('admin/common/footer');  
            
		}
	/*
     * Adding a new role
     */
		function add()
    {   
    	//echo "string";die();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('role','Role','required');
        
        if($this->form_validation->run())     
        {   
            $params = array(
                'role' => $this->input->post('role'),
                'created_at' => Date('y-m-d h:i:s'),
                'updated_at' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $role_id = $this->Roles_model->add_role($params);
            redirect('admin/manage_role/index');
        }
        else
        {            
            //$data['_view'] = 'roles/add';
            $this->load->view('admin/common/top_header'); 
	        $this->load->view('admin/common/header');
	        $this->load->view('admin/common/leftbar');

            $this->load->view('admin/manage_role/add');
            $this->load->view('admin/common/footer'); 
        }
    }


    /*
     * Editing a role
     */
   function edit($id)
    {   
        // check if the role exists before trying to edit it
        $data['role'] = $this->Roles_model->get_role($id);
        
        if(isset($data['role']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('role','Role','required');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'role' => $this->input->post('role'),
					'updated_at' => Date('y-m-d h:i:s')
                );

                $this->Roles_model->update_role($id,$params);            
                redirect('admin/manage_role/index');
            }
            else
            { 
	                //$data['_view'] = 'role/edit';
            	$this->load->view('admin/common/top_header'); 
		        $this->load->view('admin/common/header');
		        $this->load->view('admin/common/leftbar');
                $this->load->view('admin/manage_role/edit',$data);
                $this->load->view('admin/common/footer');
            }

	            
        }
        else
            show_error('The role you are trying to edit does not exist.');
    } 

     /*
     * Deleting role
     */
    function remove($id)
    {
        $role = $this->Roles_model->get_role($id);

        // check if the role exists before trying to delete it
        if(isset($role['id']))
        {
            $this->Roles_model->delete_role($id);
            redirect('admin/manage_role/index');
        }
        else
            show_error('The role you are trying to delete does not exist.');
    }

	}
?>