<?php
class Training_models extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Training_model');
        if(empty($_SESSION['admin_logged_in']['user_id'])){
                redirect('admin'); 
        }
    } 

    /*
     * Listing of training_model
     */
    function index()
    {
        $data['training_model'] = $this->Training_model->get_all_training();
        //print_r($data['training_model']);die();

        $this->load->view('admin/common/top_header'); 
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/leftbar');
        $this->load->view('admin/common/footer');
        $this->load->view('admin/training_model/index',$data);
    }

     /*
     * Adding a new training_model
     */
    function add()
    {   
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('discription','Discription','required');
        $this->form_validation->set_rules('no_question','No_question','required');
        $this->form_validation->set_rules('start_date','Start_date','required');
        $this->form_validation->set_rules('dead_line','Dead_line','required');
        $this->form_validation->set_rules('assign_by','Assign_by','required');
        $this->form_validation->set_rules('department','Department','required');
        $this->form_validation->set_rules('designation','Designation','required');
        $this->form_validation->set_rules('total_marks','Total_marks','required');
       
        if($this->form_validation->run()){
            $params = array(
                'title' => $this->input->post('title'),
                'discription' => $this->input->post('discription'),
                'no_question' => $this->input->post('no_question'),
                'start_date' => $this->input->post('start_date'),
                'dead_line' => $this->input->post('dead_line'),
                'assign_by' => $this->input->post('assign_by'),
                'department' => $this->input->post('department'),
                'designation' => $this->input->post('designation'),
                'status' => 0,
                'total_marks' => $this->input->post('total_marks'),
                'created_at' => Date('y-m-d h:i:s'),
                'updated_on' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $training_details = $this->Training_model->add_training($params);
            redirect('admin/training_models');
        }
        else
        {            
            $data['coordinater']=$this->Training_model->get_all_coordinater();

            $this->load->view('admin/common/top_header'); 
            $this->load->view('admin/common/header');
            $this->load->view('admin/common/leftbar');
            $this->load->view('admin/common/footer'); 
            $this->load->view('admin/training_model/add',$data);
        }
    }

    function edit($id){

        $data['training_model'] = $this->Training_model->get_training($id);
        
        //$data['assign_by'] = $this->Training_model->get_assignby($id);
        
        if(isset($data['training_model']['id']))
        {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('discription','Discription','required');
        $this->form_validation->set_rules('no_question','No_question','required');
        $this->form_validation->set_rules('start_date','Start_date','required');
        $this->form_validation->set_rules('dead_line','Dead_line','required');
        $this->form_validation->set_rules('assign_by','Assign_by','required');
        $this->form_validation->set_rules('department','Department','required');
        $this->form_validation->set_rules('designation','Designation','required');
        $this->form_validation->set_rules('total_marks','Total_marks','required');
       
        
        if($this->form_validation->run()){
            $params = array(
                'title' => $this->input->post('title'),
                'discription' => $this->input->post('discription'),
                'no_question' => $this->input->post('no_question'),
                'start_date' => $this->input->post('start_date'),
                'dead_line' => $this->input->post('dead_line'),
                'assign_by' => $this->input->post('assign_by'),
                'department' => $this->input->post('department'),
                'designation' => $this->input->post('designation'),
                'status' => 0,
                'total_marks' => $this->input->post('total_marks'),
                'created_at' => Date('y-m-d h:i:s'),
                'updated_on' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $this->Training_model->update_training($id,$params);  
            redirect('admin/training_models');
        }
        else
        {            
            $data['coordinater']=$this->Training_model->get_all_coordinater();
            //print_r($data['coordinater']);die();
            $this->load->view('admin/common/top_header'); 
            $this->load->view('admin/common/header');
            $this->load->view('admin/common/leftbar');
            $this->load->view('admin/common/footer'); 
            $this->load->view('admin/training_model/edit',$data);
        }
        }
        else
            show_error('The training model you are trying to edit does not exist.');
    }

    /*
     * Deleting training_model
     */
    function delete($id)
    {
        $training_model = $this->Training_model->get_training($id);

        // check if the training_model exists before trying to delete it
        if(isset($training_model['id']))
        {
            $this->Training_model->delete_training($id);
            redirect('admin/training_models');
        }
        else
            show_error('The training_model you are trying to delete does not exist.');
    }

}