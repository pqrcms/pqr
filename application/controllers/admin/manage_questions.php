<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	Class Manage_questions extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/Manage_questions_model');
			if(empty($_SESSION['admin_logged_in']['user_id'])){
				redirect('admin'); 
			}
		}

		public function index(){
		//$this->load->view('types',$data);
		//$data['title']= 'Warehouse - Delivery';
        $data['manage_questions'] = $this->Manage_questions_model->get_All_Questions();
        //print_r($data);die();
		$this->load->view('admin/common/top_header'); 
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/leftbar');
        $this->load->view('admin/common/footer');
        $this->load->view('admin/manage_questions/index',$data);
            
  
		}

	/*
     * Adding a new manage_question
    */
    function add()
    {   
         
        	$data['types']=$this->Manage_questions_model->get_type();
        	//print_r($data);die();
            $data['training_model']=$this->Manage_questions_model->get_tasks();
            //print_r($data['training_model']);die();
            $this->load->view('admin/common/top_header'); 
	        $this->load->view('admin/common/header');
	        $this->load->view('admin/common/leftbar');
	        $this->load->view('admin/manage_questions/add',$data);
            $this->load->view('admin/common/footer'); 
            
       // }
    }

    function save_questions()
    {
        //print_r($_POST);die();
        $type = $this->input->post('type');
        if($type == 1)
        {
            $params = array(
             'type' => $this->input->post('type'),
             'task_id' => $this->input->post('task_id'),
             'question' => $this->input->post('question'),
             'option_one' => $this->input->post('option_one'),
             'option_two' => $this->input->post('option_two'),
             'option_three' => $this->input->post('option_three'),
             'option_four' => $this->input->post('option_four'),
             'answer' => $this->input->post('answer'),
             'marks' => $this->input->post('marks'),
             'created_at' => Date('y-m-d h:i:s'),
             'updated_on' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $manage_question = $this->Manage_questions_model->add_manage_question($params);
            //print_r($manage_question);die();
            if($manage_question)
            {
                echo "200";die();
            }else{
                echo "100";die();
            }
        }else{
            $params = array(
             'type' => $this->input->post('type'),
             'task_id' => $this->input->post('task'),
             'question' => $this->input->post('question'),
             'marks' => $this->input->post('mark'),
             'created_at' => Date('y-m-d h:i:s'),
             'updated_on' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $manage_question = $this->Manage_questions_model->add_manage_question($params);
            if($manage_question)
            {
                echo "200";die();
            }else{
                echo "100";die();
            }
        }  
    }

    /*
     * Editing a manage_question
     */
    function edit($id)
    {   
        // check if the manage_question exists before trying to edit it
        $data['manage_questions'] = $this->Manage_questions_model->get_manage_question($id);
        
        //print_r($data['manage_questions']);die();
        if(isset($data['manage_questions']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('type','Type','required');
            $this->form_validation->set_rules('question','Question','required');
            if($this->input->post('type') == 1)
            {
                $this->form_validation->set_rules('option_one','Option_one','required');
                $this->form_validation->set_rules('option_two','option_two','required');
                $this->form_validation->set_rules('option_three','option_three','required');
                $this->form_validation->set_rules('option_four','option_four','required');
                $this->form_validation->set_rules('answer','Answer','required');
                $this->form_validation->set_rules('marks','Marks','required');

            }
            //else{
            //     $this->form_validation->set_rules('essay','Essay','required');
            // }
			
			if($this->form_validation->run())     
            { 
                //echo $id;
                //print_r($_POST);die();  
                if(($this->input->post('type')) == 1){
                    $params = array(
                     'type' => $this->input->post('type'),
                     'question' => $this->input->post('question'),
                     'option_one' => $this->input->post('option_one'),
                     'option_two' => $this->input->post('option_two'),
                     'option_three' => $this->input->post('option_three'),
                     'option_four' => $this->input->post('option_four'),
                     'answer' => $this->input->post('answer'),
                     'marks' => $this->input->post('marks'),
                ); 
                $this->Manage_questions_model->update_manage_question($id,$params);            
                redirect('admin/manage_questions/index');
                }else{
                    $params = array(
                    'type' => $this->input->post('type'),
                    'question' => $this->input->post('question'),
                    'marks' => $this->input->post('marks'),
                    'updated_on' => Date('y-m-d h:i:s'),
                    ); 
                //print_r($params);die();
                $this->Manage_questions_model->update_manage_question($id,$params);            
                redirect('admin/manage_questions/index');
                }
            }
            else
            {
            	$data['types']=$this->Manage_questions_model->get_type();
                //$data['_view'] = 'manage_question/edit';
                $this->load->view('admin/common/top_header'); 
		        $this->load->view('admin/common/header');
		        $this->load->view('admin/common/leftbar');
                //$this->load->view('admin/manage_role/edit',$data);
                $this->load->view('admin/manage_questions/edit',$data);
                $this->load->view('admin/common/footer');
                
            }
        }
        else
            show_error('The manage_question you are trying to edit does not exist.');
    }

    /*
     * Deleting manage_question
     */
    function remove($id)
    {
        $manage_question = $this->Manage_questions_model->get_manage_question($id);

        // check if the manage_question exists before trying to delete it
        if(isset($manage_question['id']))
        {
            $this->Manage_questions_model->delete_manage_question($id);
            redirect('admin/manage_questions/index');
        }
        else
            show_error('The manage_question you are trying to delete does not exist.');
    }   

	}
?>