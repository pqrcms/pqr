<?php
class Manage_feedback extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Manage_feedback_model');
        if(empty($_SESSION['admin_logged_in']['user_id'])){
                redirect('admin'); 
        }
    } 
    /*
     * Listing of manage feedback
     */
    function index()
    {
        $data['emp_feedback'] = $this->Manage_feedback_model->get_all_feedback();
        
        $this->load->view('admin/common/top_header'); 
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/leftbar');
        $this->load->view('admin/common/footer');
        $this->load->view('admin/manage_feedback/index',$data);
    }
}