<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
//error_reporting(0);
class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->view('admin/common/top_header');
		$this->load->view('admin/common/header');
	    $this->load->view('admin/common/leftbar');
	    if(empty($_SESSION['admin_logged_in']['user_id'])){
				redirect('admin'); 
		}
	}

	public function index()
	  {
	    $data['user'] = $this->User_model->get_all_users(); 
	    $this->load->view('admin/user/user',$data);
	    $this->load->view('admin/common/footer');
	  }

	public function user_status()
	{
		$id =  $this->uri->segment(4);  
		$status =  $this->uri->segment(5); 	
		$this->User_model->update_status($id,$status);
		$user_det = $this->User_model->get_userById($id);
		$to = $user_det->user_email;
          $name = $user_det->user_name;
           $order = array('email' => $email,'name' => $name);
           $message = $this -> load -> view('email/register_email',$order,TRUE);
           $this ->users_profile_model->send_mail('info@alumini.com',$to,'Register',$message);
		redirect('admin/user/list'); 
	}	

	public function view_user_profile(){
		$id =  $this->uri->segment(4);  
		$data['user'] = $this->User_model->get_userById($id);
		$this->load->view('admin/user/user_view',$data);
	    $this->load->view('admin/common/footer');
	}

	}