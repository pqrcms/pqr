<?php
//error_reporting(0);
class Emp_details extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Emp_details_model');
        $this->load->library('excel');//load PHPExcel library 
        if(empty($_SESSION['admin_logged_in']['user_id'])){
                redirect('admin'); 
        }
    } 

    /*
     * Listing of emp_details
     */
    function index()
    {
        $data['emp_details'] = $this->Emp_details_model->get_all_emp_details();
        //print_r($data);die();
        //$data['_view'] = 'emp_detail/index';
        $this->load->view('admin/common/top_header'); 
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/leftbar');
        $this->load->view('admin/common/footer');
        $this->load->view('admin/emp_details/index',$data);
    }

    /*
     * Adding a new emp_detail
     */
    function add()
    {   
        //print_r($_POST);die();
        $this->load->library('form_validation');

		$this->form_validation->set_rules('emp_name','Emp Name','required|alpha');
        $this->form_validation->set_rules('emp_last','Emp Last','required|alpha');
        $this->form_validation->set_rules('address','Address','required');
        $this->form_validation->set_rules('city','City','required|alpha');
        $this->form_validation->set_rules('state','State','required');
        $this->form_validation->set_rules('zip','Zip','required|min_length[6]');
        $this->form_validation->set_rules('department','Department','required|alpha');
        $this->form_validation->set_rules('position','Position','required');
        $this->form_validation->set_rules('doj','Doj','required');
        $this->form_validation->set_rules('employee_id','Employee_id','required');
        $this->form_validation->set_rules('dob','Dob','required');
        $this->form_validation->set_rules('phone_num','Phone num','required|min_length[10]|numeric');       
		$this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('password','Password','required|min_length[8]|alpha_numeric');
        $this->form_validation->set_rules('skills','Skills','required');
		
		if($this->form_validation->run()){
            $params = array(
				'emp_name' => $this->input->post('emp_name'),
                'emp_last' => $this->input->post('emp_last'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'zip' => $this->input->post('zip'),
                'department' => $this->input->post('department'),
                'position' => $this->input->post('position'),
                'doj' => $this->input->post('doj'),
                'employee_id' => $this->input->post('employee_id'),
                'dob' => $this->input->post('dob'),
                'phone_num' => $this->input->post('phone_num'),
				'email' => $this->input->post('email'),
                'password' => MD5($this->input->post('password')),
				'skills' => $this->input->post('skills'),
                'status' => 0,
                'created_at' => Date('y-m-d h:i:s'),
                'updated_at' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $emp_detail_id = $this->Emp_details_model->add_emp_detail($params);
            redirect('admin/Emp_details/index');
        }
        else
        {            
            //$data['_view'] = 'emp_detail/add';
            $this->load->view('admin/common/top_header'); 
            $this->load->view('admin/common/header');
            $this->load->view('admin/common/leftbar');
            $this->load->view('admin/common/footer'); 
            $this->load->view('admin/Emp_details/add');
        }
    }  

    /*
     * Editing a emp_detail
     */
    function edit($emp_id)
    {   
        // check if the emp_detail exists before trying to edit it
        $data['emp_details'] = $this->Emp_details_model->get_emp_detail($emp_id);
        //print_r($data);die();

        if(isset($data['emp_details']['emp_id']))
        {
        $this->load->library('form_validation');

		$this->form_validation->set_rules('emp_name','Emp Name','required|alpha');
        $this->form_validation->set_rules('emp_last','Emp Last','required|alpha');
        $this->form_validation->set_rules('address','Address','required');
        $this->form_validation->set_rules('city','City','required|alpha');
        $this->form_validation->set_rules('state','State','required');
        $this->form_validation->set_rules('zip','Zip','required|min_length[6]');
        $this->form_validation->set_rules('department','Department','required|alpha');
        $this->form_validation->set_rules('position','Position','required');
        $this->form_validation->set_rules('doj','Doj','required');
        $this->form_validation->set_rules('employee_id','employee_id','required');
        $this->form_validation->set_rules('dob','Dob','required');
        $this->form_validation->set_rules('phone_num','Phone num','required|min_length[10]|numeric');       
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('password','Password','required|min_length[8]|alpha_numeric');
        $this->form_validation->set_rules('skills','Skills','required');
		
			if($this->form_validation->run())     
            {   
                $params = array(
				'emp_name' => $this->input->post('emp_name'),
                'emp_last' => $this->input->post('emp_last'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'zip' => $this->input->post('zip'),
                'department' => $this->input->post('department'),
                'position' => $this->input->post('position'),
                'doj' => $this->input->post('doj'),
                'employee_id' => $this->input->post('employee_id'),
                'dob' => $this->input->post('dob'),
                'phone_num' => $this->input->post('phone_num'),
                'email' => $this->input->post('email'),
                'password' => MD5($this->input->post('password')),
                'skills' => $this->input->post('skills'),
                'status' => 0,
                'created_at' => Date('y-m-d h:i:s'),
                'updated_at' => Date('y-m-d h:i:s'),
                );
                $this->Emp_details_model->update_emp_detail($emp_id,$params);            
                redirect('admin/emp_details/index');
            }
            else
            {
                //$data['_view'] = 'emp_detail/edit';
                $this->load->view('admin/common/top_header'); 
                $this->load->view('admin/common/header');
                $this->load->view('admin/common/leftbar');
                $this->load->view('admin/common/footer'); 
                $this->load->view('admin/emp_details/edit',$data);
            }
        }
        else
            show_error('The emp_detail you are trying to edit does not exist.');
    } 

    /*
     * Deleting emp_detail
     */
    function remove($emp_id)
    {
        $emp_details = $this->Emp_details_model->get_emp_detail($emp_id);

        // check if the emp_detail exists before trying to delete it
        if(isset($emp_details['emp_id']))
        {
            $this->Emp_details_model->delete_emp_detail($emp_id);
            redirect('admin/emp_details/index');
        }
        else
            show_error('The emp detail you are trying to delete does not exist.');
    }
    
//add balk employee
    public function add_file(){
        $this->load->view('admin/common/top_header'); 
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/leftbar');
        $this->load->view('admin/common/footer'); 
        $this->load->view('admin/emp_details/add_file');
    }
/**
 * This function deactivate voting then redirect to votes_list
 * @example id=1
 * @param integer $id
 */
    function remove_list($id)
    {
        $this->Emp_details_model->deactivate($id);
        //$this->session->set_flashdata('success_msg', '1 new category deactivated!');
        redirect('admin/emp_details/index','refresh');
    }

    public function import()
        {
            //print_r($_FILES);die();
          if(isset($_POST["Import"]))
            {
                $filename=$_FILES["upload"]["tmp_name"];
                if($_FILES["upload"]["size"] > 0)
                  {
                    $file = fopen($filename, "r");
                    $i=0;
                     while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                     {
                        if($i!=0){
                        //echo "<pre>";print_r($emapData);echo "</pre>";die();
                            $data = array(
                                'emp_name' => $emapData[1],
                                'emp_last' => $emapData[2],
                                'address' => $emapData[3],
                                'city' => $emapData[4],
                                'state' => $emapData[5],
                                'zip' => $emapData[6],
                                'department' => $emapData[7],
                                'position' => $emapData[8],
                                'doj' => date('Y-m-d H:i',strtotime($emapData[9])),
                                'employee_id' => $emapData[10],
                                'dob' => date('Y-m-d H:i',strtotime($emapData[11])),
                                'phone_num' => $emapData[12],
                                'email' => $emapData[13],
                                'password' => MD5($emapData[14]),
                                'skills' => $emapData[15],
                                'created_at' => Date('y-m-d h:i:s'),
                                'updated_at' => Date('y-m-d h:i:s'),
                                );
                            //print_r($data);die();
                            $this->load->model('admin/Emp_details_model');
                            $insertId = $this->Emp_details_model->insertCSV($data);
                            }
                            $i++;
                     }
                    redirect('admin/emp_details/index');
                  }
            }
        }

    //Upload EXcel File
//     public function import(){
//     //print_r($_FILES);die();
//     $file = $_FILES['upload']['tmp_name'];
//     //load the excel library
//     $this->load->library('excel');
//     //$xmll=file_get_contents($file);
//     //read file from path
//     $objPHPExcel = PHPExcel_IOFactory::load($file);
//     //print_r($objPHPExcel);die();
//     //get only the Cell Collection
//     $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
//     //extract to a PHP readable array format
//     foreach ($cell_collection as $cell) {
//      $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
//      $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
//      $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//     $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
//     $arrayCount = count($allDataInSheet);
//     //print_r($allDataInSheet);die();
//     // foreach ($allDataInSheet as $a) {
//     //     print_r($a);
//     // }
//     // die();
//     for($i=2;$i<=$arrayCount;$i++)
//     {
//     $data = array(
//         'emp_name' => $allDataInSheet[$i]["B"],
//         'emp_last' => $allDataInSheet[$i]["C"],
//         'address' => $allDataInSheet[$i]["D"],
//         'city' => $allDataInSheet[$i]["E"],
//         'state' => $allDataInSheet[$i]["F"],
//         'zip' => $allDataInSheet[$i]["G"],
//         'department' => $allDataInSheet[$i]["H"],
//         'position' => $allDataInSheet[$i]["I"],
//         'doj' => $allDataInSheet[$i]["J"],
//         'employee_id' => $allDataInSheet[$i]["K"],
//         'dob' => $allDataInSheet[$i]["L"],
//         'phone_num' => $allDataInSheet[$i]["M"],
//         'email' => $allDataInSheet[$i]["N"],
//         'password' => $allDataInSheet[$i]["O"],
//         'skills' => $allDataInSheet[$i]["P"],
//         );
//     //print_r($data);die();
//     $this->load->model('admin/Emp_details_model');
//     $insertId = $this->Emp_details_model->insertCSV($data);
//     }
//     redirect('admin/emp_detail/index');
// }
// }

    // public function import(){
    // //Path of files were you want to upload on localhost (C:/xampp/htdocs/ProjectName/uploads/excel/) 
    //      //print_r($_FILES);die();
    //      $configUpload['upload_path'] = FCPATH.'uploads/excel/';
    //      $configUpload['allowed_types'] = 'xls|xlsx|csv';
    //      $configUpload['max_size'] = '5000';
         
    //      $this->load->library('upload', $configUpload);
    //      $this->upload->do_upload('userfile');
         
    //      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
    //      $file_name = $upload_data['file_name']; //uploded file name
    //      $extension=$upload_data['file_ext'];    // uploded file extension
    //      //print_r($extension);die();
        
    //     //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
    //      $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007     
    //     //Set to read only
    //      $objReader->setReadDataOnly(true);          
    //     //Load excel file
    //      $objPHPExcel=$objReader->load(FCPATH.'uploads/excel/'.$file_name);

    //      $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
    //      $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
    //       //loop from first data untill last data

    //       for($i=2;$i<=$totalrows;$i++)
    //       {
    //           $emp_name = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();           
    //           $emp_last = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); //Excel Column 1
    //           $address = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 2
    //           $city = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue(); //Excel Column 3
    //           $state = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); //Excel Column 4
    //           $zip = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
    //           $department = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
    //           $position = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
    //           $doj = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
    //           $employee_id = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
    //           $dob = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
    //           $phone_num = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
    //           $email = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
    //           $password = $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
    //           $skills = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
              
    //           $data_user=array(
    //             'emp_name' => $emp_name,
    //             'emp_last' => $emp_last,
    //             'address' => $address,
    //             'city' => $city,
    //             'state' => $state,
    //             'zip' => $zip,
    //             'department' => $department,
    //             'position' => $position,
    //             'doj' => $doj,
    //             'employee_id' => $employee_id,
    //             'dob' => $dob,
    //             'phone_num' => $phone_num,
    //             'email' => $email,
    //             'password' => $password,
    //             'skills' => $skills,
    //         );
    //           print_r($data_user);die();
    //           //$this->load->model('admin/Emp_details_model');
    //           $this->Emp_details_model->insertCSV($data_user);
    //     }
    //         //fclose($file);
    //         //unlink('././uploads/excel/'.$file_name); //File Deleted After uploading in database .
    //         redirect('admin/emp_detail/index');
    //  }

}