<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_multiple_essay extends CI_Controller {

    public function __construct(){
            parent::__construct();
            $this->load->model('admin/Add_multiple_essay_model');
            if(empty($_SESSION['admin_logged_in']['user_id'])){
                redirect('admin'); 
            }
    }

    public function index()
        {
            $this->load->view('admin/common/top_header'); 
            $this->load->view('admin/common/header');
            $this->load->view('admin/common/leftbar');
            $this->load->view('admin/common/footer'); 
            $this->load->view('admin/add_multiple_essay/index');
            
        }

    public function import()
        {
            //print_r($_FILES);die();
          if(isset($_POST["Import"]))
            {
                $filename=$_FILES["file"]["tmp_name"];
                if($_FILES["file"]["size"] > 0)
                  {
                    $file = fopen($filename, "r");
                     while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                     {
                        //echo "<pre>";print_r($emapData);echo "</pre>";die();
                            $data = array(
                                'type' => 2,
                                'question' => $emapData[0],
                                'essay' => $emapData[1],
                                );
                        $this->load->model('admin/Add_multiple_essay_model');
                       $insertId = $this->Add_multiple_essay_model->insertCSV($data);
                     }
                    fclose($file);
                    redirect('admin/add_multiple_essay/index');
                  }
            }
        }
}