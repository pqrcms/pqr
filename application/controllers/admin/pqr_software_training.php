<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	Class Pqr_software_training extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/Pqr_software_training_model');
			if(empty($_SESSION['admin_logged_in']['user_id'])){
				redirect('admin'); 
			}
		}

		public function index(){

		//$data['title']= 'Warehouse - Delivery';
        $data['type'] = $this->Pqr_software_training_model->get_All_Traning();

			$this->load->view('admin/common/top_header'); 
	        $this->load->view('admin/common/header');
	        $this->load->view('admin/common/leftbar');
	        $this->load->view('admin/pqr_software_training/index',$data);
	        $this->load->view('admin/common/footer');  
		}


	}
?>