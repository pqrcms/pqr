<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_material extends CI_Controller {

    public function __construct(){
            parent::__construct();
            $this->load->model('admin/Add_material_model');
            $this->load->helper("URL", "DATE", "URI", "FORM");
            $this->load->library('form_validation');
            $this->load->library('uploader');
            if(empty($_SESSION['admin_logged_in']['user_id'])){
                redirect('admin'); 
            }
    }

    public function index()
    {       
            $data['materials'] = $this->Add_material_model->get_All_Materials();
            //print_r($data);die();
            $this->load->view('admin/common/top_header'); 
            $this->load->view('admin/common/header');
            $this->load->view('admin/common/leftbar');
            $this->load->view('admin/common/footer'); 
            $this->load->view('admin/add_material/index',$data);      
    }

    public function add_files()
    {
            $this->load->view('admin/common/top_header'); 
            $this->load->view('admin/common/header');
            $this->load->view('admin/common/leftbar');
            $this->load->view('admin/common/footer'); 
            $this->load->view('admin/add_material/add_files');    
    }

    function do_upload(){
        //$filename = $_FILES['userfile']['name'];
        // $flag = "profile";
        // $materials['user_id'] = "materials";
        // $materials['flag'] = $flag;
        // if(!empty($_FILES['userfile']['name']))
        // {
        //         $target_file = $this->uploader->upload_image($_FILES['userfile'], $flag,$materials);
        //         print_r($target_file);
        //         //echo  $target_file['profile_org_url'];die();
        //        // }
        // }
        // echo "<pre>";print_r($_FILES); echo "</pre>";die();
        if(!empty($_FILES['userfile']['name']))
        {
                $fileName=$_FILES["userfile"]["name"];
                $fileTmpName=$_FILES["userfile"]["tmp_name"];  
                        //New file name
                $random=rand(1111,9999);

                $newFileName=$random.str_replace(' ','', $fileName);

                        //File upload path
                $uploadPath="uploads/".$newFileName;
                if (!file_exists("uploads/")) 
                {
                    mkdir("uploads/", 0755, true);
                }
                //function for upload file
                move_uploaded_file($fileTmpName,$uploadPath);
                // $this->db->where('id',$j_id);
                // $this->db->update('jobs_applied', 
                //     array( 'cv_doc' => $uploadPath ));

                $params = array(
                'name' => $uploadPath,
                'created_at' => Date('y-m-d h:i:s'),
                'updated_at' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $role_id = $this->Add_material_model->add_material($params);
        }
                    fclose($file);
                    redirect('admin/add_material/index'); 
    }

    /*
     * Deleting manage_question
     */
    function remove($id)
    {
        $materials = $this->Add_material_model->get_materials($id);

        // check if the manage_question exists before trying to delete it
        if(isset($materials['id']))
        {
            $this->Add_material_model->delete_materials($id);
            redirect('admin/add_material/index');
        }
        else
            show_error('The materials you are trying to delete does not exist.');
    } 

}