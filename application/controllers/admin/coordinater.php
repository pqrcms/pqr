<?php
class Coordinater extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Coordinater_model');
        if(empty($_SESSION['admin_logged_in']['user_id'])){
                redirect('admin'); 
        }
    } 
    /*
     * Listing of Coordinater
     */
    function index()
    {
        $data['coordinater'] = $this->Coordinater_model->get_all_coordinater();
        $this->load->view('admin/common/top_header'); 
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/leftbar');
        $this->load->view('admin/common/footer');
        $this->load->view('admin/coordinater/index',$data);
    }
    /*
     * Adding a new coordinater
     */
    function add()
    {   
        $this->load->library('form_validation');

        $this->form_validation->set_rules('emp_id','Emp Id','required');
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('email','Email','required');     
        
        if($this->form_validation->run()){
            $params = array(
                'emp_id' => $this->input->post('emp_id'),
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'created_at' => Date('y-m-d h:i:s'),
                'updated_on' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $coordinater = $this->Coordinater_model->add_coordinater($params);
            redirect('admin/coordinater');
        }
        else
        {            
            //$data['coordinater']=$this->Coordinater_model->get_all_coordinater();
            $this->load->view('admin/common/top_header'); 
            $this->load->view('admin/common/header');
            $this->load->view('admin/common/leftbar');
            $this->load->view('admin/common/footer'); 
            $this->load->view('admin/coordinater/add');
        }
    }

    /*
     * Editing a coordinater
     */
   function edit($id)
    {   
        // check if the coordinater exists before trying to edit it
        $data['coordinater'] = $this->Coordinater_model->get_coordinater($id);

        if(isset($data['coordinater']['id']))
        {
            $this->load->library('form_validation');

        $this->form_validation->set_rules('emp_id','Emp Id','required');
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('email','Email','required');  
        
            if($this->form_validation->run())     
            {   
                $params = array(
                'emp_id' => $this->input->post('emp_id'),
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'updated_on' => Date('y-m-d h:i:s')
                );

                $this->Coordinater_model->update_coordinater($id,$params);            
                redirect('admin/coordinater');
            }
            else
            { 
                $this->load->view('admin/common/top_header'); 
                $this->load->view('admin/common/header');
                $this->load->view('admin/common/leftbar');
                $this->load->view('admin/coordinater/edit',$data);
                $this->load->view('admin/common/footer');
            }
        }
        else
            show_error('The coordinater you are trying to edit does not exist.');
    } 

    /*
     * Deleting role
     */
    function delete($id)
    {
        $coordinater = $this->Coordinater_model->get_coordinater($id);

        // check if the role exists before trying to delete it
        if(isset($coordinater['id']))
        {
            $this->Coordinater_model->delete_coordinater($id);
            redirect('admin/coordinater');
        }
        else
            show_error('The coordinater you are trying to delete does not exist.');
    }
}