<?php
class Complited_tasks extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Complited_tasks_model');
        if(empty($_SESSION['admin_logged_in']['user_id'])){
                redirect('admin'); 
        }
    } 
    /*
     * Listing of Coordinater
     */
    function index()
    {
        $data['emp_tasks'] = $this->Complited_tasks_model->get_all_tasks();
        
        $this->load->view('admin/common/top_header'); 
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/leftbar');
        $this->load->view('admin/common/footer');
        $this->load->view('admin/complited_tasks/index',$data);
    }
}