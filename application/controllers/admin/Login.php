<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	Class Login extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->view('admin/common/top_header');
			$this->load->model('admin/Login_model');
		}

		public function index(){
			if(!empty($_SESSION['admin_logged_in']['user_id'])){
				redirect('admin/dashboard');
			} else{ 
				$this->load->view('admin/login');
				$this->load->view('admin/common/footer');
			}	
		}

		public function signin_form()
		{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');	
		
		$email_address = $_POST['email'];
		$password = $_POST['password'];

		$valid_check = $this->Login_model->check_credentials($email_address,$password);

			if($valid_check == true)
			{
				$data = array(
				'user_id' => $valid_check->id,
				'email_address' => $valid_check->email,
				'is_logged_in' => true);
			    $this->session->set_userdata('admin_logged_in', $data);
				echo 1; die();
			}
			else
			{
				echo 2; die();
			}
		}

	public function logout()
	{
		$this->session->unset_userdata('admin_logged_in');
		$this->load->view('admin/login');
		$this->load->view('admin/common/footer');
	}	


	}
?>