<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	Class Quiz extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/Quiz_model');
			if(empty($_SESSION['admin_logged_in']['user_id'])){
				redirect('admin'); 
			}
		}

		public function index(){

		//$data['title']= 'Warehouse - Delivery';
        $data['type'] = $this->Quiz_model->get_All_Types();
        
			$this->load->view('admin/common/top_header'); 
	        $this->load->view('admin/common/header');
	        $this->load->view('admin/common/leftbar');
	        $this->load->view('admin/common/footer');  
	        $this->load->view('admin/quiz/index',$data);

		}


	}
?>