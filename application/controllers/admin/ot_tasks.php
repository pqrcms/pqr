<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	Class Ot_tasks extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/Ot_tasks_model');
			if(empty($_SESSION['admin_logged_in']['user_id'])){
				redirect('admin'); 
			}
		}

		public function index(){

          	$data['ot_tasks'] = $this->Ot_tasks_model->get_all_tasks();
          	 
			$this->load->view('admin/common/top_header'); 
	        $this->load->view('admin/common/header');
	        $this->load->view('admin/common/leftbar');
	        $this->load->view('admin/manage_ot_tasks/index',$data);
	        $this->load->view('admin/common/footer');  
            
		}
	/*
     * Adding a new role
     */
		function add()
    {   
    	//echo "string";die();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tasks','tasks','required');
        
        if($this->form_validation->run())     
        {   
            $params = array(
                'tasks' => $this->input->post('tasks'),
                'created_at' => Date('y-m-d h:i:s'),
                'updated_on' => Date('y-m-d h:i:s'),
            );
            //print_r($params);die();
            $tasks_id = $this->Ot_tasks_model->add_tasks($params);
            redirect('admin/ot_tasks/index');
        }
        else
        {            
            //$data['_view'] = 'roles/add';
            $this->load->view('admin/common/top_header'); 
	        $this->load->view('admin/common/header');
	        $this->load->view('admin/common/leftbar');

            $this->load->view('admin/manage_ot_tasks/add');
            $this->load->view('admin/common/footer'); 
        }
    }


    /*
     * Editing a role
     */
   function edit($id)
    {   
        // check if the role exists before trying to edit it
        $data['tasks'] = $this->Ot_tasks_model->get_tasks($id);
        
        if(isset($data['tasks']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('tasks','Tasks','required');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'tasks' => $this->input->post('tasks'),
					'updated_on' => Date('y-m-d h:i:s')
                );

                $this->Ot_tasks_model->update_tasks($id,$params);            
                redirect('admin/ot_tasks/index');
            }
            else
            { 
	                //$data['_view'] = 'role/edit';
            	$this->load->view('admin/common/top_header'); 
		        $this->load->view('admin/common/header');
		        $this->load->view('admin/common/leftbar');
                $this->load->view('admin/manage_ot_tasks/edit',$data);
                $this->load->view('admin/common/footer');
            }

	            
        }
        else
            show_error('The role you are trying to edit does not exist.');
    } 

     /*
     * Deleting role
     */
    function remove($id)
    {
        $tasks = $this->Ot_tasks_model->get_tasks($id);

        // check if the role exists before trying to delete it
        if(isset($tasks['id']))
        {
            $this->Ot_tasks_model->delete_tasks($id);
            redirect('admin/ot_tasks/index');
        }
        else
            show_error('The role you are trying to delete does not exist.');
    }

	}
?>