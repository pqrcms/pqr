<?php

//session_start(); //we need to start session in order to access it through CI

Class User_Authentication extends CI_Controller {

public function __construct() {
parent::__construct();

// Load form helper library
$this->load->helper('form');

// Load form validation library
$this->load->library('form_validation');

// Load session library
$this->load->library('session');

// Load database
$this->load->model('login/login_database');
}

// Show login page
public function index() {
$this->load->view('login/login_form');
}

// Show registration page
public function user_registration_show() {
$this->load->view('login/registration_form');
}

// Validate and store registration data in database
public function new_user_registration() {

// Check validation for user input in SignUp form
$this->form_validation->set_rules('username', 'Username', 'trim|required');
$this->form_validation->set_rules('email_value', 'Email', 'trim|required');
$this->form_validation->set_rules('password', 'Password', 'trim|required');
if ($this->form_validation->run() == FALSE) {
$this->load->view('login/registration_form');
} else {
$data = array(
'user_name' => $this->input->post('username'),
'user_email' => $this->input->post('email_value'),
'user_password' => $this->input->post('password')
);
$result = $this->login_database->registration_insert($data);
if ($result == TRUE) {
$data['message_display'] = 'Registration Successfully !';
$this->load->view('login/login_form', $data);
} else {
$data['message_display'] = 'Username already exist!';
$this->load->view('login/registration_form', $data);
}
}
}

// Check for user login process
public function user_login_process() {

$this->form_validation->set_rules('username', 'Username', 'trim|required');
$this->form_validation->set_rules('password', 'Password', 'trim|required');

if ($this->form_validation->run() == FALSE) {
if(isset($this->session->userdata['logged_in'])){
$this->load->view('login/admin_page');
}else{
$this->load->view('login/login_form');
}
} else {
$data = array(
'username' => $this->input->post('username'),
'password' => $this->input->post('password')
);
$result = $this->login_database->login($data);
if ($result == TRUE) {

$username = $this->input->post('username');
$result = $this->login_database->read_user_information($username);
if ($result != false) {
$session_data = array(
'username' => $result[0]->user_name,
'email' => $result[0]->user_email,
);
// Add user data in session
$this->session->set_userdata('logged_in', $session_data);
$this->load->view('login/admin_page');
}
} else {
$data = array(
'error_message' => 'Invalid Username or Password'
);
$this->load->view('login/login_form', $data);
}
}
}
// //Forgot Password
// public function forgot_password()
// {
// 	$this->load->view('login/ForgotPassword');
// }
// //
// public function user_forgot_password(){
// 	 	 $user_email = $this->input->post('user_email'); 
// 	 	 //print_r($user_email);die();     
//          $findemail = $this->login_database->ForgotPassword($user_email); 
//          if($findemail)
//          {
//           $this->login_database->sendpassword($findemail);        
//          }else{
//           $this->session->set_flashdata('msg',' Email not found!');
//           redirect(base_url().'login/login_database','refresh');
//       	 }
// }

public function display_doforget()
 {
    $data="";
    $this->load->view('login/forgetpassword',$data);
 }
 public function doforget()
 {
 //print_r($_POST);die();
 $this->load->helper('url');
 $email= $this->input->post('user_email');

 $this->load->library('form_validation');
 $this->form_validation->set_rules('user_email','user_email','required|trim');
 if ($this->form_validation->run() == FALSE)
 {
 
 $this->load->view('login/forgetpassword');
 
 }
 else
 {
  // print_r($email);die();
$this->db->select("*");
$this->db->from("user_login");
$this->db->where("user_email",$email);
$query = $this->db->get();
$res = $query->row_array();

 // $q = $this->db->query("select * from user_login where user_email='" . $email . "'");
 // echo $q->num_rows;die();
 // if ($q->num_rows) {
 //      $r = $q->result();
 //      $user=$r;
 //       print_r($user);die();
 $this->load->helper('string');
 $password= random_string('alnum',6);
 $this->db->where('user_id', $user->user_id);
 $this->db->update('user_login',array('user_password'=>$password,'pass_encryption'=>MD5($password)));
 $this->load->library('email');
 $this->email->from('sunilpqr@mailinator.com', 'sampletest');
 $this->email->to($user->user_email); 
 $this->email->subject('Password reset');
 $this->email->message('You have requested the new password, Here is you new password:'. $password); 
 $this->email->send();
     $this->session->set_flashdata('message','Password has been reset and has been sent to email'); 
    redirect('login/display_doforget');
    }
    }
 }

// Logout from admin page
public function logout() {

// Removing session data
$sess_array = array(
'username' => ''
);
$this->session->unset_userdata('logged_in', $sess_array);
$data['message_display'] = 'Successfully Logout';
$this->load->view('login/login_form', $data);
}

}

?>