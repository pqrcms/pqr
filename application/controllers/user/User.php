<?php
//session_start(); 

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
	parent::__construct();
    // Load session library
    $this->load->library('session');
	$this->load->helper('form');
	$this->load->library('form_validation');
	$this->load->library('session');
	$this->load->model('user/User_model');
        if(empty($_SESSION['is_userlogged_in']['user_id']))
        {
        redirect('login');
         //   redirect('user/user/login');
        }
	}

	public function index()
	{
		redirect('user/user/home');
	}

    public function home()
    {
        if($_SESSION['is_userlogged_in']['active']==0)
        {
        $user_id = $_SESSION['is_userlogged_in']['user_id'];
        $data['ot_tasks'] = $this->User_model->get_all_ot_tasks();
        //$data['trainee_feedback'] = $this->User_model->get_trainee_feedback();
        //print_r($data);die();
        $i=0;
        foreach ($data['ot_tasks'] as $ot) {
            $ot_id =  $ot['id'];
            $this->db->from('notification');
            $this->db->where('emp_id', $user_id);
            $this->db->where('type', $ot_id);
            $query= $this->db->get();
            $not = $query->row_array();
            if($not)
            {
                //print_r($not);die();
                //$created_at = $not['created_at'];
                //print_r($created_at);die();
                $data['ot_tasks'][$i]['posted_date'] = $not['created_at'];
                $data['ot_tasks'][$i]['check_status'] = 1;
            }else{
                $data['ot_tasks'][$i]['check_status'] = 0;
            }
            $i++;
        }
        $data['notification'] = $this->User_model->get_all_notification($user_id);
        $this->load->view('user/home',$data);
        }
        else{
            redirect('user/user/dashboard');
        }
    }

    //
    public function my_profile(){
        $user_id = $_SESSION['is_userlogged_in']['user_id'];
        //print_r($user_id);die();
        $data['emp_details'] = $this->User_model->get_emp_detail($user_id);
        print_r($data);die();
        $this->load->view('user/my_profile',$data);
    }

    public function dashboard(){
        if($_SESSION['is_userlogged_in']['active']==0)
        {
            redirect('user/user/home');
        }
        else{
            $user_id = $_SESSION['is_userlogged_in']['user_id'];

            $data['task_count']=$this->User_model->get_task_count($user_id);
            $data['emp_tasks']=$this->User_model->get_tasks_circle($user_id);
            //print_r($data['emp_tasks']);exit();
            $data['trainee_feedback'] = $this->User_model->get_trainee_feedback($user_id);
            //print_r($data['trainee_feedback']);die();
            $data['training_module']=$this->User_model->get_module($user_id);
            $data['result']=$this->User_model->get_result($user_id);
            $data['notifications']=$this->User_model->get_notification($user_id);
            // print_r($data['result']);exit();
            $data['trainer']=$this->User_model->get_trainer($user_id);
            $data['daily_log'][]['day']=$this->User_model->get_daily_log($user_id,date('Y-m-d'));
            $data['daily_log'][]['day']=$this->User_model->get_daily_log($user_id,date('Y-m-d',strtotime('-1 days',strtotime(date('Y-m-d')))));
            $data['daily_log'][]['day']=$this->User_model->get_daily_log($user_id,date('Y-m-d',strtotime('-2 days',strtotime(date('Y-m-d')))));
            // print_r($data['daily_log']);exit();
            //print_r($data['trainee_feedback']);exit();
            $this->load->view('user/dashboard',$data);
        }
    }
	// Check for user login process
	public function user_login_process() {
		//echo "jnsea";
        //print_r($_POST);die();
	}

	/***** User Login ****/ 
    

/***** Forget Password ****/ 

    public function forgot_password(){

    	$this->load->view('user/forgot_password');
    }


/***** Reset Password****/ 
    function reset_password()
    {
    	$email = base64_decode($this->input->get('email')); 
        $hash = $this->input->get('hash'); 
        $this->db->select('*');
        $this->db->from('emp_details');
        $this->db->where('email',$email);
        $this->db->where('hash_code',$hash);
        $detail_last_user = $this->db->get();
        $resultq = $detail_last_user->result();
        $data = $resultq;
        if($data)
        {
        	$this->load->view("user/reset_password");
        }
        else{
        	//$this->load->view("user/expired_link");
        	echo "<h2>Reset link is expired</h2>";die();
        }
    }

// Store new password into DB
    function validate_reset_password()
    {
        $email = base64_decode($this->input->post('email')); 
        $hash = $this->input->post('hash'); 
        $this->db->select('*');
        $this->db->from('emp_details');
        $this->db->where('email',$email);
        $this->db->where('hash_code',$hash);
        $detail_last_user = $this->db->get();
        $resultq = $detail_last_user->result();
        $data = $resultq;
        if($data)
        {
        	$code["password"] =md5($this->input->post('password')); 
        	$code["hash_code"] =""; 
            $this->db->where('email', $email);
            $update = $this->db->update('emp_details', $code); 
           	echo "1";die();
        }
        else
        {
        	echo "2";die();
        }
    }

//Send Notification to HR
    public function hr_submit($id)
    {
        //print_r($_POST);die();
        $emp_id = $_SESSION['is_userlogged_in']['user_id'];
        $tasks = $this->User_model->get_tasks_name($id);
         $this->User_model->increament_task_count($emp_id);
        $tot=$this->User_model->get_tasks_count();
        $count=$this->User_model->get_completed_count($emp_id);
        // print_r($tasks);exit();
        $task = $tasks->tasks;
        //print_r($task);die();

        $date=date('Y-m-d H:i');
        $params = array(
                'emp_id' => $emp_id,
                'type' => $id,
                'tasks' => $task,
                'notification' => 1,
                'created_at' => $date,
                'updated_on' => $date
                );
        //print_r($params);die();
        $this->User_model->send_notification($emp_id,$params);
        if($count['task_count']>=$tot->tot)
        {
         $this->User_model->change_active($emp_id);
         $_SESSION['is_userlogged_in']['active']=1;
            echo 1;die;
        }
        else{
            echo $date;die;
        }
    }
    function get_task($tid)
    {
        $data=$this->User_model->get_task($tid);
        echo json_encode($data);die;
    }
    function add_feedback()
    {
       // print_r($_POST);exit();
       $data=array(
            'emp_id'=>$_SESSION['is_userlogged_in']['user_id'],
            'trainer_id'=>$_POST['trainer'],
            'task_id'=>$_POST['task'],
            'selected_fids'=>$_POST['fids'],
            'created_at'=>date('Y-m-d H:i')
        );
        $data=$this->User_model->add_feedback($data);
        echo $data;die;
    }
    function get_question($tid,$no)
    {
        $data['no']=$no;
        $data['tid']=$tid;
        $data['question']=$this->User_model->get_question($tid,$no);
        $view="No Questions";
        if($data['question']['type']==1){
            $view=$this->load->view('user/mcq',$data,true);
        }
        else if($data['question']['type']==2)
        {
            $view=$this->load->view('user/essay',$data,true);
        }
        // print_r($data);exit();
        echo $view;die;
    }
    function get_total($tid)
    {   
        $_SESSION['start_time']=date("H:i:s");
        $emp_id=$_SESSION['is_userlogged_in']['user_id'];
        $data=$this->User_model->get_count_question($tid);
        $task=$this->User_model->get_task_details($tid);
        $log='Started training <span class="bold">“'.$task['title'].'”';
        $datas=array(
            'emp_id'=>$emp_id,
            'log'=>$log,
            'status'=>'0',
            'date'=>date('Y-m-d'),
            'time'=>date('H:i'),
            'created_at'=>date('Y-m-d H:i'),
            );
        $flag=$this->User_model->add_daily_log($datas);
        // print_r($data);exit();
        echo $data['total'];die;
    }
    function save_answer($tid)
    {
        // print_r($_POST);exit();
        $_SESSION['question'][$tid."_".$_POST['qid']]=$_POST['answer'];
        $emp_id=$_SESSION['is_userlogged_in']['user_id'];
        $data=array(
            'emp_id'=>$_SESSION['is_userlogged_in']['user_id'],
            'task_id'=>$tid,
            'qid'=>$_POST['qid'],
            'answer'=>$_POST['answer'],
            'type'=>$_POST['type'],
            );
        $data=$this->User_model->save_answer($data,$tid,$_POST['qid'],$emp_id);
        // print_r($data);exit();
        echo 1;die;
    }
    function submit_task($tid)
    {
        // print_r($_POST);exit();
        // $_SESSION['question']="";
        unset($_SESSION['question']);
        $dteStart = new DateTime($_SESSION['start_time']); 
        $dteEnd   = new DateTime(date('H:i:s')); 
        $dteDiff  = $dteStart->diff($dteEnd); 
        $diiff= $dteDiff->format("%H:%I:%S"); 

        // print_r($diiff);exit();
        $emp_id=$_SESSION['is_userlogged_in']['user_id'];

        $answered=$this->User_model->get_answered_count($tid,$emp_id);
        // print_r($answered);exit();
        $mark=$this->User_model->get_score($tid,$emp_id);
        // print_r($data);exit();
        $task=$this->User_model->get_task_details($tid);
        $data=array(
            'emp_id'=>$_SESSION['is_userlogged_in']['user_id'],
            'task_id'=>$tid,
            'status'=>'1',
            'start_time'=>$_SESSION['start_time'],
            'completed_time'=>date('Y-m-d H:i:s'),
            'total_time'=>$diiff,
            'mark_obtained'=>$mark['mark'],
            'max_marks'=>$task['total_marks'],
            'total_question_attended'=>$answered['total'],
            'created_at'=>date('Y-m-d H:i:s'),
            );
        $data=$this->User_model->submit_task($data);
        $log= 'Submitted training <span class="bold">“'.$task['title'].'”';
        $data=array(
            'emp_id'=>$emp_id,
            'log'=>$log,
            'status'=>'0',
            'date'=>date('Y-m-d'),
            'time'=>date('H:i'),
            'created_at'=>date('Y-m-d H:i')
            );
        $flag=$this->User_model->add_daily_log($data);
        $notification= 'Completed training <span class="bold">“'.$task['title'].'”';
        $data=array(
            'emp_id'=>$emp_id,
            'notification'=>$notification,
            'user_type'=>'0',
            'status'=>'0',
            'created_at'=>date('Y-m-d H:i')
            );
        $flag=$this->User_model->add_notification($data);
        unset($_SESSION['start_time']);
        // print_r($data);exit();
        echo 1;die;
    }
    function delete_log($lid)
    {
        $data=$this->User_model->delete_log($lid);
        echo json_encode($data);die;
    }
     public function get_log($day1,$day2,$month,$year){
        if($day1>$day2)
        {
            $temp=$day1;
            $day1=$day2;
            $day2=$temp;
        }
        $date=$year."-".$month."-".$day1;
        $test1 = new DateTime($date);
        $date1= date_format($test1, 'Y-m-d'); // 2011-03-03 00:00:00
        $date=$year."-".$month."-".$day2;
        $test2 = new DateTime($date);
        $date2=date_format($test2, 'Y-m-d');

        $begin = new DateTime( "2015-07-03" );
        $end   = new DateTime( "2015-07-09" );

        $emp_id=$_SESSION['is_userlogged_in']['user_id'];

        for($i = $test1; $i <= $test2; $i->modify('+1 day')){
            $d= $i->format("Y-m-d");
            $data[]['day']=$this->User_model->get_daily_log($emp_id,$d);
        }
        echo json_encode($data);die;
        /*print_r($data);exit();
            $data['daily_log'][]['day']=$this->User_model->get_daily_log($user_id,date('Y-m-d'));
            $data['daily_log'][]['day']=$this->User_model->get_daily_log($user_id,date('Y-m-d',strtotime('-1 days',strtotime(date('Y-m-d')))));
            $data['daily_log'][]['day']=$this->User_model->get_daily_log($user_id,date('Y-m-d',strtotime('-2 days',strtotime(date('Y-m-d')))));*/

    }
}