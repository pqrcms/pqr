<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	Class Home extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->helper('Ckediter');
			$this->load->library('ckeditor');
	  		$this->load->library('ckfinder');
			$this->load->model('Home_model');
			$this->load->model('employee/Aboutus_model');
			$this->load->library('uploader');
			if(empty($_SESSION['employee_logged_in']['user_id'])){
				redirect('employee'); 
			}
		}
		public function index(){
			$data['image'] = $this->Home_model->get_all_home_image();
			$this->load->view('employee/common/top_header'); 
			$this->load->view('employee/common/header');
			$this->load->view('employee/common/leftbar');
			$this->load->view('employee/common/footer');
			$this->load->view('employee/home/home_image',$data);  
		}
		
		
		public function add_home_image()
		{
			//$data['journal'] = $this->Journal_model->get_all_journal();
			$this->load->view('employee/common/top_header'); 
			$this->load->view('employee/common/header');
			$this->load->view('employee/common/leftbar');
			$this->load->view('employee/common/footer');
			$this->load->view('employee/home/add_home_image');
		}

		public function insert_home_image()
		{
			
			$name="home_image";
			$filename = $_FILES['home_image']['name'];
			$flag = "post";
			$add_gallery['user_id'] = "home_image/".$name;
			$add_gallery['flag'] = $flag;
			$count = count($_FILES['home_image']['name']);
			//print_r($_FILES['home_image']);exit();
			if(!empty($_FILES['home_image']['name'][0]))
			{   
				$flag = "post";
				$add_gallery['user_id'] = "home_image/".$name;
				$add_gallery['flag'] = $flag;
				$count = count($_FILES['home_image']['name']);  
				for($s=0; $s<=$count-1;$s++)
				{
					$image['name'] = $_FILES['home_image']['name'][$s];
					$image['type'] = $_FILES['home_image']['type'][$s];
					$image['tmp_name'] = $_FILES['home_image']['tmp_name'][$s];
					$image['error'] = $_FILES['home_image']['error'][$s];
					$image['size'] = $_FILES['home_image']['size'][$s];
					$target_file[] = $this->uploader->upload_image($image, $flag,$add_gallery);    
				}
				foreach($target_file as $t_f)
				{
					$data1=array(
						'title' => $this->input->post('title'),
						'content' => $this->input->post('content'),
						'alt' => $this->input->post('alt'),
						'image'=>$t_f['profile_org_url'],
						'created_at'  => date("Y-m-d H:i:s"),
					);
					$this->db->insert('home_image', $data1);
				}
			}
			redirect('employee/home');
		} 
		public function delete_home_image($id)
		{
  			//echo $id;exit();
			$this->Home_model->delete_home_image($id);
			redirect('employee/home');
		}	

		public function choose_us()
	    {
			$data['choose'] = $this->Home_model->get_all_choose_us();
			$this->load->view('employee/common/top_header'); 
	        $this->load->view('employee/common/header');
	        $this->load->view('employee/common/leftbar');
	        $this->load->view('employee/common/footer');
	        $this->load->view('employee/home/choose_us',$data);  
		}
		public function add_choose_us()
		{
			$this->form_validation->set_rules('title_1', 'Title 1', 'required',
                        array('title_1' => 'Title 1 is empty'));
			$this->form_validation->set_rules('content_1', 'Content 1', 'required',
                        array('content_1' => 'Content 1 is empty'));
			$this->form_validation->set_rules('title_2', 'Title 2', 'required',
                        array('title_2' => 'Title 2 is empty'));
			$this->form_validation->set_rules('content_2', 'Content 2', 'required',
                        array('content_2' => 'Content 2 is empty'));
			$this->form_validation->set_rules('title_3', 'Title 3', 'required',
                        array('title_3' => 'Title 3 is empty'));
			$this->form_validation->set_rules('content_3', 'Content 3', 'required',
                        array('content_3' => 'Content 3 is empty'));
			$this->form_validation->set_rules('title_4', 'Title 4', 'required',
                        array('title_4' => 'Title 4 is empty'));
			$this->form_validation->set_rules('content_4', 'Content 4', 'required',
                        array('content_4' => 'Content 4 is empty'));
			$this->form_validation->set_rules('title_5', 'Title 5', 'required',
                        array('title_5' => 'Title 5 is empty'));
			$this->form_validation->set_rules('content_5', 'Content 5', 'required',
                        array('content_5' => 'Content 5 is empty'));
			$this->form_validation->set_rules('title_6', 'Title 6', 'required',
                        array('title_6' => 'Title 6 is empty'));
			$this->form_validation->set_rules('content_6', 'Content 6', 'required',
                        array('content_6' => 'Content 6 is empty'));
        	if($this->form_validation->run())
        	{
        		 $params = array(
					'title_1'   => $this->input->post('title_1'), 
					'content_1' => $this->input->post('content_1'),
					'title_2'   => $this->input->post('title_2'), 
					'content_2' => $this->input->post('content_2'),
					'title_3'   => $this->input->post('title_3'), 
					'content_3' => $this->input->post('content_3'),
					'title_4'   => $this->input->post('title_4'), 
					'content_4' => $this->input->post('content_4'),
					'title_5'   => $this->input->post('title_5'), 
					'content_5' => $this->input->post('content_5'),
					'title_6'   => $this->input->post('title_6'), 
					'content_6' => $this->input->post('content_6'),
	                'created_at'       => Date('Y-m-d h:i:s'),
	                'updated_at'       => Date('Y-m-d h:i:s'),
	            );
	            $id = $this->Home_model->add_choose($params);	            
	            redirect('employee/home/choose_us');
        	}
        	else
	        {	           
	            $this->load->view('employee/common/top_header'); 
		        $this->load->view('employee/common/header');
		        $this->load->view('employee/common/leftbar');
		        $this->load->view('employee/common/footer'); 
		        $this->load->view('employee/home/add_choose_us');	            
	        }
		}
		public function edit_choose_us($id)
		{
			 $choose = $this->Home_model->get_choose_byid($id);
	        if(isset($choose->id))
	        {
				$this->form_validation->set_rules('title_1', 'Title 1', 'required',
                        array('title_1' => 'Title 1 is empty'));
				$this->form_validation->set_rules('content_1', 'Content 1', 'required',
	                        array('content_1' => 'Content 1 is empty'));
				$this->form_validation->set_rules('title_2', 'Title 2', 'required',
	                        array('title_2' => 'Title 2 is empty'));
				$this->form_validation->set_rules('content_2', 'Content 2', 'required',
	                        array('content_2' => 'Content 2 is empty'));
				$this->form_validation->set_rules('title_3', 'Title 3', 'required',
	                        array('title_3' => 'Title 3 is empty'));
				$this->form_validation->set_rules('content_3', 'Content 3', 'required',
	                        array('content_3' => 'Content 3 is empty'));
				$this->form_validation->set_rules('title_4', 'Title 4', 'required',
	                        array('title_4' => 'Title 4 is empty'));
				$this->form_validation->set_rules('content_4', 'Content 4', 'required',
	                        array('content_4' => 'Content 4 is empty'));
				$this->form_validation->set_rules('title_5', 'Title 5', 'required',
	                        array('title_5' => 'Title 5 is empty'));
				$this->form_validation->set_rules('content_5', 'Content 5', 'required',
	                        array('content_5' => 'Content 5 is empty'));
				$this->form_validation->set_rules('title_6', 'Title 6', 'required',
	                        array('title_6' => 'Title 6 is empty'));
				$this->form_validation->set_rules('content_6', 'Content 6', 'required',
	                        array('content_6' => 'Content 6 is empty'));
				if($this->form_validation->run())     
	            {  	               
	            	
	                $params = array(
						'title_1'   => $this->input->post('title_1'), 
						'content_1' => $this->input->post('content_1'),
						'title_2'   => $this->input->post('title_2'), 
						'content_2' => $this->input->post('content_2'),
						'title_3'   => $this->input->post('title_3'), 
						'content_3' => $this->input->post('content_3'),
						'title_4'   => $this->input->post('title_4'), 
						'content_4' => $this->input->post('content_4'),
						'title_5'   => $this->input->post('title_5'), 
						'content_5' => $this->input->post('content_5'),
						'title_6'   => $this->input->post('title_6'), 
						'content_6' => $this->input->post('content_6'),
		                'updated_at'       => Date('Y-m-d h:i:s'),
	                );
	                
	                $this->Home_model->update_choose($id,$params);            
	                redirect('employee/home/choose_us');
	            }
	            else
	            {   
	                $data['choose'] = $this->Home_model->get_choose_byid($id);            
	                $this->load->view('employee/common/top_header'); 
			        $this->load->view('employee/common/header');
			        $this->load->view('employee/common/leftbar');
			        $this->load->view('employee/common/footer');
			        $this->load->view('employee/home/edit_choose_us',$data);
	            }
	        }
	        else

	            show_error('The Choose Us you are trying to edit does not exist.');
	    
		}
		public function delete_choose_us($id)
		{
			if(isset($id))
	    	{
	    		$delete = $this->Home_model->delete_choose_us($id);
	    		redirect('employee/home/choose_us');
	    	}
		}

		public function news()
	    {
			$data['news'] = $this->Home_model->get_all_news();
			$this->load->view('employee/common/top_header'); 
	        $this->load->view('employee/common/header');
	        $this->load->view('employee/common/leftbar');
	        $this->load->view('employee/common/footer');
	        $this->load->view('employee/home/news',$data);  
		}
		public function add_news()
		{
			$this->form_validation->set_rules('title', 'Title', 'required',
                        array('title' => 'Title is empty'));
			$this->form_validation->set_rules('new', 'news', 'required',
                        array('new' => 'News is empty'));
			
        	if($this->form_validation->run())
        	{
        		$filename = $_FILES['image']['name'];
        		 $params = array(
					'title'   => $this->input->post('title'), 
					'news' => $this->input->post('new'),
					'alt' => $this->input->post('alt'),
	                'created_at'       => Date('Y-m-d h:i:s'),
	                'updated_at'       => Date('Y-m-d h:i:s'),
	            );
	            $id = $this->Home_model->add_news($params);	  

	            $flag = "profile";
	            $add_journal['user_id'] = "News/".$id;
	            $add_journal['flag'] = $flag;
	            if(!empty($_FILES['image']['name'])){
	            $target_file = $this->uploader->upload_image($_FILES['image'], $flag,$add_journal);
	            }
	            $this->db->where('id',$id);
	            $this->db->update('news', 
	                array( 'image' => $target_file['profile_org_url'], ));

	            redirect('employee/home/news');
        	}
        	else
	        {	     
	        	$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
				$this->ckeditor->config['toolbar'] = 'Full';
				$this->ckeditor->config['language'] = 'en';
				$this->ckeditor->config['width'] = '730px';
				$this->ckeditor->config['height'] = '300px';            

				//Add Ckfinder to Ckeditor
				$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');      
	            $this->load->view('employee/common/top_header'); 
		        $this->load->view('employee/common/header');
		        $this->load->view('employee/common/leftbar');
		        $this->load->view('employee/common/footer'); 
		        $this->load->view('employee/home/add_news');	            
	        }
		}
		public function edit_news($id)
		{
			 $news = $this->Home_model->get_news_byid($id);
	        if(isset($news->id))
	        {
				$this->form_validation->set_rules('title', 'Title', 'required',
                        array('title' => 'Title is empty'));
				$this->form_validation->set_rules('new', 'news', 'required',
                        array('new' => 'News is empty'));
				if($this->form_validation->run())     
	            {  	       
	            	$filename = $_FILES['image']['name'];        
	            	
	                $params = array(
						'title'   => $this->input->post('title'), 
						'news' => $this->input->post('new'),

		                'updated_at'       => Date('Y-m-d h:i:s'),
		            );
		             $flag = "profile";
		            $add_journal['user_id'] = "News/".$id;
		            $add_journal['flag'] = $flag;
		            if(!empty($_FILES['image']['name'])){
		            $target_file = $this->uploader->upload_image($_FILES['image'], $flag,$add_journal);
		            }
		            if(!empty($target_file['profile_org_url']))
	                    $params['image'] = $target_file['profile_org_url'];
	                $this->Home_model->update_news($id,$params);   

	               
		            // $this->db->where('id',$id);
		            // $this->db->update('news', 
		            //     array( 'image' => $target_file['profile_org_url'], ));         
	                redirect('employee/home/news');
	            }
	            else
	            {   
	            	$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
					$this->ckeditor->config['toolbar'] = 'Full';
					$this->ckeditor->config['language'] = 'en';
					$this->ckeditor->config['width'] = '730px';
					$this->ckeditor->config['height'] = '300px';            

					//Add Ckfinder to Ckeditor
					$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');

	                $data['news'] = $this->Home_model->get_news_byid($id);            
	                $this->load->view('employee/common/top_header'); 
			        $this->load->view('employee/common/header');
			        $this->load->view('employee/common/leftbar');
			        $this->load->view('employee/common/footer');
			        $this->load->view('employee/home/edit_news',$data);
	            }
	        }
	        else

	            show_error('The news you are trying to edit does not exist.');
	    
		}
		public function delete_news($id)
		{
			if(isset($id))
	    	{
	    		$delete = $this->Home_model->delete_news($id);
	    		redirect('employee/home/news');
	    	}
		}
		function discount_coupon()
		{
			$data['coupon'] = $this->Home_model->get_all_coupons();            
	                $this->load->view('employee/common/top_header'); 
			        $this->load->view('employee/common/header');
			        $this->load->view('employee/common/leftbar');
			        $this->load->view('employee/common/footer');
			        $this->load->view('employee/home/coupon',$data);
		}
		function add_coupon()
		{
			$chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
		    srand((double)microtime()*1000000); 
		    $i = 0; 
		    $pass = '' ; 

		    while ($i <= 7) { 
		        $num = rand() % 33; 
		        $tmp = substr($chars, $num, 1); 
		        $pass = $pass . $tmp; 
		        $i++; 
		    }
		   
			$data['code'] = $pass;            
	                $this->load->view('employee/common/top_header'); 
			        $this->load->view('employee/common/header');
			        $this->load->view('employee/common/leftbar');
			        $this->load->view('employee/common/footer');
			        $this->load->view('employee/home/add_coupon',$data);
		}
		function delete_coupon($id)
		{
			$this->Home_model->delete_coupon($id);
			redirect('employee/home/discount_coupon');
		}
		function insert_coupon()
		{
			//print_r($_POST);exit();
			if(!empty($_POST['coupon'])&&!empty($_POST['discount'])&&!empty($_POST['no_of_coupon'])&&!empty($_POST['expiry']))
			{
				$start = $this->input->post('expiry');
		    	$s = date("Y-m-d", strtotime($start));
				$code = array(
					'vouchercode' => $this->input->post('coupon'), 
					'discount_amount' => $this->input->post('discount'),
					'num_vouchers' => $this->input->post('no_of_coupon'),
					'expiry' => $s,
					);
				$this->db->insert('discount_codes',$code);
			}
			redirect('employee/home/discount_coupon');
		}
		public function blogs()
	    {
			$data['blogs'] = $this->Home_model->get_all_blogs();
			$this->load->view('employee/common/top_header'); 
	        $this->load->view('employee/common/header');
	        $this->load->view('employee/common/leftbar');
	        $this->load->view('employee/common/footer');
	        $this->load->view('employee/home/blogs',$data);  
		}
		public function add_blogs()
		{
			$this->form_validation->set_rules('title', 'Title', 'required',
                        array('title' => 'Title is empty'));
			$this->form_validation->set_rules('blog', 'blogs', 'required',
                        array('blog' => 'Blogs is empty'));
			
        	if($this->form_validation->run())
        	{
        		$filename = $_FILES['image']['name'];
        		 $params = array(
					'blog_title'   => $this->input->post('title'), 
					'blog_content' => $this->input->post('blog'),
					'alt' => $this->input->post('alt'),
	                'created_at'       => Date('Y-m-d h:i:s'),
	                'updated_at'       => Date('Y-m-d h:i:s'),
	            );
	            $id = $this->Home_model->add_blogs($params);	  

	            $flag = "profile";
	            $add_blog['user_id'] = "Blogs/".$id;
	            $add_blog['flag'] = $flag;
	            if(!empty($_FILES['image']['name'])){
	            $target_file = $this->uploader->upload_image($_FILES['image'], $flag,$add_blog);
	            }
	            $this->db->where('id',$id);
	            $this->db->update('blogs', 
	                array( 'blog_image' => $target_file['profile_org_url'], ));

	            redirect('employee/home/blogs');
        	}
        	else
	        {	     
	        	$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
				$this->ckeditor->config['toolbar'] = 'Full';
				$this->ckeditor->config['language'] = 'en';
				$this->ckeditor->config['width'] = '730px';
				$this->ckeditor->config['height'] = '300px';            

				//Add Ckfinder to Ckeditor
				$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');      
	            $this->load->view('employee/common/top_header'); 
		        $this->load->view('employee/common/header');
		        $this->load->view('employee/common/leftbar');
		        $this->load->view('employee/common/footer'); 
		        $this->load->view('employee/home/add_blogs');	            
	        }
		}
		public function edit_blogs($id)
		{
			 $blogs = $this->Home_model->get_blogs_byid($id);
	        if(isset($blogs->id))
	        {
				$this->form_validation->set_rules('title', 'Title', 'required',
                        array('title' => 'Title is empty'));
				$this->form_validation->set_rules('blog', 'blogs', 'required',
                        array('blog' => 'Blogs is empty'));
				if($this->form_validation->run())     
	            {  	       
	            	$filename = $_FILES['image']['name'];        
	            	
	                $params = array(
						'blog_title'   => $this->input->post('title'), 
						'blog_content' => $this->input->post('blog'),

		                'updated_at'       => Date('Y-m-d h:i:s'),
		            );
		             $flag = "profile";
		            $add_blog['user_id'] = "Blogs/".$id;
		            $add_blog['flag'] = $flag;
		            if(!empty($_FILES['image']['name'])){
		            $target_file = $this->uploader->upload_image($_FILES['image'], $flag,$add_blog);
		            }
		            if(!empty($target_file['profile_org_url']))
	                    $params['image'] = $target_file['profile_org_url'];
	                $this->Home_model->update_blogs($id,$params);   

	               
		            // $this->db->where('id',$id);
		            // $this->db->update('news', 
		            //     array( 'image' => $target_file['profile_org_url'], ));         
	                redirect('employee/home/blogs');
	            }
	            else
	            {   
	            	$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
					$this->ckeditor->config['toolbar'] = 'Full';
					$this->ckeditor->config['language'] = 'en';
					$this->ckeditor->config['width'] = '730px';
					$this->ckeditor->config['height'] = '300px';            

					//Add Ckfinder to Ckeditor
					$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');

	                $data['blogs'] = $this->Home_model->get_blogs_byid($id);            
	                $this->load->view('employee/common/top_header'); 
			        $this->load->view('employee/common/header');
			        $this->load->view('employee/common/leftbar');
			        $this->load->view('employee/common/footer');
			        $this->load->view('employee/home/edit_blogs',$data);
	            }
	        }
	        else

	            show_error('The news you are trying to edit does not exist.');
	    
		}
		public function delete_blogs($id)
		{
			if(isset($id))
	    	{
	    		$delete = $this->Home_model->delete_blogs($id);
	    		redirect('employee/home/blogs');
	    	}
		}
		public function guidelines()
	    {
			$data['guidelines'] = $this->Home_model->get_all_guidelines();
			$this->load->view('employee/common/top_header'); 
	        $this->load->view('employee/common/header');
	        $this->load->view('employee/common/leftbar');
	        $this->load->view('employee/common/footer');
	        $this->load->view('employee/home/guidelines',$data);  
		}
		public function add_guidelines()
		{
			$this->form_validation->set_rules('title', 'Title', 'required',
                        array('title' => 'Title is empty'));
			$this->form_validation->set_rules('guideline', 'guidelines', 'required',
                        array('guideline' => 'guidelines is empty'));
			
        	if($this->form_validation->run())
        	{
        		$filename = $_FILES['image']['name'];
        		 $params = array(
					'guidelines_title'   => $this->input->post('title'), 
					'guidelines' => $this->input->post('guideline'),
					'alt' => $this->input->post('alt'),
	                'created_at'       => Date('Y-m-d h:i:s'),
	                'updated_at'       => Date('Y-m-d h:i:s'),
	            );
	            $id = $this->Home_model->add_guidelines($params);	  

	            $flag = "profile";
	            $add_guidelines['user_id'] = "guidelines/".$id;
	            $add_guidelines['flag'] = $flag;
	            if(!empty($_FILES['image']['name'])){
	            $target_file = $this->uploader->upload_image($_FILES['image'], $flag,$add_guidelines);
	            }
	            $this->db->where('id',$id);
	            $this->db->update('home_guidelines', 
	                array( 'guidelines_image' => $target_file['profile_org_url'], ));

	            redirect('employee/home/guidelines');
        	}
        	else
	        {	     
	        	$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
				$this->ckeditor->config['toolbar'] = 'Full';
				$this->ckeditor->config['language'] = 'en';
				$this->ckeditor->config['width'] = '730px';
				$this->ckeditor->config['height'] = '300px';            

				//Add Ckfinder to Ckeditor
				$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');      
	            $this->load->view('employee/common/top_header'); 
		        $this->load->view('employee/common/header');
		        $this->load->view('employee/common/leftbar');
		        $this->load->view('employee/common/footer'); 
		        $this->load->view('employee/home/add_guidelines');	            
	        }
		}
		public function edit_guidelines($id)
		{
			 $guidelines = $this->Home_model->get_guidelines_byid($id);
	        if(isset($guidelines->id))
	        {
				$this->form_validation->set_rules('title', 'Title', 'required',
                        array('title' => 'Title is empty'));
				$this->form_validation->set_rules('guideline', 'guidelines', 'required',
                        array('guideline' => 'guidelines is empty'));
				if($this->form_validation->run())     
	            {  	       
	            	$filename = $_FILES['image']['name'];        
	            	
	                $params = array(
						'guidelines_title'   => $this->input->post('title'), 
						'guidelines' => $this->input->post('guideline'),

		                'updated_at'       => Date('Y-m-d h:i:s'),
		            );
		             $flag = "profile";
		            $add_guidelines['user_id'] = "guidelines/".$id;
		            $add_guidelines['flag'] = $flag;
		            if(!empty($_FILES['image']['name'])){
		            $target_file = $this->uploader->upload_image($_FILES['image'], $flag,$add_guidelines);
		            }
		            if(!empty($target_file['profile_org_url']))
	                    $params['image'] = $target_file['profile_org_url'];
	                $this->Home_model->update_guidelines($id,$params);   

	               
		            // $this->db->where('id',$id);
		            // $this->db->update('news', 
		            //     array( 'image' => $target_file['profile_org_url'], ));         
	                redirect('employee/home/guidelines');
	            }
	            else
	            {   
	            	$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
					$this->ckeditor->config['toolbar'] = 'Full';
					$this->ckeditor->config['language'] = 'en';
					$this->ckeditor->config['width'] = '730px';
					$this->ckeditor->config['height'] = '300px';            

					//Add Ckfinder to Ckeditor
					$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');

	                $data['guidelines'] = $this->Home_model->get_guidelines_byid($id);            
	                $this->load->view('employee/common/top_header'); 
			        $this->load->view('employee/common/header');
			        $this->load->view('employee/common/leftbar');
			        $this->load->view('employee/common/footer');
			        $this->load->view('employee/home/edit_guidelines',$data);
	            }
	        }
	        else

	            show_error('The news you are trying to edit does not exist.');
	    
		}
		public function delete_guidelines($id)
		{
			if(isset($id))
	    	{
	    		$delete = $this->Home_model->delete_guidelines($id);
	    		redirect('employee/home/guidelines');
	    	}
		}
		public function jobs()
	    {
			$data['jobs'] = $this->Home_model->get_all_jobs();
			$this->load->view('employee/common/top_header'); 
	        $this->load->view('employee/common/header');
	        $this->load->view('employee/common/leftbar');
	        $this->load->view('employee/common/footer');
	        $this->load->view('employee/home/jobs',$data);  
		}
		public function add_jobs()
		{
			$this->form_validation->set_rules('title', 'Title', 'required',
                        array('title' => 'Title is empty'));
			$this->form_validation->set_rules('location', 'location', 'required',
                        array('location' => 'location is empty'));
			$this->form_validation->set_rules('openings', 'openings', 'required',
                        array('openings' => 'No of Openings is empty'));
			$this->form_validation->set_rules('description', 'description', 'required',
                        array('description' => 'Job description is empty'));
			$this->form_validation->set_rules('education', 'education', 'required',
                        array('education' => 'Education is empty'));
			$this->form_validation->set_rules('salary', 'salary', 'required',
                        array('salary' => 'salary is empty'));			
        	if($this->form_validation->run())
        	{
        		 $params = array(
					'job_title'   => $this->input->post('title'), 
					'location' => $this->input->post('location'),
					'no_of_openings' => $this->input->post('openings'),
					'job_description' => $this->input->post('description'),
					'education' => $this->input->post('education'),
					'salary' => $this->input->post('salary'),
	                'created_at'       => Date('Y-m-d h:i:s'),
	                'updated_at'       => Date('Y-m-d h:i:s'),
	            );
	            $id = $this->Home_model->add_jobs($params);	  
	            redirect('employee/home/jobs');
        	}
        	else
	        {	        
	            $this->load->view('employee/common/top_header'); 
		        $this->load->view('employee/common/header');
		        $this->load->view('employee/common/leftbar');
		        $this->load->view('employee/common/footer'); 
		        $this->load->view('employee/home/add_jobs');	            
	        }
		}
		public function edit_jobs($id)
		{
			 $jobs = $this->Home_model->get_jobs_byid($id);
	        if(isset($jobs->id))
	        {
				$this->form_validation->set_rules('title', 'Title', 'required',
	                        array('title' => 'Title is empty'));
				$this->form_validation->set_rules('location', 'location', 'required',
	                        array('location' => 'location is empty'));
				$this->form_validation->set_rules('openings', 'openings', 'required',
	                        array('openings' => 'No of Openings is empty'));
				$this->form_validation->set_rules('description', 'description', 'required',
	                        array('description' => 'Job description is empty'));
				$this->form_validation->set_rules('education', 'education', 'required',
	                        array('education' => 'Education is empty'));
				$this->form_validation->set_rules('salary', 'salary', 'required',
	                        array('salary' => 'salary is empty'));
				if($this->form_validation->run())     
	            {  	        
	            	
	                $params = array(
							'job_title'   => $this->input->post('title'), 
							'location' => $this->input->post('location'),
							'no_of_openings' => $this->input->post('openings'),
							'job_description' => $this->input->post('description'),
							'education' => $this->input->post('education'),
							'salary' => $this->input->post('salary'),
			                'created_at'       => Date('Y-m-d h:i:s'),
			                'updated_at'       => Date('Y-m-d h:i:s'),
		            	);
		             
	                $this->Home_model->update_jobs($id,$params);         
	                redirect('employee/home/jobs');
	            }
	            else
	            {   
	                $data['jobs'] = $this->Home_model->get_jobs_byid($id);            
	                $this->load->view('employee/common/top_header'); 
			        $this->load->view('employee/common/header');
			        $this->load->view('employee/common/leftbar');
			        $this->load->view('employee/common/footer');
			        $this->load->view('employee/home/edit_jobs',$data);
	            }
	        }
	        else

	            show_error('The jobs you are trying to edit does not exist.');
	    
		}
		public function delete_jobs($id)
		{
			if(isset($id))
	    	{
	    		$delete = $this->Home_model->delete_jobs($id);
	    		redirect('employee/home/jobs');
	    	}
		}
		public function job_application()
	    {
			$data['application'] = $this->Home_model->get_all_application();
			$this->load->view('employee/common/top_header'); 
	        $this->load->view('employee/common/header');
	        $this->load->view('employee/common/leftbar');
	        $this->load->view('employee/common/footer');
	        $this->load->view('employee/home/job_application',$data);  
		}
		public function download_resume($id)
		{
			$jobs = $this->Home_model->get_application_byid($id);
			$path = $jobs->resume;
			$name=$jobs->resume_name;
			if(is_file($path))
			{
				// required for IE
				if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); 
				}

				// get the file mime type using the file extension
				$this->load->helper('file');

				$mime = get_mime_by_extension($path);

				// Build the headers to push out the file properly.
				header('Pragma: public');     // required
				header('Expires: 0');         // no cache
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
				header('Cache-Control: private',false);
				header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
				header('Content-Disposition: attachment; filename="'.basename($name).'"');  // Add the file name
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: '.filesize($path)); // provide file size
				header('Connection: close');
				readfile($path); // push it out
				exit();
			}
		}
		public function delete_application($id)
		{
			$this->db->where('id',$id);
			$this->db->delete('job_application');
		}
		public function manage_aboutus_professors()
		{
	    $data['message'] = $this->Aboutus_model->aboutus_professors();
	    // print_r($data);exit();
		$this->load->view('employee/common/top_header');
		$this->load->view('employee/common/header');
	    $this->load->view('employee/common/leftbar');
	    $this->load->view('employee/about_us/professors',$data);
	    $this->load->view('employee/common/footer');
		}
	  public function add_aboutus_professors()
	  {
	  	if($_POST)
	  	{
	  		$message_details = array(
	          	'name' => $this->input->post('name'), 
	          	'college_name' => $this->input->post('college_name'),
	          	'message' => $this->input->post('message'),
	          	'added_date'  => Date('Y-m-d H:i:s'),
                'updated_date'  => Date('Y-m-d H:i:s'),
	          	);
	  		// print_r($message_details);exit();
	          $res = $this->Aboutus_model->insert_aboutus($message_details,'aboutus_professors');
	    
	          if($res > 0){
	          	$filename = $_FILES['photo']['name'];
            $flag = "aboutus_professors";
            $add_news['user_id'] = "user_image/".$res;
            $add_news['flag'] = $flag;
            if(!empty($_FILES['photo']['name']))
            {
                $target_file = $this->uploader->upload_image($_FILES['photo'], $flag,$add_news);
                $this->db->where('id',$res);
                $this->db->update('aboutus_professors', 
                    array( 'photo' => $target_file['profile_org_url'], ));
            }
        }
	    redirect('employee/Home/manage_aboutus_professors');
	         
	  	}
	  	else{
	  		$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
				$this->ckeditor->config['toolbar'] = 'Full';
				$this->ckeditor->config['language'] = 'en';
				$this->ckeditor->config['width'] = '730px';
				$this->ckeditor->config['height'] = '300px';            

				//Add Ckfinder to Ckeditor
				$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');
			$this->load->view('employee/common/top_header');
			$this->load->view('employee/common/header');
		    $this->load->view('employee/common/leftbar');
		      $this->load->view('employee/about_us/add_aboutus_professors');
		      $this->load->view('employee/common/footer');
	  	}
	  }
		public function edit_professors($id)
		{
			if($_POST)
	  	{
	  		$message_details = array(
	          	'name' => $this->input->post('name'), 
	          	'college_name' => $this->input->post('college_name'),
	          	'message' => $this->input->post('message'),
                'updated_date'  => Date('Y-m-d H:i:s'),
	          	);
	  		// print_r($message_details);exit();
	          $res = $this->Aboutus_model->update_aboutus($message_details,$id,'aboutus_professors');
	    
	          if($res > 0){
	          	$filename = $_FILES['photo']['name'];
            $flag = "aboutus_professors";
            $add_news['user_id'] = "user_image/".$res;
            $add_news['flag'] = $flag;
            if(!empty($_FILES['photo']['name']))
            {
                $target_file = $this->uploader->upload_image($_FILES['photo'], $flag,$add_news);
                $this->db->where('id',$res);
                $this->db->update('aboutus_professors', 
                    array( 'photo' => $target_file['profile_org_url'], ));
            }
        }
	    redirect('employee/Home/manage_aboutus_professors');
	         
	  	}
	  	else{ 
	  		$data['message'] = $this->Aboutus_model->aboutus_professors_byid($id);
	    
	  		$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
				$this->ckeditor->config['toolbar'] = 'Full';
				$this->ckeditor->config['language'] = 'en';
				$this->ckeditor->config['width'] = '730px';
				$this->ckeditor->config['height'] = '300px';            

				//Add Ckfinder to Ckeditor
				$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');
			$this->load->view('employee/common/top_header');
			$this->load->view('employee/common/header');
		    $this->load->view('employee/common/leftbar');
		      $this->load->view('employee/about_us/edit_aboutus_professors',$data);
		      $this->load->view('employee/common/footer');
	  	}
		}
		public function delete_professors($id)
		{
			$this->Aboutus_model->delete_aboutus($id,'aboutus_professors');
			redirect('employee/Home/manage_aboutus_professors');
		}



		public function manage_aboutus_students()
		{
	    $data['message'] = $this->Aboutus_model->aboutus_students();
	    // print_r($data);exit();
		$this->load->view('employee/common/top_header');
		$this->load->view('employee/common/header');
	    $this->load->view('employee/common/leftbar');
	    $this->load->view('employee/about_us/students',$data);
	    $this->load->view('employee/common/footer');
		}
	  public function add_aboutus_students()
	  {
	  	if($_POST)
	  	{
	  		$message_details = array(
	          	'name' => $this->input->post('name'), 
	          	'college_name' => $this->input->post('college_name'),
	          	'message' => $this->input->post('message'),
	          	'added_date'  => Date('Y-m-d H:i:s'),
                'updated_date'  => Date('Y-m-d H:i:s'),
	          	);
	  		// print_r($message_details);exit();
	          $res = $this->Aboutus_model->insert_aboutus($message_details,'aboutus_students');
	    
	          if($res > 0){
	          	$filename = $_FILES['photo']['name'];
            $flag = "aboutus_students";
            $add_news['user_id'] = "user_image/".$res;
            $add_news['flag'] = $flag;
            if(!empty($_FILES['photo']['name']))
            {
                $target_file = $this->uploader->upload_image($_FILES['photo'], $flag,$add_news);
                $this->db->where('id',$res);
                $this->db->update('aboutus_students', 
                    array( 'photo' => $target_file['profile_org_url'], ));
            }
        }
	    redirect('employee/Home/manage_aboutus_students');
	         
	  	}
	  	else{
	  		$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
				$this->ckeditor->config['toolbar'] = 'Full';
				$this->ckeditor->config['language'] = 'en';
				$this->ckeditor->config['width'] = '730px';
				$this->ckeditor->config['height'] = '300px';            

				//Add Ckfinder to Ckeditor
				$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');
			$this->load->view('employee/common/top_header');
			$this->load->view('employee/common/header');
		    $this->load->view('employee/common/leftbar');
		      $this->load->view('employee/about_us/add_aboutus_students');
		      $this->load->view('employee/common/footer');
	  	}
	  }
		public function edit_students($id)
		{
			if($_POST)
	  	{
	  		$message_details = array(
	          	'name' => $this->input->post('name'), 
	          	'college_name' => $this->input->post('college_name'),
	          	'message' => $this->input->post('message'),
                'updated_date'  => Date('Y-m-d H:i:s'),
	          	);
	  		// print_r($message_details);exit();
	          $res = $this->Aboutus_model->update_aboutus($message_details,$id,'aboutus_students');
	    
	          if($res > 0){
	          	$filename = $_FILES['photo']['name'];
            $flag = "aboutus_students";
            $add_news['user_id'] = "user_image/".$res;
            $add_news['flag'] = $flag;
            if(!empty($_FILES['photo']['name']))
            {
                $target_file = $this->uploader->upload_image($_FILES['photo'], $flag,$add_news);
                $this->db->where('id',$res);
                $this->db->update('aboutus_students', 
                    array( 'photo' => $target_file['profile_org_url'], ));
            }
        }
	    redirect('employee/Home/manage_aboutus_students');
	         
	  	}
	  	else{ 
	  		$data['message'] = $this->Aboutus_model->aboutus_students_byid($id);
	    
	  		$this->ckeditor->basePath = base_url().'assets_employee/ckeditor/';
				$this->ckeditor->config['toolbar'] = 'Full';
				$this->ckeditor->config['language'] = 'en';
				$this->ckeditor->config['width'] = '730px';
				$this->ckeditor->config['height'] = '300px';            

				//Add Ckfinder to Ckeditor
				$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets_employee/ckfinder/');
			$this->load->view('employee/common/top_header');
			$this->load->view('employee/common/header');
		    $this->load->view('employee/common/leftbar');
		      $this->load->view('employee/about_us/edit_aboutus_students',$data);
		      $this->load->view('employee/common/footer');
	  	}
		}
		public function delete_students($id)
		{
			$this->Aboutus_model->delete_aboutus($id,'aboutus_students');
			redirect('employee/Home/manage_aboutus_students');
		}
		public function edit_home_image($id)
		{
			if($_POST){
			$name="home_image";
			if($_FILES['home_image']['name']){
						$filename = $_FILES['home_image']['name'];
						$flag = "post";
						$add_gallery['user_id'] = "home_image/".$name;
						$add_gallery['flag'] = $flag;
						$count = count($_FILES['home_image']['name']);
						//print_r($_FILES['home_image']);exit();
							$flag = "post";
							$add_gallery['user_id'] = "home_image/".$name;
							$add_gallery['flag'] = $flag;
								$image['name'] = $_FILES['home_image']['name'];
								$image['type'] = $_FILES['home_image']['type'];
								$image['tmp_name'] = $_FILES['home_image']['tmp_name'];
								$image['error'] = $_FILES['home_image']['error'];
								$image['size'] = $_FILES['home_image']['size'];
								$target_file = $this->uploader->upload_image($image, $flag,$add_gallery); 
					$data1=array(
						'title' => $this->input->post('title'),
						'content' => $this->input->post('content'),
						'alt' => $this->input->post('alt'),
						'image'=>$target_file['profile_org_url'],
						'created_at'  => date("Y-m-d H:i:s"),
					);
					} 
					else{
						$data1=array(
						'title' => $this->input->post('title'),
						'content' => $this->input->post('content'),
						'alt' => $this->input->post('alt'),
						'created_at'  => date("Y-m-d H:i:s"),
					);
					}
					$this->db->where('id',$id);
					$this->db->update('home_image', $data1);
			redirect('employee/home');
			}
			else{
			$data['image'] = $this->Home_model->get_all_home_image_byid($id);
				$this->load->view('employee/common/top_header'); 
			$this->load->view('employee/common/header');
			$this->load->view('employee/common/leftbar');
			$this->load->view('employee/common/footer');
			$this->load->view('employee/home/edit_home_image',$data);
			}
		} 
		public function due_payment(){
			$data['pay'] = $this->Home_model->get_all_due_payment();
				$this->load->view('employee/common/top_header'); 
			$this->load->view('employee/common/header');
			$this->load->view('employee/common/leftbar');
			$this->load->view('employee/common/footer');
			$this->load->view('employee/home/due_payment',$data);
		}
		public function add_due_payment(){
			if(!empty($_POST['email']) && !empty($_POST['amount']))
		  	{
		  		$amount = array(
		          	'email' => $this->input->post('email'), 
		          	'phone' => $this->input->post('phone'),
		          	'amount' => $this->input->post('amount'),
		          	'created_at'  => Date('Y-m-d H:i:s'),
	                'updated_at'  => Date('Y-m-d H:i:s'),
		          	);
		  		// print_r($message_details);exit();
		          $this->db->insert('due_payment',$amount);
		          $insert = $this->db->insert_id();
		          if(!empty($insert)){
		          	$id=$insert;
	    	$email=$_POST['email'];
	    	$amount=$_POST['amount'];
	    	$phone=$_POST['phone'];
	    	// print_r($_POST);exit();
	    	$data['link']=base_url()."Home/due_payment/".base64_encode($id)."/".base64_encode($email)."/".base64_encode($amount)."/".rtrim(base64_encode($phone),'=');
   			$message=$this->load->view('email/payment_link',$data,TRUE);
//print_r($data['link']);exit();
	    	// $to="developer10@indglobal-consulting.com";
	    	$to = $email;

                     $headers = 'From: alerts@gsper.com' . "\r\n" .			    'Reply-To: alerts@gsper.com' . "\r\n" .			    'X-Mailer: PHP/' . phpversion();
			    $headers  .= 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    $mail = mail($to,"Payment Link:GSPER",$message,$headers);
		          }
		    
		          
	       // }
		    redirect('employee/Home/due_payment');
		         
		  	}
		  	else{
		  		
				$this->load->view('employee/common/top_header');
				$this->load->view('employee/common/header');
			    $this->load->view('employee/common/leftbar');
			      $this->load->view('employee/home/add_due_payment');
			      $this->load->view('employee/common/footer');
		  	}
		}
		public function subscribe(){
			$data['subscribe'] = $this->Home_model->get_all_subscribe();
			$this->load->view('employee/common/top_header');
				$this->load->view('employee/common/header');
			    $this->load->view('employee/common/leftbar');
			      $this->load->view('employee/home/subscribe',$data);
			      $this->load->view('employee/common/footer');
		}
		public function delete_subscribe($id){
			if(isset($id)){
				$this->db->where('id',$id);
				$this->db->delete('subscribe');
				redirect('employee/Home/subscribe');
			}
		}
		public function add_subscribe(){
			if(!empty($_POST['email']) )
		  	{
		  		$amount = array(
		          	'email' => $this->input->post('email'), 
		          	
		          	'created_at'  => Date('Y-m-d H:i:s'),
		          	);
		  		// print_r($message_details);exit();
		          $this->db->insert('subscribe',$amount);
		          $insert = $this->db->insert_id();
		         

		    redirect('employee/Home/subscribe');
		         
		  	}
		  	else{
		  		
				$this->load->view('employee/common/top_header');
				$this->load->view('employee/common/header');
			    $this->load->view('employee/common/leftbar');
			      $this->load->view('employee/home/add_subscribe');
			      $this->load->view('employee/common/footer');
		  	}

		}

	}
?>