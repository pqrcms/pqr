<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	Class Login extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->view('employee/common/top_header');
			$this->load->model('employee/Login_model');
		}

		public function index(){
			if(!empty($_SESSION['employee_logged_in']['user_id'])){
				redirect('employee/dashboard');
			} else{ 
				$this->load->view('employee/login');
				$this->load->view('employee/common/footer');
			}	
		}

		public function signin_form()
		{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');	
		$email_address = $_POST['email'];
		$password = $_POST['password'];

		//print_r($_POST);die();
		$valid_check = $this->Login_model->check_credentials($email_address,$password);

			if($valid_check == true)
			{
				$data = array(
				'user_id' => $valid_check->emp_id,
				'email_address' => $valid_check->email,
				'is_logged_in' => true);
			    $this->session->set_userdata('employee_logged_in', $data);
				echo 1; die();
			}
			else
			{
				echo 2; die();
			}
		}

	public function logout()
	{
		$this->session->unset_userdata('employee_logged_in');
		$this->load->view('employee/login');
		$this->load->view('employee/common/footer');
	}	


	}
?>