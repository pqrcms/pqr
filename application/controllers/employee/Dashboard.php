<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	Class Dashboard extends CI_Controller{

		public function __construct(){
			parent::__construct();
			//$this->load->model('Home_model');
			if(empty($_SESSION['admin_logged_in']['user_id'])){
				redirect('employee'); 
			}
		}

		public function index(){
			// $data['user'] = $this->Home_model->get_total_students();
			// $data['reviewer'] = $this->Home_model->get_total_reviewer();
			// $data['paper'] = $this->Home_model->get_paper_received();
			// $data['published'] = $this->Home_model->get_paper_published();
			$this->load->view('employee/common/top_header'); 
	        $this->load->view('employee/common/header');
	        $this->load->view('employee/common/leftbar');
	        $this->load->view('employee/dashboard');
	        $this->load->view('employee/common/footer');  
		}
	}
?>