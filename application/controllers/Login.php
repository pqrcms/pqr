<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	Class Login extends CI_Controller{

		public function __construct(){
			parent::__construct();
		    $this->load->library('session');
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->model('user/User_model');
		}

		public function index(){
			if(!empty($_SESSION['is_userlogged_in']['user_id'])){
				redirect('user/user/home');
			} else{ 
				 $this->load->view('user/index');
			}	
		}
public function validate_credentials() 
    {     
    	//print_r($_POST);die();
        $this->load->model('User_model'); 
         
        $email = $this->input->post('email'); 
        $password = md5($this->input->post('password')); 
        
        $is_valid = $this->User_model->validate($email, $password); 
        if($is_valid) 
        { 
               	$data = array( 
            		'email' => $email,
                    'id'=>     $is_valid->emp_id,
                    'user_id' =>$is_valid->employee_id,
                    'active'=>$is_valid->active,
            		'is_userlogged_in' => true); 

            	$this->session->set_userdata('is_userlogged_in', $data); 
                //$this->data['is_userlogged_in']=$this->session->userdata['is_userlogged_in'];
                //print_r($this->data['is_userlogged_in']);die();
            echo 1; 
        }else{ 
            echo 2; 
        } 
    }
    public function validate_email(){
    	//print_r($_POST);die();
        $this->load->model('User_model'); 
        $email = $this->input->post('email'); 
        $is_valid = $this->User_model->validate_email($email); 

        if($is_valid) 
        { 
            $hash_code = md5(uniqid());
            $code["hash_code"] = $hash_code;  
            $this->db->where('email', $email);
            $update = $this->db->update('emp_details', $code); 
            $email = $email;
            $to =  $email;
            $base = base_url();
            $link = $base."user/user/reset_password?email=".base64_encode($email)."&hash=".$hash_code; 
            $order = array('email' => $email,'link' => $link);
            $message = $this -> load -> view('email/forgot_email',$order,TRUE);
            print_r($message);die();
            //$this ->User_model->send_mail('pqr@mailinator.com',$to,'Reset Password Link',$message);
            echo 1; 
        }else{
            echo 2; 
        }
    }
		public function signin_form()
		{
		/*$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');	
		$email_address = $_POST['email'];
		$password = $_POST['password'];

		//print_r($_POST);die();
		$valid_check = $this->Login_model->check_credentials($email_address,$password);

			if($valid_check == true)
			{
				$data = array(
				'user_id' => $valid_check->emp_id,
				'email_address' => $valid_check->email,
				'is_logged_in' => true);
			    $this->session->set_userdata('employee_logged_in', $data);
				echo 1; die();
			}
			else
			{
				echo 2; die();
			}*/
		}

    public function logout(){
        $this->session->unset_userdata(array("is_userlogged_in"=>"","password"=>"","email"=>""));
        $this->session->sess_destroy();
        redirect('login');
    }


	}
?>