

    <!-- Bootstrap 3.3.5 -->
   <script src="<?php echo base_url('assets_admin/js/bootstrap.min.js'); ?>"></script>
    <!-- DataTables -->
   <script src="<?php echo base_url('assets_admin/js/jquery.dataTables.min.js'); ?>"></script>
   <script src="<?php echo base_url('assets_admin/js/dataTables.bootstrap.min.js'); ?>"></script>
    <!-- SlimScroll -->
   <script src="<?php echo base_url('assets_admin/js/jquery.slimscroll.min.js'); ?>"></script>
    <!-- FastClick -->
   <script src="<?php echo base_url('assets_admin/js/fastclick.min.js'); ?>"></script>
    <!-- AdminLTE App -->
   <script src="<?php echo base_url('assets_admin/js/app.min.js'); ?>"></script>
   <script src="<?php echo base_url('assets_admin/js/bootstrap-datepicker.min.js'); ?>"></script>
    <!-- AdminLTE for demo purposes -->
   <script src="<?php echo base_url('assets_admin/js/demo.js'); ?>"></script>
   <script src="<?php echo base_url(); ?>assets_admin/js/icheck.min.js"></script>

    <!-- page script -->
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
      <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
      <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>      
      <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script>
       $(function () {
        $("#example2").DataTable();
        $("#example1").DataTable({
          "autoWidth": false,
scrollX: 'false',
              dom: 'Bfrtip',
              buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
              ]
        });
       } );


    
      
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#journal_id').on('change',function(){
    //alert(12);
        var j_id = $(this).val();
        //alert(j_id);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('admin/digital/get_latest_volume') ?>",
            data:{j_id:j_id},
            dataType: 'json',
            success:function(res){
              
              
              
                $(".vol_value").val(res.vol);
                $(".vol_issue").val(res.iss);
            }
        });
    });

       $('#journal_volume').on('change',function(){
    //alert(12);
        var j_id = $(this).val();
        //alert(j_id);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('admin/journal/get_journal_volume') ?>",
            data:{j_id:j_id},
            dataType: 'json',
            success:function(res){
              //console.log(res);
              var str=`<option value="">Select Volume</option>`;
                $.each(res.value, function(key,val)
                  {
                    
                    str +=  `<option value="${val.volume}">${val.volume}</option>`;
                  });
                $('#select_volume').html(str);
            }
        });
    });

       $('#select_volume').on('change',function(){
    //alert(12);
        var volume = $(this).val();
        //alert(volume);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('admin/journal/get_journal_volume_id') ?>",
            data:{volume:volume},
            dataType: 'json',
            success:function(res){
              //console.log(res);
              
                $('#volume').val(res.volume);
            }
        });
    });

        $('#journal_rightpanel').on('change',function(){
    //alert(12);
        var j_id = $(this).val();
        //alert(volume);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('admin/journal/get_right_panel_types') ?>",
            data:{j_id:j_id},
            dataType: 'json',
            success:function(res){
              //console.log(res);
              
                $('#volume').val(res.volume);
                $('#change_volume').val(res.volume);
            }
        });
    });

         $('#change_volume').on('click',function(){
    //alert(12);

        var j_id = $(this).val();
        //alert(volume);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('admin/journal/get_right_panel_types') ?>",
            data:{j_id:j_id},
            dataType: 'json',
            success:function(res){
              //console.log(res);
              
                $('#volume').val(res.volume);
            }
        });
    });

         $('#select_country').on('change',function(){
    //alert(12);
        var id = $(this).val();
        //alert(j_id);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('admin/conference/get_state_by_country') ?>",
            data:{id:id},
            dataType: 'json',
            success:function(res){
              console.log(res);
              var str=`<option value="">Select State</option>`;
                $.each(res.state, function(key,val)
                  {
                    
                    str +=  `<option value="${val.name}">${val.name}</option>`;
                  });
                $('#select_state').html(str);
            }
        });
    });

         $('#select_state').on('change',function(){
    //alert(12);
        var id = $(this).val();
        //alert(j_id);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('admin/conference/get_city_by_state') ?>",
            data:{id:id},
            dataType: 'json',
            success:function(res){
              console.log(res);
              var str=`<option value="">Select City</option>`;
                $.each(res.city, function(key,val)
                  {
                    
                    str +=  `<option value="${val.name}">${val.name}</option>`;
                  });
                $('#select_city').html(str);
            }
        });
    });

          $('#conference_session').on('change',function(){
    //alert(12);
        var id = $(this).val();
        //alert(id);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('admin/conference/get_dates_by_conference') ?>",
            data:{id:id},
            dataType: 'json',
            success:function(res){
              console.log(res);
              var str=`<option value="">Select Date</option>`;
                $.each(res.dates, function(key,val)
                  {
                    
                    str +=  `<option value="${val}">${val}</option>`;
                  });
                $('#session_date').html(str);
            }
        });
    });

      });


    </script>
  </body>
</html>
