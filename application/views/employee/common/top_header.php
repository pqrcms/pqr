
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>PQR</title>
      <link rel="icon" href="<?php echo base_url(); ?>assets_admin/image/foodstop.jpg" type="image/jpg">
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link href="<?php echo base_url();?>assets_admin/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo base_url();?>assets_admin/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="<?php echo base_url();?>assets_admin/css/ionicons.min.css">
      <link href="<?php echo base_url();?>assets_admin/css/dataTables.bootstrap.css" rel="stylesheet">
      <link href="<?php echo base_url();?>assets_admin/css/AdminLTE.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>assets_admin/css/_all-skins.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_admin/css/bootstrap-datepicker.min.css');?>">
      <!-- jQuery 2.1.4 -->
      <script src="<?php echo base_url('assets_admin/js/jQuery-2.1.4.min.js'); ?>"></script>  
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" >
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" >    
      <style>
            .error{
                  color:red;
            }

            .btn-circle {
                width: 30px;
                height: 30px;
                text-align: center;
                padding: 6px 0;
                font-size: 12px;
                line-height: 1.428571429;
                border-radius: 15px;
            }
            .btn-circle-out {
                  margin-top: 40px;
            }
            .btn-circle-in {
                  margin-top: 30px;
            }
      </style>
</head>