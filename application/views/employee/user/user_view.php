<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- general form elements -->
              <div class="box box-primary col-lg-1 col-md-1">
                <div class="box-header with-border">
                  <h2 class="box-title">User Profile of <b><?php echo $user->user_name; ?></b></h2>
                </div><!-- /.box-header -->
                <!-- form start -->
               
                  <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                            <tr>
                                <td>User Profile Image</td>
                                <td><img src="<?php  echo base_url().$user->user_image; ?>"></td>
                            </tr>
                            <tr>
                                <td>User Name</td>
                                <td><b><?php  echo $user->user_name; ?></b></td>
                            </tr>

                            <tr>
                                <td>User Email</td>
                                <td><b><?php  echo $user->user_email; ?></b></td>
                            </tr>

                            <tr>
                                <td>User Mobile</td>
                                <td><b><?php  echo $user->user_mobile; ?></b></td>
                            </tr>

                            <tr>
                                <td>User Location</td>
                                <td><b><?php  echo $user->user_location; ?></b></td>
                            </tr>

                            <tr>
                                <td>User DOB</td>
                                <td><b><?php  echo $user->user_dob; ?></b></td>
                            </tr>

                            <tr>
                                <td>User Gender</td>
                                <td><b><?php if($user->user_gender == 1) echo 'Male'; else 'Female';  ?></b></td>
                            </tr>

                            <tr>
                                <td>User College</td>
                                <td><b><?php  echo $user->user_college; ?></b></td>
                            </tr>

                            <tr>
                                <td>User Education</td>
                                <td><b><?php  echo $user->edu_name; ?></b></td>
                            </tr>

                            <tr>
                                <td>User Branch</td>
                                <td><b><?php  echo $user->dept_name; ?></b></td>
                            </tr>

                            <tr>
                                <td>User Batch</td>
                                <td><b><?php  echo $user->from_year.' - '.$user->to_year; ?></b></td>
                            </tr>

                            <tr>
                                <td>Facebook Link</td>
                                <td><b><?php  echo $user->facebook_link; ?></b></td>
                            </tr>

                            <tr>
                                <td>Twitter Link</td>
                                <td><b><?php  echo $user->twitter_link; ?></b></td>
                            </tr>

                            <tr>
                                <td>Instagram Link</td>
                                <td><b><?php  echo $user->instagram_link; ?></b></td>
                            </tr>
      
                            <tr>
                              <td>
                                  <a href='<?php echo base_url("admin/user/list"); ?>'><button class=' btn btn-danger'>Back</button></a>
                              </td>
                            </tr>
                       </table>
                  </div>
                    <!-- /.box-body -->
               
              </div><!-- /.box -->
			</div>


