<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<div class="content-wrapper">
<div class="container">
    <h1 class="well">Employee Registration Form</h1>
	<div class="col-lg-12 well">
	<div class="row">
				    <form enctype="multipart/form-data" method="POST" id="news" class="news" action="<?php echo base_url();?>admin/Emp_details/add">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>Employee First Name</label>
								<input type="text" placeholder="Enter First Name Here.." name="emp_name" class="form-control" value="<?php echo $this->input->post('emp_name'); ?>" />
								<span class="text-danger"><?php echo form_error('emp_name');?></span>
							</div>
							<div class="col-sm-6 form-group">
								<label>Employee Last Name</label>
								<input type="text" placeholder="Enter Last Name Here.." name="emp_last" class="form-control" value="<?php echo $this->input->post('emp_last'); ?>" />
								<span class="text-danger"><?php echo form_error('emp_last');?></span>
							</div>
						</div>					
						<div class="form-group">
							<label>Address</label>
							<textarea name="address" placeholder="Enter Address Here.." rows="3" class="form-control" value="<?php echo $this->input->post('address'); ?>"></textarea>
							<span class="text-danger"><?php echo form_error('address');?></span>
						</div>	
						<div class="row">
							<div class="col-sm-4 form-group">
								<label>City</label>
								<input type="text" placeholder="Enter City Name Here.." name="city" class="form-control" value="<?php echo $this->input->post('city'); ?>" />
								<span class="text-danger"><?php echo form_error('city');?></span>
							</div>	
							<div class="col-sm-4 form-group">
								<label>State</label>
								<input type="text" placeholder="Enter State Name Here.." name="state" class="form-control" value="<?php echo $this->input->post('state'); ?>" />
								<span class="text-danger"><?php echo form_error('state');?></span>
							</div>	
							<div class="col-sm-4 form-group">
								<label>Zip</label>
								<input type="text" placeholder="Enter Zip Code Here.." name="zip" class="form-control" value="<?php echo $this->input->post('zip'); ?>" />
								<span class="text-danger"><?php echo form_error('zip');?></span>
							</div>		
						</div>
						<div class="row">
							<div class="col-sm-4 form-group">
								<label>Designation</label>
								<input type="text" placeholder="Enter Designation Here.." name="position" class="form-control" value="<?php echo $this->input->post('position'); ?>" />
							<span class="text-danger"><?php echo form_error('position');?></span>
							</div>		
							<div class="col-sm-4 form-group">
								<label>Department</label>
								<input type="text" placeholder="Enter Department Here.." name="department" class="form-control" value="<?php echo $this->input->post('department'); ?>" />
								<span class="text-danger"><?php echo form_error('department');?></span>
							</div>
							<div class="col-sm-4 form-group">
								<label>Date Of Join</label>
								<input type="date" name="doj" class="form-control" value="<?php echo $this->input->post('doj'); ?>" />
							<span class="text-danger"><?php echo form_error('doj');?></span>
							</div>
						</div>						
					<div class="form-group">
						<label>Phone Number</label>
						<input type="text" placeholder="Enter Phone Number Here.." name="phone_num" class="form-control" value="<?php echo $this->input->post('phone_num'); ?>" />
						<span class="text-danger"><?php echo form_error('phone_num');?></span>
					</div>		
					<div class="form-group">
						<label>Email Address</label>
						<input type="email" placeholder="Enter Email Address Here.." name="email_id" class="form-control" value="<?php echo $this->input->post('email_id'); ?>" />
							<span class="text-danger"><?php echo form_error('email_id');?></span>
					</div>	
					<div class="form-group">
						<label>Password</label>
						<input type="password" placeholder="Enter Password Here.." name="password" class="form-control" value="<?php echo $this->input->post('password'); ?>" />
							<span class="text-danger"><?php echo form_error('password');?></span>
					</div>
					<div class="form-group">
						<label>Skills</label>
						<input type="text" placeholder="Enter Skills Here.." name="skills" class="form-control" value="<?php echo $this->input->post('skills'); ?>" />
							<span class="text-danger"><?php echo form_error('skills');?></span>
					</div>
					<button type="submit" class="btn btn-lg btn-info">Submit</button>					
					</div>
				</form> 
				</div>
	</div>
	</div>
	</div>

