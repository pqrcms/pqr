<!DOCTYPE html>
<html>
<head>
    <title>GSPER</title> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="763390043517-4b8mec11ivc6lj7a8cuui1ukltbir1tf.apps.googleusercontent.com">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/meanmenu.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hover.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliderstyle.css">    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <!--link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/countdown/css/jquery.countdownTimer.css" /-->
    <style type="text/css">
      
    #customBtn {
      display: inline-block;
      background: white;
      color: #444;
      width: 190px;
      border-radius: 5px;
      border: thin solid #888;
      box-shadow: 1px 1px 1px grey;
      white-space: nowrap;
    }
    #customBtn:hover {
      cursor: pointer;
    }
    span.label {
      font-family: serif;
      font-weight: normal;
    }
    span.icon {
      background: url('/identity/sign-in/g-normal.png') transparent 5px 50% no-repeat;
      display: inline-block;
      vertical-align: middle;
      width: 42px;
      height: 42px;
    }
    span.buttonText {
      display: inline-block;
      vertical-align: middle;
      padding-left: 42px;
      padding-right: 42px;
      font-size: 14px;
      font-weight: bold;
      /* Use the Roboto font that is loaded in the <head> */
      font-family: 'Roboto', sans-serif;
    }

    </style>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104669135-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>
    <div id="hand-over"></div>
   <header id="header">
        <div class="header_top">
           <div class="container">
            <div class="row">
              <div class="hidden-xs col-sm-12 col-md-12">
                <ul class="list-inline top_list">
                  <li>
                    <a href="<?php echo base_url('user/home/blog'); ?>">BLOG</a>
                  </li>
                  <li>
                    <a href="<?php //echo base_url('user/home/doi'); ?>">DOI</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url('user/home/contact'); ?>">CONTACT US</a>
                  </li>
                </ul>
              </div>
            </div>
           </div>
        </div>
        <div class="header_bottom">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-3 col-md-3 hidden-xs">
                <div class="contact_no">
                  <div class="cont_img">
                    <img src="<?php echo base_url(); ?>assets/images/contact_icon.png" alt="">
                  </div>
                  <div class="cont_text">
                    <h4>Contact</h4>
                    <p>+91 98456 98456</p>
                  </div>
                </div>
              </div>
              <div class="hidden-xs col-sm-6 col-md-6">
                <div class="logo_cont">
                  <a href="<?php echo base_url('user/home'); ?>">
                    <img src="<?php echo base_url(); ?>assets/images/logo.png" class="" alt="GSPER" width="150">
                  </a>
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 hidden-xs">
                <div class="mail_cont">
                  <div class="mail_img">
                    <img src="<?php echo base_url(); ?>assets/images/mail_icon.png" class="img-responsive" alt="">
                  </div>
                  <div class="mail_text">
                    <h4>Mail Us</h4>
                    <p>info@gsper.com</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-2 col-sm-1 col-md-2">
                <div class="menu_cont">
                  <!-- <h4><a href="javascript:void(0);"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;<span  class="hidden-xs hidden-sm">MENU</span></a></h4> -->
                  <span style="font-size:25px;cursor:pointer" onclick="openNav()">&#9776; <label class="hidden-xs hidden-sm">Menu</label></span>
                  <!-- <div class="megamenu no_mar_pad"> -->
                  <div id="mySidenav" class="sidenav">
                      <ul class="unstyled">

                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="<?php echo base_url('user/home/about'); ?>">About Us</a></li>
                        <li><a href="<?php echo base_url('user/home/contact'); ?>">Contact Us</a></li>
                        <li><a href="<?php echo base_url('user/digital-library'); ?>">Digital Library</a></li>
                        <li><a href="<?php echo base_url('user/journal'); ?>">Journal</a></li>
                        <li><a href="<?php echo base_url('user/home/career'); ?>">Careers</a></li>
                        <li>
                          <ul class="menu-accrodion unstyled goal_list">
                            <li>
                              <h3>Guidlines <span class="pull-right menu-accrodion-plus"><i class="fa fa-plus" aria-hidden="true"></i></span></h3>
                                <div class="hide_show_data">
                                  <p>
                                    <ul>
                                          <li><a href="<?php echo base_url('user/home/guidelines'); ?>">Guidelines</a></li>
                                          <li><a href="<?php echo base_url('user/home/author_guidelines');?>">Author Guidelines</a></li>
                                          <li><a href="<?php echo base_url('user/home/peer_guidelines');?>">Peer Guidelines</a></li>
                                          <li><a href="<?php echo base_url('user/home/organizing_guidelines');?>">Organizing Guidelines</a></li>
                                          <li><a href="<?php echo base_url('user/home/publication_guidelines');?>">Publication Guidelines</a></li>
                                    </ul>
                                  </p>
                                </div>
                             </li>
                           </ul>
                        </li>
                        <li>
                            <ul class="menu-accrodion unstyled goal_list">
                              <li>
                                <h3>Policies <span class="pull-right menu-accrodion-plus"><i class="fa fa-plus" aria-hidden="true"></i></span></h3>
                                  <div class="hide_show_data">
                                    <p>
                                      <ul>
                                            <li><a href="<?php echo base_url('user/home/copyright_policy');?>">Copyright Policy</a></li>
                                            <li><a href="<?php echo base_url('user/home/plagiarism_policy'); ?>">Plagiarism Policy</a></li>
                                            <li><a href="<?php echo base_url('user/home/plublication_policy'); ?>">publication Policy</a></li>
                                            <li><a href="<?php echo base_url('user/home/policy'); ?>">Privacy Policy</a></li>
                                            <li><a href="<?php echo base_url('user/home/refund_policy'); ?>">Refund Policy</a></li>
                                      </ul>
                                    </p>
                                  </div>
                              </li>
                            </ul>
                        </li>
                        <li>
                          <ul class="menu-accrodion unstyled goal_list">
                              <li>
                            <h3>Open Access<span class="pull-right menu-accrodion-plus"><i class="fa fa-plus" aria-hidden="true"></i></span></h3>
                            <div class="hide_show_data">
                              <p>
                                <ul>
                                    <li><a href="<?php echo base_url('user/home/benefits_openaccess');?>">Benefits of open access</a></li>
                                    <li><a href="<?php echo base_url('user/home/why_openaccess');?>">why open access</a></li>
                                </ul>
                              </p>
                            </div>
                          </li>
                          </ul>
                        </li>
                        <li>
                          <ul class="menu-accrodion unstyled goal_list">
                           <li>
                             <h3>Membership <span class="pull-right menu-accrodion-plus"><i class="fa fa-plus" aria-hidden="true"></i></span></h3>
                              <div class="hide_show_data">
                               <p>
                                <ul>
                                      <li><a href="<?php echo base_url('user/home/professional_membership');?>">Professional Membership</a></li>
                                      <li><a href="<?php echo base_url('user/home/institute_membership');?>">Institutional Membership</a></li>
                                      <li><a href="<?php echo base_url('user/home/student_membership');?>">Student membership</a></li>
                                </ul>
                               </p>
                             </div>
                           </li>
                          </ul>
                        </li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="col-xs-5 hidden-sm hidden-md hidden-lg">
                 <div class="logo_cont">
                  <a href="<?php echo base_url('user/home'); ?>">
                    <img src="<?php echo base_url(); ?>assets/images/logo.png" class="" alt="GSPER" width="150">
                  </a>
                </div>
              </div>
              <div class="col-sm-5 col-md-6 hidden-xs">
                <ul class="list-inline exmenu_list">
                  <li>
                    <a href="<?php echo base_url('user/journal'); ?>">journal</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url('user/digital-library'); ?>">digital library</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url('user/home/guidelines'); ?>">guidelines</a>
                  </li>
                </ul>
              </div>
             <!-- <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="sign_cont">
                  <button class="btn btnLogin">LOGIN</button>
                  <button type="button" class="btn btn-primary btnRegister" data-toggle="modal" data-target="#RegisterModal">CREATE ACCOUNT</button>
                </div>
              </div> -->
              <div class="col-xs-5 col-sm-6 col-md-4">

                <div class="sign_cont">
                  <!-- <a href="" class="btn btnLogin" data-toggle="modal" data-target="#LoginModal">LOGIN</a>
                  <button type="button" class="btn btn-primary btnRegister" data-toggle="modal" data-target="#RegisterModal">CREATE ACCOUNT</button> -->
                  <ul class="inline">
                      <?php if($this->session->userdata('is_logged_in')){?> 
                      <p class="btn btnLogin dropdown-toggle SourceBold" data-toggle="dropdown"><?php echo $_SESSION['is_logged_in']['name']; ?>&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></p>
                          <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">
                              <li><a tabindex="-1" href="<?php echo base_url();?>user/student">Dashboard</a></li>
                              <li><a tabindex="-1" href="<?php echo base_url();?>student/logout">Logout</a></li>
                          </ul>
                          <?php } elseif($this->session->userdata('teacher_logged_in')){?> 
                      <p class="btn btnLogin dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['teacher_logged_in']['name']; ?>&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></p>
                        <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">
                            <li><a tabindex="-1" href="<?php echo base_url();?>reviewer_panel">Dashboard</a></li>
                            <li><a tabindex="-1" href="<?php echo base_url();?>reviewer/logout">Logout</a></li>
                        </ul>
                      <?php }else { ?>
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                          <!-- <a class="navbar-brand" href="#">MN</a> -->
                          <button class="navbar-toggler hidden-sm hidden-md hidden-lg" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="navbar-toggler-icon1"> 
                                 <a href="#"> <span class="glyphicon glyphicon-user"></span></a>
                              </span>
                          </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                              <ul class="navbar-nav mr-auto">
                                <li><a href="javascript:void(0);" class="btn btnLogin" data-toggle="modal" data-target="#LoginModal">LOGIN</a></li>
                                <li><p class="btn btnRegister" data-toggle="modal" data-target="#RegisterModal">CREATE ACCOUNT</p></li>
                                <li><a class="btn btnLogin" href="<?php echo base_url('user/reviewer'); ?>">REVIEWER</a></li>
                              </ul>
                            </div>
                        </nav>
                      <?php } ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade common_modal_style" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Login Here</h4>
          </div>
          <div class="modal-body">
            <form action="" id="floating-label" name="login_form" class="login_form">

              <div class="login_form_grp">
                <ul class="inline">
                  <li>
                    <div>
                      <input type="radio" class="custom_radio" id="login_student" name="login_as" value="1">
                      <label for="login_student" class="custom_radio_label"> Student</label>
                    </div>
                  </li>
                  <li>
                    <div>
                      <input type="radio" class="custom_radio" id="login_teacher" name="login_as" value="2">
                      <label for="login_teacher" class="custom_radio_label"> Teacher</label>
                    </div>
                  </li>
                  <li><span class="icon-bt-fb" onclick="Login();"><i class="fa fa-facebook" aria-hidden="true"></i> Login</span></li>
                </ul>
                <span class="error err_login_as"></span>
              </div>
              <div class="form-group">
                <div class="animated_form_group">
                  <label for="" class="control-label">Email</label>
                  <input type="email" class="form-control" name="email" id="email">
                </div>
                <span class="error err_email"></span>
              </div>
               <div class="form-group">
                <div class="animated_form_group">
                  <label for="" class="control-label">Password</label>
                  <input type="password" class="form-control" name="password" id="password">
                 
                </div>
                 <span class="error err_password"></span>
              </div>
              <div class="form-group">
                <div>
                <ul class="list-inline">
                  <li>
                  <input id="keep_me" type="checkbox" class="custom_check" name="keep_me">
                  <label for="keep_me" class="custom_check_label SourceBold">Keep Me Logged In</label>
                  <span class="error err_keep_me"></span>
                  </li>
                  <li style="float:right;">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#forgot_password" onclick="forgot_password();">Forgot Password?</a>
                  </li>
                  </ul>
                </div>
              </div>
              <div class="form-group">
                <ul class="list-inline">
                  <li><button type="submit" class="special_blue_btn">Login</button></li>
                  <li>Not Register? <a href="javascript:void(0);" data-toggle="modal" data-target="#RegisterModal">Click Here</a></li>
                  <!-- <li><p class="btn btnRegister" data-toggle="modal" data-target="#RegisterModal">CREATE ACCOUNT</p></li> -->
                </ul>
                <span class="error err_submit"></span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
 </header>

<section id="account-create">
     <div class="modal fade bs-example-modal-lg" id="RegisterModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-keyboard="false" data-backdrop="static">
          <div class="modal-dialog " role="document">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                      <div class="hidden-xs col-sm-4 col-md-4" id="accout-img">
                        <img src="<?php echo base_url(); ?>assets/images/Vector-Smart-Object2.png" alt="">
                      </div>
                      <div class="col-xs-12 col-sm-8 col-md-8">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><img src="<?php echo base_url(); ?>assets/images/close.png" alt=""></button>
                          <h3 class="modal-title">Create Account</h3>
                        </div>
                        <div>
                          <center class="social-icons">
                          <span class="sign_in_block cursor" id="register_fb">Facebook <i class="fa fa-facebook-square" aria-hidden="true"></i></span>
                          <span class="sign_in_block cursor" id="register_google"  data-onsuccess="onSignIn">Google<i class="fa fa-google-square" aria-hidden="true"></i></span>
                          <span class="sign_in_block cursor" id="register_linkedin">Linked <i class="fa fa-linkedin-square" aria-hidden="true"></i></span>
                          </center>
                       </div>
                                   <center><h4>OR</h4></center>
                        <form action="" id="register" name="register">
                          <input type="hidden" disabled class="sent_num" value="">
                          <input type="hidden"  class="" id="app_id" name="app_id" value="">
                          <div class="form-group">
                            <input type="text" id="firstname" name="firstname" class="form-control" placeholder="First name">
                            <span class="error err_firstname"></span>
                          </div>
                          <div class="form-group">
                            <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Last name">
                            <span class="error err_lastname"></span>
                          </div>
                          <div class="form-group">
                            <input type="text" placeholder="Email" name="uemail" id="uemail" class="form-control">
                    <span class="error err_uemail"></span>
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control pwd" id="passwords" name="password" placeholder="Enter Password">
                            <span class="error err_password"></span>
                          </div>
                           <div class="form-group">
                            <input type="password" class="form-control c_password" id="c_password" name="c_password" placeholder="Confirm Password">
                            <span class="error err_c_password"></span>
                          </div>
                          <div class="form-group">
                            <input type="text" name="phone" class="form-control" id="phone"  placeholder="Phone number">
                            <span class="error err_phone"></span>
                            <div class="verify-content" >
                              <a class="verify" href="#">verify</a>
                            </div>
                            <!-- <span class="error err_verify" id="ms_timer"></span> -->
                            <div id="ms_timer"></div>
                          </div>
                          <div class="form-group">
                            <input type="text" name="otp" id="otp" class="form-control otps" placeholder="Enter OTP">
                            <span class="error err_otp"></span>
                          </div>
                          
                          <!-- <div class="form-group">
                            <input type="file" class="form-control file-choose">
                          </div> -->
                          <div>
                              <input id="checkbox-2" class="checkbox-custom terms_check" name="keep_me" type="checkbox">
                              <label for="checkbox-2" class="checkbox-custom-label">I am agree to the GSPER <a href="<?php echo base_url('home/privacy'); ?>" target="_blank" >Terms and Privacy Policy.</a> </label>
                              <span class="error err_terms"></span>
                          </div>
                          
                          <button type="submit" class="btn" id="sub-mit">CREATE ACCOUNT</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</section>
<section>
    <div class="modal fade common_modal_style" id="forgot_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Forgot Password</h4>
          </div>
          <div class="modal-body">
            <form action="" id="" name="fp_form" class="login_form">
               <div class="login_form_grp">
                <ul class="inline">
                  <li>
                    <div>
                      <input type="radio" class="custom_radio" id="fp_student" name="fp_as" value="1">
                      <label for="fp_student" class="custom_radio_label">Student</label>
                    </div>
                  </li>
                  <li>
                    <div>
                      <input type="radio" class="custom_radio" id="fp_teacher" name="fp_as" value="2">
                      <label for="fp_teacher" class="custom_radio_label">Teacher</label>
                    </div>
                  </li>
                </ul>
                <span class="error fp_err_fp_as"></span>
              </div>
              <div class="form-group">
                <div class="animated_form_group">
                  <label for="" class="control-label">Enter Your Email Id</label>
                  <input type="email" class="form-control" name="forgot_email" id="forgot_email">
                </div>
                <span class="error fp_err_email"></span>
              </div>
               
             
              <div class="form-group">
                <ul class="list-inline">
                  <li><button type="submit" class="special_blue_btn">Submit</button></li>
                </ul>
                <span class="error err_submit"></span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script> -->
<script type="text/javascript"> var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"018b2ac0562c15242a61ebda8bee7c8f4c1e77d4a0d2851dda2ca666a650d4795d5fe629768af6d9a03793700d9418c2", values:{},ready:function(){}}; var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true; s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>"); </script>
