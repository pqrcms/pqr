<section class="no_mar_pad read_news">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<main class="white_bg_color">
					<center><h2 class="SourceSemiBold">Change Your password</h2 class="SourceSemiBold">
					<!-- <h5>20 May, 2017</h5> -->

          <div class="modal-body">
            <form action="" id="floating-label" name="change_password" class="change_password">
            <input type="hidden" class="form-control" name="utype" id="utype" value="<?php echo $utype;?>">
            <input type="hidden" class="form-control" name="email" id="email" value="<?php echo $email;?>">
              <div class="form-group">
                <div class="animated_form_group">
                  <label for="" class="control-label">New Password</label>
                  <input type="password" class="form-control" name="cpassword" id="cpassword">
                </div>
                <span class="error err_cpassword"></span>
              </div>
               <div class="form-group">
                <div class="animated_form_group">
                  <label for="" class="control-label">Enter Again Password</label>
                  <input type="password" class="form-control" name="ccpassword" id="ccpassword">
                 
                </div>
                 <span class="error err_ccpassword"></span>
              </div>
              <div class="form-group">
                <ul class="list-inline">
                  <li><button type="submit" class="special_blue_btn">Submit</button></li>
                </ul>
                <span class="error err_submit"></span>
              </div>
            </form>
          </div>

					</center>
				</main>
			</div>
		</div>
	</div>
</section>