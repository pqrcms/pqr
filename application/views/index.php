<!-- header file -->
<?php //print_r($home_image);exit();?>
<?php include 'header.php';?>

<section class="home_banner">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div id="homeCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Wrapper for slides -->
				  <?php //foreach ($home_image as  $h) { ?>
					<div class="carousel-inner" role="listbox">
						<?php $active = ''; foreach($home_image as $index => $h){  if($index == 0) { $active ="active"; } else $active ="";   ?>
					    <div class="item <?php echo $active;?>">
					    	<div class="row">
				
					    		<div class="col-xs-12 col-sm-5 col-md-5">
					    			<img src="<?php echo base_url($h->image); ?>" alt="" class="img-responsive">
					    		</div>
					    		<div class="col-xs-12 col-sm-7 col-md-7">
					    			<div class="item_text">
					    				<h2><?php echo $h->title; ?></h2>
					    				<p>
					    					<?php echo $h->content; ?>
					    				</p>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					    <?php } ?>
					</div>
					  <!-- Left and right controls -->
					<a class="left carousel-control" href="#homeCarousel" role="button" data-slide="prev">
					    <img src="<?php echo base_url(); ?>assets/images/left_arrow.png" alt="">
					</a>
					<a class="right carousel-control" href="#homeCarousel" role="button" data-slide="next">
					    <img src="<?php echo base_url(); ?>assets/images/right_arrow.png" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>
	<section class="ticker">
		<marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
			<div class="inner_marquee_text">
				<p>
					India&#39;s NO.1  Journal Publishing organisation Global Institute for Scientific Publication in Engineering and Research. The first ever user interface for Publishers and Reviewers, A New one of a kind Modern and Transparent way of publishing and indexing research work.
				</p>
			</div>
		</marquee>
	</section>
</section>
<section class="home_journal">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="journal_carousel">
					<div id="owl_carousel1" class="owl-carousel owl-theme">
						<?php foreach ($journal as  $j) { $j_code = $j->journal_code.'_'.$j->id; ?>
			            <div class="item">
			              <div class="item_cont">
			              	<figure class="pos-relation">
			              		<img src="<?php echo base_url($j->journal_image); ?>" alt="">
			              		<figcaption>
			              			<h1><?php echo $j->title;?>(<?php echo $j->journal_code;?>)</h1>
			              			<h5><?php echo $j->impact_factor;?></h5>
			              			<h5><?php echo $j->issn_print;?></h5>
			              			<a href="<?php echo base_url('user/journal/journal_details/').$j_code ?>" class="hover_efect_button">Read More</a>
			              		</figcaption>
			              	</figure>
			              </div>
			            </div>
			            <?php } ?>
			        </div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="chooseus">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="choose_cont">
					<h2>Why Choose Us</h2>
					<ul class="choose_cat_list">
						<li>
							<div class="choose_cat cat1">
								<h3><?php echo $choose->title_1;?></h3>
								<p><?php echo $choose->content_1;?></p>
							</div>
						</li>
						<li>
							<div class="choose_cat cat2">
								<h3><?php echo $choose->title_2;?></h3>
								<p><?php echo $choose->content_2;?></p>
							</div>
						</li>
						<li>
							<div class="choose_cat cat3">
								<h3><?php echo $choose->title_3;?></h3>
								<p><?php echo $choose->content_3;?></p>
							</div>
						</li>
						<li>
							<div class="choose_cat cat4">
								<h3><?php echo $choose->title_4;?></h3>
								<p><?php echo $choose->content_4;?></p>
							</div>
						</li>
						<li>
							<div class="choose_cat cat5">
								<h3><?php echo $choose->title_5;?></h3>
								<p><?php echo $choose->content_5;?></p>
							</div>
						</li>
						<li>
							<div class="choose_cat cat6">
								<h3><?php echo $choose->title_6;?></h3>
								<p><?php echo $choose->content_6;?></p>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="achievement">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="achiement_cont digital_achivement">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-3">
							<div class="achiement_item_cont">
								<img src="<?php echo base_url(); ?>assets/images/article_icon.png" alt="">
								<h3><span class="counter"><?php echo $article->count;?></span>+</h3>
								<p>Articles Archieved</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3">
							<div class="achiement_item_cont">
								<img src="<?php echo base_url(); ?>assets/images/author_icon.png" alt="">
								<h3><span class="counter"><?php echo $authors->count; ?></span>+</h3>
								<p>Our Authors</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3">
							<div class="achiement_item_cont">
								<img src="<?php echo base_url(); ?>assets/images/reviewer_icon.png" alt="">
								<h3><span class="counter"><?php echo $reviwers->count; ?></span>+</h3>
								<p>Reviewers</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3">
							<div class="achiement_item_cont">
								<img src="<?php echo base_url(); ?>assets/images/views_icon.png" alt="">
								<h3><span class="counter"><?php echo $views->count; ?></span>+</h3>
								<p>Views Per Month</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="combineSection">
	<div class="flex">
		<div class="seventy">
			<div class="flex video_flex">
				<div class="sixty pos-relation common_flex_style bg">
					<div class="video_cont">
							<a data-toggle="modal" data-target="#cor_video_modal">
								<main><img src="<?php echo base_url(); ?>assets/images/play_video.png" alt="" class="play_video_button"></main>
							</a>
						<div id="cor_video_modal" class="modal fade" role="dialog">
							<div class="modal-dialog model-lg">
								<div class="modal-content">
								<div class="modal-body">
									<iframe data-url="https://www.youtube.com/embed/ffes87Md39Y" allowfullscreen="" width="100%" height="400" frameborder="0"></iframe>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="forty blue_bg pos-relation common_flex_style center" >
					<?php if($this->session->userdata('is_logged_in')){?> 
					<a href="<?php echo base_url();?>user/student"> 
						<main>

							<img  src="<?php echo base_url(); ?>assets/images/icons/paper_img.png" width="100">
							<h3>Paper Submission</h3>
						</main>
					</a>
					<?php } else {?>
					<a href="javascript:void(0);" class="btn btnLogin" data-toggle="modal" data-target="#LoginModal">
						<main>

							<img  src="<?php echo base_url(); ?>assets/images/icons/paper_img.png" width="100">
							<h3>Paper Submission</h3>
						</main>
						<?php } ?>
				</div>
			</div>
			<div class="flex">
				<div class="fifty">
					<div class="gallery pos-relation common_flex_style hidden-xs">
						<a href="<?php base_url();?>/home/gallery" class="SouceBold">
							<main><h1>Gallery</h1></main>
						</a>
					</div>
				</div>
				<div class="fifty sky_blue pos-relation pos-relation common_flex_style hidden-xs">
					<a href="" class="SouceBold">
						<main><h1>Open <span><i class="fa fa-key" aria-hidden="true"></i></span> Access</h1></main>
					</a>
				</div>
			</div>
		</div>
		<div class="thirty join_as pos-relation common_flex_style hidden-xs">
			<a href="<?php echo base_url("reviewer_registration"); ?>" class="SouceBold"><main><h1>Join As Reviewer</h1></main></a>
		</div>
	</div>
</div>
<section class="newsletter flex no_mar_pad">
	<div class="seventy hidden-xs">
		<main class="white_bg_color over_scroll">
			<ul class="unstyled">
				<?php foreach ($news as  $n) { ?>
					<li>
						<article>
							<div class="row">
								<div class="col-md-3">
									<img src="<?php echo base_url($n->image); ?>" width="150px" height="120px" alt="<?php echo $n->alt;?>">
								</div>
								<div class="col-md-9">
									<h3 class="SourceBold"><?php echo $n->title; ?></h3>
									<p>
										<span><?php echo $n->date; ?></span>
										<a href="<?php echo base_url('read-more'); ?>" target="_blank" class="pull-right">Read More...</a>
									</p>
								</div>
							</div>
						</article>
					</li>
				<?php } ?>
			</ul>
		</main>
	</div>
	<div class="thirty">
		<h2>Sign Up for the GSPER Newsletter</h2>
		<p>Stay tune with Gsper get the latest news, notifications, upcoming conferences,
events and many more .....</p>
		<form action="" name="subscribe" id="subscribe" class="form_custom">
			<input type="email" class="input_text" name="s_email" id="s_email">
			<span class="error err_email"></span>
			<button type="Submit" class="btnSubmit pos-relation sweep-to-bottom">SUBMIT</button>
		</form>
	</div>
</section>
<section class="speach_sec">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<h1>What Professors &amp; Scientists Speak about us</h1>
				<div class="professor_testimonial">
					<div class="owl-carousel owl-theme prof_carousel">
						<?php if(!empty($professor)){ foreach ($professor as $p) { ?>
						<div class="item">
							<div class="single-item text-center">
								<div class="img-holder">
									<img src="<?php echo base_url($p->photo); ?>" alt="">
								</div>
								<div class="text-holder">
									<h5><?php echo $p->message; ?></h5>
								</div>
								<div class="client-info">
									<h4><a href="javascript:void(0);"><?php echo $p->name; ?></a></h4>
									<h5><?php echo $p->college_name; ?></h5>
								</div>
							</div>
						</div>
						<?php } }?>
						<!-- <div class="item">
							<div class="single-item text-center">
								<div class="img-holder">
									<img src="<?php echo base_url(); ?>assets/images/prof1.jpg" alt="">
								</div>
								<div class="text-holder">
									<h5>GSPER has introduce and new teacher panel which is revolutionary it is user friendly and provides the best experience for reviewing research work.</h5>
								</div>
								<div class="client-info">
									<h4><a href="javascript:void(0);">Dr. Vikram Joshi</a></h4>
									<h5>University of Calicut Malappuram</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-item text-center">
								<div class="img-holder">
									<img src="<?php echo base_url(); ?>assets/images/prof3.jpg" alt="">
								</div>
								<div class="text-holder">
									<h5>GSPER is providing better indexing and publication quality at a ease price and doing a marvellous job in the research and development field.</h5>
								</div>
								<div class="client-info">
									<h4><a href="javascript:void(0);">Dr. Mira Nair</a></h4>
									<h5>Osmania University Hyderabad</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-item text-center">
								<div class="img-holder">
									<img src="<?php echo base_url(); ?>assets/images/prof1.jpg" alt="">
								</div>
								<div class="text-holder">
									<h5>A easy tracking and transparent publication user interface has been developed by GSPER. Many Many thanks and congratulation  on their accomplishment.</h5>
								</div>
								<div class="client-info">
									<h4><a href="javascript:void(0);">Dr. Ali Khan</a></h4>
									<h5>VIT University Vellore</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-item text-center">
								<div class="img-holder">
									<img src="<?php echo base_url(); ?>assets/images/prof2.jpg" alt="">
								</div>
								<div class="text-holder">
									<h5>GSPER is not only changing the scientific era of publication but is also providing passive income to a number of teacher around the globe with their reviewer program and scheme.</h5>
								</div>
								<div class="client-info">
									<h4><a href="javascript:void(0);">Prof. Lupin</a></h4>
									<h5>Curtin University Malaysia</h5>
								</div>
							</div>
						</div> -->
						<!-- <div class="item">
							<div class="single-item text-center">
								<div class="img-holder">
									<img src="<?php echo base_url(); ?>assets/images/prof3.jpg" alt="">
								</div>
								<div class="text-holder">
									<h5>GSPER provides a common platform for like-minded researchers, scientists, academicians, students.</h5>
								</div>
								<div class="client-info">
									<h4><a href="javascript:void(0);">Prof. Ramanuja Acharya</a></h4>
									<h5>Bangalore University, Bangalore</h5>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="students_testi">
	<h2 class="text-center">What Our Students Speak about us</h2>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="students_carousel">
					<div id="owl_carousel2" class="owl-carousel owl-theme">
						<?php if(!empty($student)){ foreach ($student as $s) {?>
			            <div class="item">
			              <div class="item_cont">
			              	<div class="inner_cont stud1">
			              		<p><?php echo $s->message; ?></p>
			              	</div>
			              	<div class="stud_details">
			              		<div class="stud_title">
			              			<h4><?php echo $s->name; ?></h4>
			              			<p><?php echo $s->college_name; ?></p>
			              		</div>
			              		<div class="stud_img">
			              			<img src="<?php echo base_url($s->photo); ?>" alt="">
			              		</div>
			              	</div>
			              </div>
			            </div>
			            <?php }} ?>
			            <!-- <div class="item">
			              <div class="item_cont">
			              	<div class="inner_cont stud2">
			              		<p>GSPER is simplify amazing i heard about them from my teacher and submitted my research article was happy to see how much effort they put to publish my research work.</p>
			              	</div>
			              	<div class="stud_details">
			              		<div class="stud_title">
			              			<h4>J. Samira</h4>
			              			<p>Jain University Bangalore</p>
			              		</div>
			              		<div class="stud_img">
			              			<img src="<?php echo base_url(); ?>assets/images/student2.png" alt="">
			              		</div>
			              	</div>
			              </div>
			            </div>
			            <div class="item">
			              <div class="item_cont">
			              	<div class="inner_cont stud3">
			              		<p>GSPER is the best place to get your article published at fast space and reasonable registration fees. The service provided was more than satisfactory.</p>
			              	</div>
			              	<div class="stud_details">
			              		<div class="stud_title">
			              			<h4>Komal Thakur</h4>
			              			<p>R.V College of Engineering Bangalore</p>
			              		</div>
			              		<div class="stud_img">
			              			<img src="<?php echo base_url(); ?>assets/images/student3.png" alt="">
			              		</div>
			              	</div>
			              </div>
			            </div>
			            <div class="item">
			              <div class="item_cont">
			              	<div class="inner_cont stud1">
			              		<p>I have already published few research work earlier but never saw before GSPER has student panel which guides you on how to publish your paper, you have a complete knowledge on how and when your paper is going to publish.</p>
			              	</div>
			              	<div class="stud_details">
			              		<div class="stud_title">
			              			<h4>Subhas Mehta</h4>
			              			<p>Deccan College of Engineering Hyderabad</p>
			              		</div>
			              		<div class="stud_img">
			              			<img src="<?php echo base_url(); ?>assets/images/student1.png" alt="">
			              		</div>
			              	</div>
			              </div>
			            </div>
			            <div class="item">
			              <div class="item_cont">
			              	<div class="inner_cont stud1">
			              		<p>GSPER provided me an opportunity with virtual conference were I presented my paper has I am a foreign student and they did not had any conference at my country. I am so happy to come across them.</p>
			              	</div>
			              	<div class="stud_details">
			              		<div class="stud_title">
			              			<h4>Martin Norris</h4>
			              			<p>Monash University Melbourne</p>
			              		</div>
			              		<div class="stud_img">
			              			<img src="<?php echo base_url(); ?>assets/images/student1.png" alt="">
			              		</div>
			              	</div>
			              </div>
			            </div> -->
			        </div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- footer file -->
<?php include 'footer.php';?>