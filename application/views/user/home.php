
            <?php $this->load->view('common/header')?>
        <section id="home-main" class="common-bgcolor">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="test-list">


                            <?php foreach($ot_tasks as $o){ ?>
                            <li>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="custom-radio">
                                            <input id='test-<?php echo $o['id']; ?>' <?php if($o['check_status'] == 1){ echo "checked";} ?> class="checkbox-custom" name="test-name" type="checkbox">
                                            <label for='test-<?php echo $o['id']; ?>' class="checkbox-custom-label"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div data-val="dffsd" class="question"><?php echo $o['tasks']; ?></div>
                                    </div>

                                    <div class="col-md-3 main<?php echo $o['id']; ?>">
                                        <?php if($o['check_status'] == 0){ ?>
                                        <div class="submit" id="not_submit<?php echo $o['id']; ?>">
                                            <a href="javascript:void(0);" data-id = "<?php echo $o['id']; ?>" class="hover-bottom submits">Submit</a>
                                        </div>
                                        <?php } else { ?>
                                        <div class="submit" id="submited">
                                            <h5>Submitted On</h5>
                                            <small><span class='submited_on<?php echo $o['id']; ?>'></span>
                                                <?php echo $o['posted_date'] ?></small>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>


                            <!-- <li>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="custom-radio">
                                            <input id="test-2" class="checkbox-custom" name="test-name" type="checkbox" >
                                            <label for="test-2" class="checkbox-custom-label"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="question">Code of Conduct and Work Culture</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="submit">
                                           <a href="" onclick="test_Two()" id="submit2" class="hover-bottom">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                            <!-- <li>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="custom-radio">
                                            <input id="test-3" class="checkbox-custom" name="test-name" type="checkbox">
                                            <label for="test-3" class="checkbox-custom-label"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="question">Environment Introduction</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="submit">
                                            <a href="javascript:void(0);" onclick="test_Three()" id="submit3"class="hover-bottom">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                            <!-- <li>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="custom-radio">
                                            <input id="test-4" class="checkbox-custom" name="test-name" type="checkbox">
                                            <label for="test-4" class="checkbox-custom-label"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="question">IT Introduction</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="submit">
                                            <a href="javascript:void(0);" onclick="test_Four()" id="submit4" class="hover-bottom">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                            <!-- <li>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="custom-radio">
                                            <input id="test-5" class="checkbox-custom" name="test-name" type="checkbox">
                                            <label for="test-5" class="checkbox-custom-label"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="question">Safety Induction</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="submit">
                                            <a href="javascript:void(0);" onclick="test_Five()" id="submit5" class="hover-bottom">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                            <!-- <li>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="custom-radio">
                                            <input id="test-6" class="checkbox-custom" name="test-name" type="checkbox">
                                            <label for="test-6" class="checkbox-custom-label"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="question">Quality and EHSMS System</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="submit">
                                            <a href="javascript:void(0);" onclick="test_Six()" id="submit6" class="hover-bottom">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                            <!-- <li>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="custom-radio">
                                            <input id="test-7" class="checkbox-custom" name="test-name" type="checkbox">
                                            <label for="test-7" class="checkbox-custom-label"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="question">Understanding of Training and PQR system</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="submit">
                                            <a href="javascript:void(0);" onclick="test_Seven()" id="submit7" class="hover-bottom">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                            <!-- <li>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="custom-radio">
                                            <input id="test-8" class="checkbox-custom" name="test-name" type="checkbox">
                                            <label for="test-8" class="checkbox-custom-label"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="question">Plan Tour</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="submit">
                                            <a href="javascript:void(0);" onclick="test_Eight()" id="submit8" class="hover-bottom">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        
            <?php $this->load->view('common/footer')?>


<script>
$('.submits').click(function(e){ 
    var as = $(this);
    var datas = $(this).attr("data-id");
    // var data = document.getElementById("test-"+datas).checked;

    if(!$('#test-'+datas).is(':checked')){
    alert("Please Select Orientation Tasks");

    }else {
        alert("Selected");
        $.ajax({
        type: "POST",
        url: "<?php echo site_url()?>user/user/hr_submit/"+datas,
        success: function (response) {
          if (response!=1){
            var htm=   `<div class="submit" id="submited">
                                            <h5>Submitted On</h5>
                                            <small><span class='submited_on`+datas+`'></span>
                                                `+response+`</small>
                                        </div>`;
            $('.main'+datas).append(htm);
            // $(as).hide();
            $('#submited').show();
            $('#not_submit'+datas).hide();
            //$("#test-1").removeAttr("disabled");
            // $('#test-1').attr('disabled',true);
          }
          else if(response==1)
          {
                    swal({
                    title: "Success!",
                    text: "Successfully completed",
                    type: "success",
                    confirmButtonColor: '#aedef4',
                    confirmButtonText: 'OK',
                    },
                    function(isConfirm) {
                        if(isConfirm) {
                            window.location.href="<?php echo base_url('user/user/dashboard'); ?>"
                        }
                    }
                );
          }
        }
    });
    }
});

// function test_One() {
//     var x = document.getElementById("test-1").checked;
//     if(x == ""){
//         alert("Please Select Checkbox");
//     }else{
//         alert("Selected");
//     }
// }
</script>