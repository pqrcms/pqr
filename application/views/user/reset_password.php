<!DOCTYPE html>
<html>
      <head>
            <title>Emirates CMS Power Company</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/reset.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/resources/bootstrap/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/resources/font-awesome/css/font-awesome.min.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/animate.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/sweetalert.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/style.css">
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/jquery.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/resources/bootstrap/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/wow.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/sweetalert.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/custom.js"></script>
      </head>
      <body>
            <header>
                  <div class="container">
                        <div class="row">
                              <div class="col-md-12">
                                    <figure>
                                          <a href="javascript:void(0);"><img width="273" src="<?php echo base_url(); ?>assets_user/images/logo.png" alt=""></a>
                                    </figure>
                              </div>
                        </div>
                  </div>
            </header>
            <section class="login-port skyblue-bg">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-8">
                                    <div class="right-part white">
                                          <h3 class="semibold">Welcome to Emirates Employee Training Management !</h3>
                                          <p class="light">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, <br> consectetur, adipisci velit...</p>
                                    </div>
                              </div>
                             
                              <div class="col-md-4">
                                    <div class="left-part">
                                          <h3 class="white bold">Reset Password</h3>
                                          <form id="reset_submit" class="noradius white-bg">
                                          <span id="reset_error"></span>
                                                <div>
                                                      <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter your new password">
                                                </div>
                                                <div>
                                                      <input type="password" class="form-control" name="password" id="password"  placeholder="Confirm your new password">
                                                </div>
                                                <div class="registrationFormAlert" id="divCheckPasswordMatch"></div>
                                                <div>
                                                    <button type="submit" id="submit" name="submit" class="hover-bottom" onChange="checkPasswordMatch();">Reset Password</button>
                                                </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                  </div>
            </section>
            <section class="blue-bg" id="login-min">
                  <figure>
                        <img src="<?php echo base_url(); ?>assets_user/images/login-banner.png" alt="">
                  </figure>
            </section>
            <?php $this->load->view('common/footer')?>

<script>
function checkPasswordMatch() {
         //alert('das');
          var new_password = $("#new_password").val();
          var password = $("#password").val();

          if (new_password != password)
              $("#divCheckPasswordMatch").html("Passwords do not match!");
          else
              $("#divCheckPasswordMatch").html("Passwords match.");
            }

            $(document).ready(function () {
               $("#new_password, #password").keyup(checkPasswordMatch);
});

function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var email = getUrlParameter('email');
    var hash = getUrlParameter('hash');

$('#reset_submit').on('submit',function(e)
  {
    e.preventDefault();
    //alert('asdas');
    if($("#new_password").val() == '')
    {
      $("#new_password").focus().attr("placeholder","Please Enter Your New Password").val('');
      $("#new_password").css("border-color","red");
      return false;
    }

    if($("#password").val() == '')
    {
      $("#password").focus().attr("placeholder","Please Confirm Your New Password").val('');
      $("#password").css("border-color","red");
      return false;
    }

    var new_password = $("#new_password").val();
    var password = $("#password").val();



    var dataString = {new_password:new_password,password:password,email:email,hash:hash};

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "user/user/validate_reset_password",
      data: dataString,
      success: function(res){
       //alert(res);
        if(res == 1)
        {
            alert("Your Password Reset Successfully")
            window.location="<?php echo base_url(); ?>user/user";
        } else {
            alert("failed");
        }
      }
    });
});
    //alert(email);
    //alert(hash); 
</script>