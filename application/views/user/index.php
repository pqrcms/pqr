<!DOCTYPE html>
<html>
      <head>
            <title>Emirates CMS Power Company</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/reset.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/resources/bootstrap/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/resources/font-awesome/css/font-awesome.min.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/animate.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/sweetalert.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/style.css">
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/jquery.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/resources/bootstrap/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/wow.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/sweetalert.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/custom.js"></script>
      </head>
      <body>
            <header>
                  <div class="container">
                        <div class="row">
                              <div class="col-md-12">
                                    <figure>
                                          <a href="javascript:void(0);"><img width="273" src="<?php echo base_url(); ?>assets_user/images/logo.png" alt=""></a>
                                    </figure>
                              </div>
                        </div>
                  </div>
            </header>
            <section class="login-port skyblue-bg">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-8">
                                    <div class="right-part white">
                                          <h3 class="semibold">Welcome to Emirates Employee Training Management !</h3>
                                          <p class="light">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, <br> consectetur, adipisci velit...</p>
                                    </div>
                              </div>
                             
                              <div class="col-md-4">
                                    <div class="left-part">
                                          <h3 class="white bold">Login Here</h3>
                                          <form id="user_submit" class="noradius white-bg">
                                          <span id="login_error"></span>
                                                <div>
                                                      <input type="text" class="form-control" name="email" id="email" placeholder="Enter your email id">
                                                </div>
                                                <div>
                                                      <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
                                                </div>
                                                <div class="right">
                                                <!-- href="<?php //echo base_url() ?>user/user/forgot_password" -->
                                                    <a data-toggle="modal" data-target="#myModal" class="semibold" href="">Forgot Password?
                                                    </a>
                                                </div>
                                                <div>
                                                    <button type="submit" id="submit" name="submit" class="hover-bottom">Login</button>
                                                </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                  </div>
            </section>
            <section class="blue-bg" id="login-min">
                  <figure>
                        <img src="<?php echo base_url(); ?>assets_user/images/login-banner.png" alt="">
                  </figure>
            </section>
            <footer>
                  <div class="container">
                        <div class="row">
                              <div class="col-md-6">
                                    <div class="left">
                                          <p>Copyright &copy; <span id="current-year"></span> Emirates CMS Power Company. All Rights Reserved.</p>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="right">
                                          <ul class="inline">
                                                <li><a href="#">About us</a></li>
                                                <li><a href="#">Help Centre</a></li>
                                                <li><a href="#">Contact Us</a></li>
                                                <li><a href="#">FAQ’s</a></li>
                                          </ul>
                                    </div>
                              </div>
                        </div>
                  </div>
            </footer>
      </body>
</html>

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Forgot Password</h4>
        <form id="email_submit" class="noradius white-bg">
        <div class="modal-header"></div>
        <div class="form-group">
            <?php if($this->session->flashdata('message')) {?>
             <label><span style="color: #CC6633"><?php echo $this->session->flashdata('message');?><span></label>
            <?php }?>
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Enter Your Email:</label>
            <input name="email" type="emailid" size="25" id="femail" placeholder="Enter your email here.." class="form-control" value="<?php echo set_value('email');?>" />
            <span id="email_error"  style="color:red"></span>
            <span style="color:red"><?php echo form_error('email');?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" id="submit" name="submit" class="btn btn-secondary">Submit</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
/*****End User Login ****/
 $('#user_submit').on('submit',function(e)
  {
    e.preventDefault();
    //alert('asdas');
    if($("#email").val() == '')
    {
      $("#email").focus().attr("placeholder","Please Enter Valid Email ID").val('');
      $("#email").css("border-color","red");
      return false;
    }
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if(!(emailReg.test($("#email").val())))
    {
      $("#email").focus().attr("placeholder","Please Enter Valid Email Formate").val('');
      $("#email").css("border-color","red");
      return false;
    }

    if($("#password").val() == '')
    {
      $("#password").focus().attr("placeholder","Please Enter Valid Password").val('');
      $("#password").css("border-color","red");
      return false;
    }

    var email = $("#email").val();
    var password = $("#password").val();

    var dataString = {email:email,password:password};

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "login/validate_credentials",
      data: dataString,
      success: function(res){
      //alert(res);
        if(res == 1)
        {
          setTimeout(function(){
          window.location="<?php echo base_url(); ?>user/user/home";
          },1000);
        }
        else if(res == 2)
        {
          $("#login_error").html("Email ID or Password Is Invalid");
        }
      }
    });
});

$('#email_submit').on('submit',function(e){
    e.preventDefault();
    //alert('asdas');
    if($("#femail").val() == '')
    {
      $("#femail").focus().attr("placeholder","Please Enter Your Email Id").val('');
      $("#femail").css("border-color","red");
      return false;
    }

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if(!(emailReg.test($("#femail").val())))
    {
      $("#femail").focus().attr("placeholder","Please Enter Valid Email Formate").val('');
      $("#femail").css("border-color","red");
      return false;
    }

    var email = $("#femail").val();

    var dataString = {email:email};

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "login/validate_email",
      data: dataString,
      success: function(res){
        //alert(res);
        if(res == 1)
        {
          setTimeout(function(){
          window.location="<?php echo base_url(); ?>user/user/home";
          },1000);

        }
        else if(res == 2)
        {
           $("#email_error").html("Please Enter Registered Email Id");
        }
      }
    });
    return false;
  });
</script>