<?php $this->load->view('common/header')?>
       <section id="main-dashboard">
            <div class="left-nav">
                <ul class="dash-menu">
                    <li class="active menu-active" id="dashboard_id" data-attr="dashboard"><img src="<?php echo base_url();?>assets_user/images/icons/dashboard.png" alt="">Dashboard</li>
                    <li class="menu-active" id="quiz_id" data-attr="quiz"><img src="<?php echo base_url();?>assets_user/images/icons/quiz.png" alt="">Training</li>
                    <li class="menu-active" id="result-id" data-attr="result"><img src="<?php echo base_url();?>assets_user/images/icons/score.png" alt="">Results</li>
                    <li class="menu-active" data-attr="daily-log"><img src="<?php echo base_url();?>assets_user/images/icons/dailylog.png" alt="">Daily Log</li>
                    <li class="menu-active" id="feedback-id" data-attr="feedback"><img src="<?php echo base_url();?>assets_user/images/icons/feedback.png" alt="">Feedback</li>
                    <li class="menu-active" data-attr="report"><img src="<?php echo base_url();?>assets_user/images/icons/report.png" alt="">Report</li>
                </ul>
            </div>
            <div class="right-content">
                <div class="active-now" id="dashboard">
                    <ul>
                        <li>
                            <div class="float">
                                <div class="forty">
                                    <div class="total-test">
                                        <h2 class="bold">Quiz</h2>
                                        <h3><span class="bold"><?php echo $task_count['completed_count'];?></span>/<span class="sm-cls"><small><?php echo $task_count['total_count'];?></small></span></h3>
                                        <a href="javascript:void(0);">View All</a>
                                    </div>
                                </div>
                                <div class="sixty">
                                   <div class="latest-score">
                                        <h2>Latest Score</h2>
                                        <div id="table-scroll" class="panel-hands">
                                             <table class="category_table table table-bordered mar-top" >
                                                <tbody>

                                                <?php 
                                                foreach ($result as $re) {
                                                    // print_r($re);exit;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo date('d-M-y',strtotime($re['time']));?></td>
                                                        <td><a><?php echo $re['title'];?></a></td>
                                                        <td><?php echo $re['department'];?></td>
                                                        <td><?php echo $re['assigned_by'];?></td>
                                                        <td><?php echo $re['mark_obtained'];?></td>
                                                    </tr>
                                                    <?php  }?>                                                             
                                                </tbody>
                                            </table> 
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="float flo">
                                <div class="forty">
                                    <p><span class="head-score">Score</span><span class="current-all">Current Over All</span></p>
                                    <div id="gg1" class="gauge"></div>
                                    <p class="score-qizz">Score based on 2 compleeted quiz</p>
                                </div>
                                <div class="sixty">                                       
                            <script src="https://code.highcharts.com/highcharts.js"></script>
                            <script src="https://code.highcharts.com/modules/exporting.js"></script>
                            <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">
                            </div>
                            </div>
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="active-now" id="quiz">
                    <ul>
                        <li>
                            <div class="float-srs">
                                <div class="forty-srs">
                                    <h2>Trainings</h2>
                                    <a href="<?php echo base_url();?>user/user/dashboard">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="sixty-srs">
                                    <form class="aws-search-form" role="search">
                                        <input name="s" value="" id="search_title" class="aws-search-field" placeholder="Search with training name" type="text">
                                        <input name="post_type" value="product" type="hidden">
                                        <input name="type_aws" value="true" type="hidden">
                                        <div class="aws-search-result" style="display: none;">            
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                        <li>
                        <div class="row">
                                <div class="row8-line mar-top training">
                                <?php $i=1; 
                                foreach($training_module as $t){?>
                                    <?php if($t['task_status']==0){?>
                                    <div class="col-md-4 col-sm-12 col-xs-12 training_sub">
                                    <input type="hidden" name="" class="title" id="title" value="<?php echo $t['title'];?>">
                                        <div class="intro-full mar-top">
                                            <div class="pqr-intro">
                                            <h1><?php echo sprintf("%02d", $i);?></h1>
                                            <h4><?php echo $t['title'];?></h4>
                                            <p class="para"><?php echo $t['discription'];?>
                                            </p>
                                            <p class="quest">Question : <?php echo $t['no_question'];?></p>
                                            <div class="span-class">
                                                <p><span>Start Date :</span><span> <?php echo date('d M, Y',strtotime($t['start_date']));?></span></p>
                                                <p><span>Deadline : </span> <span><?php echo date('d M, Y',strtotime($t['dead_line']));?></span></p>
                                                <p><span>Assigned by :</span><span> <?php echo $t['assigned_by'];?></span></p>
                                            </div>

                                         </div>
                                            <?php if(!empty($t['completed_time'])){?>
                                                <div class="training-score">
                                                    <p>Training Score</p>
                                                    <span><?php echo $t['mark_obtained'];?>/<?php echo $t['total_marks'];?></span>
                                                </div>
                                                <div class="finished-pgm">
                                                    <p>Finished on <?php echo date('d M, Y',strtotime($t['completed_time']));?></p>
                                                </div>
                                            <?php } 
                                            else{?>
                                                <div class="training-score">
                                                    <p>Training Score</p>
                                                    <span><?php echo $t['total_marks'];?></span>
                                                </div>
                                                <div class="finished-pgm" id="start">
                                                    <p class="start_now" data-id="<?php echo $t['tid'];?>">START NOW</p>
                                                </div>
                                                <div class="finished-pgm" id="time" style="display:none;">
                                                    Time Remaining<p class="time_left" id="showtime1" data-id="<?php echo $t['tid'];?>"></p>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <?php }else if($t['task_status']==1){?>

                                    <div class="col-md-4 col-sm-12 col-xs-12 training_sub">
                                    <input type="hidden" name="" class="title" id="title" value="<?php echo $t['title'];?>">
                                        <div class="intro-full mar-top dummy">
                                            <div class="pqr-intro dummy-data">
                                                <h1><?php echo sprintf("%02d", $i);?></h1>
                                                <h4><?php echo $t['title'];?></h4>
                                                
                                                <div class="dumb">
                                                    <img src="<?php echo base_url();?>assets_user/images/Layer-13.png" class="img-responsive">
                                                    <p>Coming Soon</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php }?>
                                    <?php $i++; }?>
                            </div>
                        </li>
                    </ul>
                </div>

            <div class="right-content active-now"  id="question">
                <div class="row">
                    <div class="col-md-2">
                        <h3 class="bold">Training 02</h3>
                        <div class="date">
                            <p>Start Date:<span> 27 Aug, 2017</span></p>
                            <p>Deadline:<span> 27 Aug, 2017</span></p>
                        </div>
                        <div class="time">
                            <p>MM  SS</p>
                            <p id="showtime2"></p>
                        </div>
                        <div class="col-md-2">
                            <input class="backBtn" id="back" disabled type="button" name="" value="<< BACK">
                        </div>
                    </div>
                    <div class="col-md-8 question_body" >
                        <p style="color: #898989; text-align: center;">Question 1/18</p>
                        <!-- <div class="content">
                            <div class="qtns">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                            </div>
                            <div class="options">
                                <form action="#">
                                  <p>
                                    <input type="radio" id="opt1" name="radio-group" checked>
                                    <label for="opt1">Type setting Industry</label>
                                  </p>
                                  <p>
                                    <input type="radio" id="opt2" name="radio-group">
                                    <label for="opt2">Type setting Industry</label>
                                  </p>
                                  <p>
                                    <input type="radio" id="opt3" name="radio-group">
                                    <label for="opt3">Type setting Industry</label>
                                  </p>
                                  <p>
                                    <input type="radio" id="opt4" name="radio-group">
                                    <label for="opt4">Type setting Industry</label>
                                  </p>
                                </form>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-md-2 submit_box" id="next_box">
                        <input class="nextBtn" id="next" type="button" name="" value="NEXT >>">
                    </div>
                    <div class="col-md-2 submit_box" id="submit_box" style="display: none;">
                        <input class="nextBtn" id="submit" type="button" name="" value="SUBMIT >>">
                    </div>
                </div>
            </div>


           <!--  <div class="right-content active-now" id="quiz4">
                <div class="row">
                    <div class="col-md-2">
                        <h3 class="bold">Training 02</h3>
                        <div class="date">
                            <p>Start Date:<span> 27 Aug, 2017</span></p>
                            <p>Deadline:<span> 27 Aug, 2017</span></p>
                        </div>
                        <div class="time">
                            <p>HH  MM  SS</p>
                            <div class="bold" id="time"></div>
                        </div>
                        <div class="col-md-2">
                            <input class="backBtn" type="button" name=""  value="<< BACK">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <p style="color: #898989; text-align: center;">Question 1/18</p>
                        <div class="content">
                            <div class="qtns">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                            </div>
                            <div class="options">
                                <form action="#">
                                  <textarea name="" id="" rows="6" placeholder="Type your answer here..."></textarea>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <input class="nextBtn" type="button" name="" value="NEXT >>">
                    </div>
                </div>
            </div> -->

                <div class="active-now" id="result">
                    <div class="result-scan">
                        <p class="result-me">Result</p>
                        <ul>
                        <?php $j=sizeof($result);
                        foreach ($result as $r) {?>
                            <li>
                                <div class="pass-you">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="intro-1">
                                                <p><?php echo sprintf("%02d", $j);?> </p>
                                                <p><?php echo $r['title'];?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="date-infor">
                                                <p>
                                                    <span>Start date</span>:
                                                    <span><?php echo date('d M, Y',strtotime($t['start_date']));?></span>
                                                </p>
                                                <p>
                                                    <span>Deadline</span>:
                                                    <span><?php echo date('d M, Y',strtotime($t['dead_line']));?></span>
                                                </p>
                                                <p>
                                                    <span>Assigned By</span>:
                                                    <span><?php echo $t['assigned_by'];?></span>
                                                </p>
                                            </div>


                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12">
                                            <div class="cir_result">
                                                <h1>35<span>/50</span></h1>
                                            </div>

                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                        <?php if($r['result_status']==1){?>
                                            <div class="div-div">
                                                <a href="javascript:void(0);">Pass</a>
                                            </div>
                                            <?php }elseif ($r['result_status']==0) {?>
                                             <div class="div-div mar-35">
                                                <a href="javascript:void(0);">Pass</a>
                                                <a href="javascript:void(0);">Fail</a>
                                            </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php $j--;}?>
                        </ul>
                    </div>
                </div>

            <div class="active-now" id="daily-log">
                <div class="row">
                    <div class="col-md-8">
                        <div class="right_text">
                            <input class="clear" type="button" id="clear_log" value="clear log">
                        </div>
                        <ul class="timeline">

                        <?php foreach ($daily_log as $dl) {?>
                            <?php if(!empty($dl['day'])){?>
                            <li class="timeline-inverted">
                              <div class="timeline-badge warning"><p style="color: #323232; font-size: 15px;"><?php echo date('D',strtotime($dl['day'][0]['date']));?></p><p style="font-size: 12px; color: #323232;"><?php echo date('M y',strtotime($dl['day'][0]['date']));?></p></div>
                              <div class="timeline-panel">
                              <?php foreach ($dl['day'] as $d) {?>
                                <div class="contentTimeline" id="log<?php echo $d['id'];?>">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <p><?php echo $d['time'];?></p>
                                        </div>
                                        <div class="col-md-9">
                                            <p><?php echo $d['log'];?></p>
                                        </div>
                                        <div class="col-md-1">
                                            <i class="glyphicon glyphicon-trash delete_log" data-id="<?php echo $d['id'];?>"></i>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                              </div>
                            </li>
                              <?php }?>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="content-wrapper">
                          <div class="left-wrapper">
                            <div class="header"><i class="fa fa-arrow-left" aria-hidden="true"></i><span><span class="month"> </span><span class="year"></span></span><i class="fa fa-arrow-right" aria-hidden="true"></i></div>
                            <div class="calender">
                              <table>
                                <thead>
                                  <tr class="weedays">
                                    <th data-weekday="sun" data-column="0">Sun</th>
                                    <th data-weekday="mon" data-column="1">Mon</th>
                                    <th data-weekday="tue" data-column="2">Tue</th>
                                    <th data-weekday="wed" data-column="3">Wed</th>
                                    <th data-weekday="thu" data-column="4">Thu</th>
                                    <th data-weekday="fri" data-column="5">Fri</th>
                                    <th data-weekday="sat" data-column="6">Sat</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr class="days" data-row="0">
                                    <td data-column="0"></td>
                                    <td data-column="1"></td>
                                    <td data-column="2"></td>
                                    <td data-column="3"></td>
                                    <td data-column="4"></td>
                                    <td data-column="5"></td>
                                    <td data-column="6"></td>
                                  </tr>
                                  <tr class="days" data-row="1">
                                    <td data-column="0"></td>
                                    <td data-column="1"></td>
                                    <td data-column="2"></td>
                                    <td data-column="3"></td>
                                    <td data-column="4"></td>
                                    <td data-column="5"></td>
                                    <td data-column="6"></td>
                                  </tr>
                                  <tr class="days" data-row="2">
                                    <td data-column="0"></td>
                                    <td data-column="1"></td>
                                    <td data-column="2"></td>
                                    <td data-column="3"></td>
                                    <td data-column="4"></td>
                                    <td data-column="5"></td>
                                    <td data-column="6"></td>
                                  </tr>
                                  <tr class="days" data-row="3">
                                    <td data-column="0"></td>
                                    <td data-column="1"></td>
                                    <td data-column="2"></td>
                                    <td data-column="3"></td>
                                    <td data-column="4"></td>
                                    <td data-column="5"></td>
                                    <td data-column="6"></td>
                                  </tr>
                                  <tr class="days" data-row="4">
                                    <td data-column="0"></td>
                                    <td data-column="1"></td>
                                    <td data-column="2"></td>
                                    <td data-column="3"></td>
                                    <td data-column="4"></td>
                                    <td data-column="5"></td>
                                    <td data-column="6"></td>
                                  </tr>
                                  <tr class="days" data-row="5">
                                    <td data-column="0"></td>
                                    <td data-column="1"></td>
                                    <td data-column="2"></td>
                                    <td data-column="3"></td>
                                    <td data-column="4"></td>
                                    <td data-column="5"></td>
                                    <td data-column="6"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <!-- <div class="right-wrapper">
                            <div class="header"><span></span></div><span class="day"></span><span class="month"></span>
                          </div> -->
                        </div>
                    </div>
                </div> 
            </div>   
                <div class="active-now" id="feedback">
                    <div class="row menu-tabe-sec">

                        <ul class="nav nav-tabs uservav">
                            <li class="active"><a data-toggle="tab" href="#home">Give Feedback</a></li>
                            <li><a data-toggle="tab" href="#menu1">Feedback from Trainer</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="feeed-reivew">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="feedback-left">
                                                <img src="<?php echo base_url();?>assets_user/images/Layer-13-copy.jpg" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-div-sec">
                                            <form name="feedback_form">
                                            <div class="feedback-right">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                            </div>
                                            <div class="drop-list mar-top">
                                                <select name="trainer" id="trainer" >
                                                  <option value="">Select trainer you want to give feedback</option>
                                                  <?php foreach ($trainer as $tr) {?>
                                                    <option value="<?php echo $tr['tid'];?>"><?php echo $tr['trainer'];?></option>
                                                  <?php  }?>
                                                </select>     
                                            </div>
                                            <div class="drop-list mar-top">
                                                <select name="task" id="task">
                                                  <option value="">Select training provided by trainer</option>
                                                </select>    
                                            </div>
                                            <div class="row">
                                                <div class="form-div">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <div class="yes-no-content">
                                                        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit...
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="yes-no-button">
                                                        <p class="onoff"><input type="checkbox" name="feedback" value="1" id="checkboxID1"><label for="checkboxID1"></label></p>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-div">
                                                    <div class="col-md-9 col-sm-12 col-xs-12">
                                                        <div class="yes-no-content">
                                                            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit...
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="yes-no-button">
                                                            <p class="onoff"><input type="checkbox" name="feedback" value="2" id="checkboxID2"><label for="checkboxID2"></label></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-div">
                                                    <div class="col-md-9 col-sm-12 col-xs-12">
                                                        <div class="yes-no-content">
                                                            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit...
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="yes-no-button">
                                                            <p class="onoff"><input type="checkbox" name="feedback" value="3" id="checkboxID3"><label for="checkboxID3"></label></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="fids" id="fids" value="">
                                            <a class="sub-button" href="javascript:void(0);"><input type="submit" value="Submit"></a>
                                        </form>
                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                test
                            </div>
                        </div>
                    </div>
                </div>
        </section>
<input type="hidden" id="max_marks" value="<?php echo (!empty($emp_tasks->max_marks))? $emp_tasks->max_marks:0;?>">
<input type="hidden" id="mark_obtained" value="<?php echo (!empty($emp_tasks->mark_obtained))? $emp_tasks->mark_obtained:0;?>">
<input type="hidden" id="attitude" value="<?php echo (!empty($trainee_feedback->attitude))? $trainee_feedback->attitude:0;?>">
<input type="hidden" id="confidence" value="<?php echo (!empty($trainee_feedback->confidence))? $trainee_feedback->confidence:0;?>">
<input type="hidden" id="traine_feedback" value="<?php echo (!empty($trainee_feedback->traine_feedback))? $trainee_feedback->traine_feedback:0;?>">
<input type="hidden" id="progress" value="<?php echo (!empty($trainee_feedback->progress))? $trainee_feedback->progress:0;?>">
<input type="hidden" id="relationship" value="<?php echo (!empty($trainee_feedback->relationship))? $trainee_feedback->relationship:0;?>">
<input type="hidden" id="self_confidence" value="<?php echo (!empty($trainee_feedback->self_confidence))? $trainee_feedback->self_confidence:0;?>">


<?php $this->load->view('common/footer')?>
<script type="text/javascript" src="http://momentjs.com/downloads/moment.js"></script>
<script language ="javascript" >

function delete_log(m){
    alert(1);
         var lid=$(m).attr('data-id');
         $.ajax({
            url: "<?php echo base_url() ?>user/user/delete_log/"+lid,
            type: "POST",
            success: function(data){
                // alert($(m).parent(".contentTimeline").html());
                    $('#log'+lid+':after').css('display','none');
                    $('#log'+lid).remove();
                }
            });
}
 var month = [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ];
            var weekday = [
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            ];
            var weekdayShort = [
                "sun",
                "mon",
                "tue",
                "wed",
                "thu",
                "fri",
                "sat"
            ];
            var monthDirection = 0;
            var complete_date;
            function getNextMonth() {
                monthDirection++;
                var current;
                var now = new Date();
                if (now.getMonth() == 11) {
                    current = new Date(now.getFullYear() + monthDirection, 0, 1);
                } else {
                    current = new Date(now.getFullYear(), now.getMonth() + monthDirection, 1);
                }
                complete_date=current;
                initCalender(getMonth(current));
            }

            function getPrevMonth() {
                monthDirection--;
                var current;
                var now = new Date();
                if (now.getMonth() == 11) {
                    current = new Date(now.getFullYear() + monthDirection, 0, 1);
                } else {
                    current = new Date(now.getFullYear(), now.getMonth() + monthDirection, 1);
                }
                complete_date=current;
                initCalender(getMonth(current));
            }
            Date.prototype.isSameDateAs = function (pDate) {
                return (
                    this.getFullYear() === pDate.getFullYear() &&
                    this.getMonth() === pDate.getMonth() &&
                    this.getDate() === pDate.getDate()
                );
            };

            function getMonth(currentDay) {
                var now = new Date();
                var currentMonth = month[currentDay.getMonth()];
                var monthArr = [];
                for (i = 1 - currentDay.getDate(); i < 31; i++) {
                    var tomorrow = new Date(currentDay);
                    tomorrow.setDate(currentDay.getDate() + i);
                    if (currentMonth !== month[tomorrow.getMonth()]) {
                        break;
                    } else {
                        monthArr.push({
                            date: {
                                weekday: weekday[tomorrow.getDay()],
                                weekday_short: weekdayShort[tomorrow.getDay()],
                                day: tomorrow.getDate(),
                                month: month[tomorrow.getMonth()],
                                year: tomorrow.getFullYear(),
                                current_day: now.isSameDateAs(tomorrow) ? true : false,
                                date_info: tomorrow
                            }
                        });
                    }
                }
                // alert(monthArr);
                return monthArr;
            }

            function clearCalender() {
                $(".calender table tbody tr").each(function () {
                    $(this).find("td").removeClass("active selectable currentDay between hover").html("");
                });
                $("td").each(function () {
                    $(this).unbind('mouseenter').unbind('mouseleave');
                });
                $("td").each(function () {
                    $(this).unbind('click');
                });
                clickCounter = 0;
            }

            function initCalender(monthData) {
                var row = 0;
                var classToAdd = "";
                var currentDay = "";
                var today = new Date();

                clearCalender();
                $.each(monthData,
                    function (i, value) {
                        var weekday = value.date.weekday_short;
                        var day = value.date.day;
                        var column = 0;
                        var index = i + 1;

                        $(".left-wrapper .header .month").html(value.date.month);
                        $(".left-wrapper .header .year").html(value.date.year);
                        if (value.date.current_day) {
                            currentDay = "currentDay";
                    classToAdd = "selectable";
                            $(".right-wrapper .header span").html(value.date.weekday);
                            $(".right-wrapper .day").html(value.date.day);
                            $(".right-wrapper .month").html(value.date.month);
                        }
                        if (today.getTime() > value.date.date_info.getTime()) {
                            classToAdd = "selectable";

                        }
                        $("tr.weedays th").each(function () {
                            var row = $(this);
                            if (row.data("weekday") === weekday) {
                                column = row.data("column");
                                return;
                            }
                        });
                        $($($($("tr.days").get(row)).find("td").get(column)).addClass(classToAdd + " " + currentDay).html(day));
                        currentDay = "";
                        if (column == 6) {
                            row++;
                        }
                    });
                $("td.selectable").click(function () {
                    dateClickHandler($(this));
                });
            }
            initCalender(getMonth(new Date()));

            var clickCounter = 0;
            $(".fa-angle-double-right").click(function () {
                $(".right-wrapper").toggleClass("is-active");
                $(this).toggleClass("is-active");
            });
            var date1;
            var date2;
            function dateClickHandler(elem) {
// alert(elem.html());
                var day1 = parseInt($(elem).html());
                if (clickCounter === 0) {
                    $("td.selectable").each(function () {
                        $(this).removeClass("active between hover");
                    });
                    date1=day1;
                }
                clickCounter++;
                if (clickCounter === 2) {
                    $("td.selectable").each(function () {
                        $(this).unbind('mouseenter').unbind('mouseleave');
                    });
                    clickCounter = 0;
                    date2=day1;
                    get_log();
                    return;
                }
                $(elem).toggleClass("active");
                $("td.selectable").hover(function () {

                    var day2 = parseInt($(this).html());
                    $(this).addClass("hover");
                    $("td.selectable").each(function () {
                        $(this).removeClass("between");

                    });
                    if (day1 > day2 + 1) {
                        $("td.selectable").each(function () {
                            var dayBetween = parseInt($(this).html());
                            // alert(dayBetween);
                            if (dayBetween > day2 && dayBetween < day1) {
                                $(this).addClass("between");
                            }
                        });
                    } else if (day1 < day2 + 1) {
                        $("td.selectable").each(function () {
                            var dayBetween = parseInt($(this).html());
                            if (dayBetween > day1 && dayBetween < day2) {
                                $(this).addClass("between");
                            }
                        });
                    }
                }, function () {
                    $(this).removeClass("hover");
                });
            }
            $(".fa-arrow-left").click(function () {
                // alert("sss");
                getPrevMonth();
                $(".content-wrapper").addClass("is-rotated-left");
                setTimeout(function () {
                    $(".content-wrapper").removeClass("is-rotated-left");
                }, 195);
            });

            $(".fa-arrow-right").click(function () {
                getNextMonth();
                $(".content-wrapper").addClass("is-rotated-right");
                setTimeout(function () {
                    $(".content-wrapper").removeClass("is-rotated-right");
                }, 195);
            });

function get_log()
{
    var m=month.indexOf($('.month').html());
    m++;
    var y=$('.year').html()
    //alert(date2);
    $.ajax({
            url: "<?php echo base_url() ?>user/user/get_log/"+date1+"/"+date2+"/"+m+"/"+y,
            dataType: "json",
            type: "POST",
            success: function(data){
                //console.log(data);
                txt="";
                $.each(data, function( index, value ) {
                  if(value.day.length>0)
                  {
                  txt+=`<li class="timeline-inverted">`;
                    d1=new Date(value.day[0].date);
                    m=moment(new Date(value.day[0].date)).format("MMM YY");
                    d=moment(new Date(value.day[0].date)).format("ddd");
                    txt+=`<div class="timeline-badge warning"><p style="color: #323232; font-size: 15px;">`+d+`</p><p style="font-size: 12px; color: #323232;">`+m+`</p></div>
                              <div class="timeline-panel">`;
                                $.each(value.day, function( index1, value1 ) {
                               txt+= `<div class="contentTimeline" id="log`+value1.id+`">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <p> `+value1.time+`</p>
                                        </div>
                                        <div class="col-md-9">
                                            <p> `+value1.log+`</p>
                                        </div>
                                        <div class="col-md-1">
                                            <i class="glyphicon glyphicon-trash delete_log" id="delete_log"  onclick="delete_log(this);" data-id="`+value1.id+`"></i>
                                        </div>
                                    </div>
                                </div>`;
                                });
                             txt+= `</div>`;
                  txt+=`</li>`;
                  }
                  // console.log(txt);
                });
                    $('.timeline').empty();
                    $('.timeline').append(txt);
                }
            });

}



        var tim;
        var min = 01;
        var sec = 60;
        var f = new Date();
        // function f1() {
        //     f2();
        //     document.getElementById("starttime").innerHTML = ""+ f.getHours() + ":" + f.getMinutes();
        // }
        // function f2() {
        //     if (parseInt(sec) > 0) {
        //         sec = parseInt(sec) - 1;
        //         document.getElementById("showtime2").innerHTML = ""+ min +":" + sec+"";
        //         document.getElementById("showtime1").innerHTML = ""+ min +":" + sec+"";
        //         tim = setTimeout("f2()", 1000);

(function(jQuery){
$(window).on("load",function(){
    $(".panel-hands").mCustomScrollbar({
        setHeight:120,
        theme:"dark-3"
    });
});
})(jQuery);

document.addEventListener("DOMContentLoaded", function(event) {
var max_marks = document.getElementById('max_marks').value;
max_marks = parseFloat(max_marks);
//alert(max_marks);

var mark_obtained = document.getElementById('mark_obtained').value;
mark_obtained = parseFloat(mark_obtained);

var mx = (max_marks / 5);
// alert(mx);
var om = (mark_obtained / mx);
//alert(mx);

if(max_marks == 0){
    //alert(om);
    om = 0;
}
//alert(om);
var gg1 = new JustGage({
  id: "gg1",
  value : om,
  color: "#283c93",
  min: 0,
  max: 5,
  decimals: 1,
  gaugeWidthScale: 0.6,
  customSectors: [{
    color : "#00a2e3",
    lo : 0,
    hi : 2.5
  },{
    color : "#283c93",
    lo : 2.5,
    hi : 5
  }],
  counter: true
});
});

var tim;
var min = 01;
var sec = 60;
var f = new Date();
function f1() {
    f2();
    document.getElementById("starttime").innerHTML = ""+ f.getHours() + ":" + f.getMinutes();
}
function f2() {
    if (parseInt(sec) > 0) {
        sec = parseInt(sec) - 1;
        document.getElementById("showtime2").innerHTML = ""+ min +":" + sec+"";
        document.getElementById("showtime1").innerHTML = ""+ min +":" + sec+"";
        tim = setTimeout("f2()", 1000);
    }
    else {
        if (parseInt(sec) == 0) {
            min = parseInt(min) - 1;
            if (parseInt(min) == 0) {
                clearTimeout(tim);
                location.href = "resultpage.php";

            }
            else {
                sec = 60;
                document.getElementById("showtime1").innerHTML = "" + min + ":" + sec + "";
                document.getElementById("showtime2").innerHTML = ""+ min +":" + sec+"";
                alert("Time Up");
                tim = setTimeout("f2()", 1000);
            }
        }
    }
}

(function(jQuery){
        $(window).on("load",function(){

    startTime();
            $(".panel-hands").mCustomScrollbar({
                setHeight:147,
                theme:"dark-3"
            });
        });
    })(jQuery);

function checkTime(i) {
              if (i < 10) {
                i = "0" + i;
              }
              return i;
            }

            // function startTime() {
            //   var today = new Date();
            //   var h = today.getHours();
            //   var m = today.getMinutes();
            //   var s = today.getSeconds();
            //   // add a zero in front of numbers<10
            //   m = checkTime(m);
            //   s = checkTime(s);
            //   var tt=h + ":" + m + ":" + s;
            //   $('#question #time').html(tt);
            //   t = setTimeout(function() {
            //     startTime()
            //   }, 500);
            // }


            function startTime() {
                  var today = new Date();
                  var h = today.getHours();
                  var m = today.getMinutes();
                  var s = today.getSeconds();
                  // add a zero in front of numbers<10
                  m = checkTime(m);
                  s = checkTime(s);
                  var tt=h + ":" + m + ":" + s;
                  $('#question #time').html(tt);
                  t = setTimeout(function() {
                    startTime()
                  }, 500);
            }
/*function startTime() {
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();
      // add a zero in front of numbers<10
      m = checkTime(m);
      s = checkTime(s);
      var tt=h + ":" + m + ":" + s;
      $('#question #time').html(tt);
      t = setTimeout(function() {
        startTime()
      }, 500);
}*/
            
jQuery(document).ready(function($){

var qno=0;
var total_questions=0;

$('.training .training_sub').each(function(){
    // alert($(this).children("#title").val().toLowerCase());
    $(this).attr('data-search-term', $(this).children("#title").val().toLowerCase());
});

$('#search_title').on('keyup', function(){
var searchTerm = $(this).val().toLowerCase();
    $('.training .training_sub').each(function(){
        if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
});

$('#trainer').on('change',function(){
        var val=$('#trainer').val();
        if(val=='')
        {
            alert('please select trainer');
            var  txts="<option value='' >Select training provided by trainer</option>";
            $('#task').children().remove();
            $('#task').append(txts);
            return false;
        }
        $.ajax({
            url: "<?php echo base_url() ?>user/user/get_task/"+val,
            dataType: "json",
            type: "POST",
            success: function(data){
                var  txt="<option value='' >Select training provided by trainer</option>";
                $.each(data, function( index, value ) {
                  txt+=`<option value="`+value['id']+`">`+value['title']+`</option>`;
                });
                if(txt!='')
                {
                    $('#task').children().remove();
                    $('#task').append(txt);
                }
                else{
                    alert("No task");
                }
            }
        });
});

 $('form[name="feedback_form"]').on('submit',function(e){ 
              e.preventDefault(); 

              var id="";
              $.each($('input[name="feedback"]:checked'), function( index, value ) {
                  // alert($(this).val());
                  if(id=="")
                  {
                    id=$(this).val();
                  }
                  else{
                    id+=','+$(this).val();
                  }
                });
              // alert(id);
              $('#fids').val(id);
               $.ajax({
            url: "<?php echo base_url() ?>user/user/add_feedback",
            dataType: "json",
            type: "POST",
            data:$("form[name='feedback_form']").serialize(),
            success: function(data){
                    
                }
            });
    });


$('.start_now').on('click',function(){
        qno=0;
        total_questions=0;
        f2();
        var tid=$(this).attr('data-id');
         $('#quiz').hide();
         $('#question').show();
         $('#time').show();
         $('#start').hide();
         $('#question #next').attr('data-id',tid);
         $('#question #back').attr('data-id',tid);
         $('#question #submit').attr('data-id',tid);

         $('#next').attr('disabled',false);
         $('#submit_box').hide();
         $('#next_box').show();
         $('#back').attr('disabled',true);

            get_total(tid);
            get_question(tid);

 });

 $('.time_left').on('click',function(){
        var tid=$(this).attr('data-id');
         $('#quiz').hide();
         $('#question').show();
         $('#time').show();
         $('#question #next').attr('data-id',tid);
         $('#question #back').attr('data-id',tid);
         $('#question #submit').attr('data-id',tid);

         $('#next').attr('disabled',false);
         $('#submit_box').hide();
         $('#next_box').show();
         $('#back').attr('disabled',true);

            get_question(tid);

 });

 $('#next').on('click',function(){
        qno++;
         var tid=$(this).attr('data-id');
         // $('#quiz').hide();
         // $('#question').show();
         if(qno<total_questions)
         {
            get_question(tid);
            save_answer(tid,(qno-1));
         }
        $('#back').attr('disabled',false);
        if((qno+1)==total_questions)
        {
            $('#next').attr('disabled',true);
            $('#next_box').hide();
            $('#submit_box').show();
        }

 });
 $('#back').on('click',function(){
        qno--;
         var tid=$(this).attr('data-id');
         // $('#quiz').hide();
         // $('#question').show();
         if(qno>=0){
            get_question(tid);
            save_answer(tid,(qno+1));
         }
         $('#next').attr('disabled',false);
         $('#submit_box').hide();
         $('#next_box').show();
         if(qno==0)
         {
            $('#back').attr('disabled',true);
         }

 });

 function save_answer(tid,qn)
 {
    //var qsno=$('form[name="question'+qn+'"] #qid').val();
    //alert(qsno);
     $.ajax({
            url: "<?php echo base_url() ?>user/user/save_answer/"+tid,
            dataType: "json",
            type: "POST",
            data:$("form[name='question"+qn+"']").serialize(),
            success: function(data){
                    
                }
            });
 }
 function get_question(tid)
 {
     $.ajax({
            url: "<?php echo base_url() ?>user/user/get_question/"+tid+"/"+qno,
            type: "POST",
            success: function(data){
                    $('.question_body').children('div').remove();
                    $('.question_body').append(data);
                }
            });
 }


 function get_total(tid)
 {
     $.ajax({
            url: "<?php echo base_url() ?>user/user/get_total/"+tid,
            type: "POST",
            success: function(data){
                    total_questions=data;
                }
            });
 }

 $('#submit').on('click',function(){
    qno=0;
         $('#submit').attr('disabled',true);
        var tid=$(this).attr('data-id');
         $.ajax({
            url: "<?php echo base_url() ?>user/user/submit_task/"+tid,
            type: "POST",
            success: function(data){
                    swal({
                    title: "Success!",
                    text: "Successfully completed",
                    type: "success",
                    confirmButtonColor: '#aedef4',
                    confirmButtonText: 'OK',
                    },
                    function(isConfirm) {
                        if(isConfirm) {
                            $('#quiz_id').trigger('click');
                        }
                    }
                    );
                }
            });
                
 
 });



 $('#delete_log').on('click',function(){
    alert(1);
         var lid=$(this).attr('data-id');
         $.ajax({
            url: "<?php echo base_url() ?>user/user/delete_log/"+lid,
            type: "POST",
            success: function(data){
                // alert($(this).parent(".contentTimeline").html());
                    $('#log'+lid+':after').css('display','none');
                    $('#log'+lid).remove();
                }
            });

 });

function delete_log(){
    alert(1);
         var lid=$(this).attr('data-id');
         $.ajax({
            url: "<?php echo base_url() ?>user/user/delete_log/"+lid,
            type: "POST",
            success: function(data){
                // alert($(this).parent(".contentTimeline").html());
                    $('#log'+lid+':after').css('display','none');
                    $('#log'+lid).remove();
                }
            });
}
 $('.delete_log').on('click',function(){
    alert(1);
         var lid=$(this).attr('data-id');
         $.ajax({
            url: "<?php echo base_url() ?>user/user/delete_log/"+lid,
            type: "POST",
            success: function(data){
                // alert($(this).parent(".contentTimeline").html());
                    $('#log'+lid+':after').css('display','none');
                    $('#log'+lid).remove();
                }
            });

 });
  $('#clear_log').on('click',function(){
              $.each($('.delete_log'), function( index, value ) {
         var lid=$(this).attr('data-id');
         alert(lid);
         $.ajax({
            url: "<?php echo base_url() ?>user/user/delete_log/"+lid,
            type: "POST",
            success: function(data){
                // alert($(this).parent(".contentTimeline").html());
                    $('#log'+lid+':after').css('display','none');
                    $('#log'+lid).remove();
                    $('.timeline').remove();
                }
            });
     });

 });



});
</script>