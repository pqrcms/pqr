<!DOCTYPE html>
<html>
      <head>
            <title>Emirates CMS Power Company</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/reset.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/resources/bootstrap/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/resources/font-awesome/css/font-awesome.min.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/animate.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/sweetalert.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/style.css">
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/jquery.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/resources/bootstrap/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/wow.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/sweetalert.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/custom.js"></script>
      </head>
      <body>
            <header>
                  <div class="container">
                        <div class="row">
                              <div class="col-md-12">
                                    <figure>
                                          <a href="javascript:void(0);"><img width="273" src="<?php echo base_url(); ?>assets_user/images/logo.png" alt=""></a>
                                    </figure>
                              </div>
                        </div>
                  </div>
            </header>
            <section class="login-port skyblue-bg">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-8">
                                    <div class="right-part white">
                                          <h3 class="semibold">Welcome to Emirates Employee Training Management !</h3>
                                          <p class="light">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, <br> consectetur, adipisci velit...</p>
                                    </div>
                              </div>
                             
                              <div class="col-md-4">
                                    <div class="left-part">
                                          <h3 class="white bold">Forgot password</h3>
                                          <form id="email_submit" class="noradius white-bg">
                                          <span id="email_error"></span>
                                              <div class="form-group">
                                                <?php if($this->session->flashdata('message')) {?>
                                                 <label><span style="color: #CC6633"><?php echo $this->session->flashdata('message');?><span></label>
                                                <?php }?>
                                               
                                              </div>
                                              <div class="form-group">
                                                <label for="txtLoginid">Enter Email Id</label>
                                                 <input name="email" type="emailid" size="25" id="email" placeholder="Enter your email here.." class="form-control" value="<?php echo set_value('email');?>" />
                                                 <span style="color:red"><?php echo form_error('email');?></span>
                                              </div>
                                              <div>
                                                  <button type="submit" id="submit" name="submit" class="hover-bottom">Submit</button>
                                              </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                  </div>
            </section>
            <section class="blue-bg" id="login-min">
                  <figure>
                        <img src="images/login-banner.png" alt="">
                  </figure>
            </section>
            <footer>
                  <div class="container">
                        <div class="row">
                              <div class="col-md-6">
                                    <div class="left">
                                          <p>Copyright &copy; <span id="current-year"></span> Emirates CMS Power Company. All Rights Reserved.</p>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="right">
                                          <ul class="inline">
                                                <li><a href="">About us</a></li>
                                                <li><a href="">Help Centre</a></li>
                                                <li><a href="">Contact Us</a></li>
                                                <li><a href="">FAQ’s</a></li>
                                          </ul>
                                    </div>
                              </div>
                        </div>
                  </div>
            </footer>
      </body>
</html>

<script>
 $('#email_submit').on('submit',function(e){
    e.preventDefault();
    //alert('asdas');
    if($("#email").val() == '')
    {
      $("#email").focus().attr("placeholder","Please Enter Your Registered Email").val('');
      $("#email").css("border-color","red");
      return false;
    }

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if(!(emailReg.test($("#email").val())))
    {
      $("#email").focus().attr("placeholder","Please Enter Valid Email Formate").val('');
      $("#email").css("border-color","red");
      return false;
    }

    var email = $("#email").val();

    var dataString = {email:email};

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "user/user/validate_email",
      data: dataString,
      success: function(res){
        //alert(res);
        if(res == 1)
        {
          setTimeout(function(){
          window.location="<?php echo base_url(); ?>user/user/home";
          },1000);

        }
        else if(res == 2)
        {
           $("#email_error").html("Please Enter Registered Email Id");
        }
      }
    });
    return false;
  });
</script>