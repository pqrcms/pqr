<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Questions
    </h1>
  </section>
  <br>
  
  <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                                     <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">

            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>Type</td>
                <td>Question</td>
                <td>Option_one</td>
                <td>Option_two</td>
                <td>Option_three</td>
                <td>Option_four</td>
                <td>Answare</td>
                <td>Marks</td>   
                <td>Action</td>
              </tr>
          </thead>
<div class="box-footer">
<a class="btn btn-success" href="<?php echo base_url('admin/manage_questions/add'); ?>">Add Questions</a>
<a class="btn btn-success" href="<?php echo base_url('admin/add_multiple/index'); ?>">Add .CSV File For MCQ</a>
<a class="btn btn-success" href="<?php echo base_url('admin/add_multiple_essay/index'); ?>">Add .CSV File For ESSAY</a>
</div>    
        <tbody>
                <?php $i=0; ?>
          <?php foreach($manage_questions as $r){ ?>
            <tr>
                <td><?php echo ++$i; ?></td> 
                <td><?php 
                    if($r->type == 1){ 
                      echo "MCQ";
                    }else{
                      echo "ESSAY";
                    } ?> 
                </td>
                
                <td><?php echo $r->question; ?></td>
                <td><?php echo $r->option_one; ?></td>
                <td><?php echo $r->option_two; ?></td>
                <td><?php echo $r->option_three; ?></td>
                <td><?php echo $r->option_four; ?></td>
                <td><?php echo $r->answer; ?></td>
                <td><?php echo $r->marks; ?></td>

                  <td>
                    <a href="<?php echo base_url('admin/manage_questions/edit/'.$r->id); ?>">Edit</a> | 
                    <a href="<?php echo base_url('admin/manage_questions/remove/'.$r->id); ?>">Delete</a>
                  </td>
             </tr>
          <?php } ?>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">  
                <td>Sr No</td>
                <td>Type</td>
                <td>Question</td>
                <td>Option_one</td>
                <td>Option_two</td>
                <td>Option_three</td>
                <td>Option_four</td>
                <td>Answare</td>  
                <td>Marks</td>  
                <td>Action</td>

            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
