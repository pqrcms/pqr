<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
 <script>
  // tinymce.init({
  //   selector: '#essay'
  // });
  //tinymce.init({ selector: "textarea", setup: function (editor) { editor.on('change', function () { editor.save(); }); } });
</script>

<div class="content-wrapper">
	<div class="container">
	    <h1 class="well">Select Type To Add Question</h1>
		<div class="col-lg-12 well">
		<div class="row">
			<form enctype="multipart/form-data" method="POST" id="question_form" class="question_form" action="" >
                <div class="col-sm-12 box-body">
         			<div class=" col-sm-6 form-group">
						<span class="text-danger">*</span>Select Type Of Question: 
						<?php //print_r($types); ?>
						<select id="type" class="form-control" name="type">
						<option value="0">Select type</option>
						<?php 
						    foreach($types as $type)
						    {
						       echo '<option value="'.$type->id.'">'.$type->type.'</option>';
						    }
						?>
						</select>
						<span id="type_error" class="text-danger"><?php echo form_error('type');?></span>
					</div>
					
	<div class="col-sm-12 form-group">
		<span class="text-danger">*</span>Enter Your Question : 
		<input type="text" placeholder="Enter Question Here.." id="question" class="form-control" name="question" value="<?php echo $this->input->post('question'); ?>" />
		<span id="question_error" class="text-danger"><?php echo form_error('question');?></span>
	</div>

	<div id="mcq_div" style="display: none;">
			<div class="col-sm-6 form-group">
				<span class="text-danger">*</span>Enetr Options 1 : 
				<input type="text" placeholder="Enter Options 1 Here.." class="form-control" id="option_one" name="option_one" value="<?php echo $this->input->post('options'); ?>" />
				<span id="option_one_error" class="text-danger"><?php echo form_error('options');?></span>
			</div>
			<div class="col-sm-6 form-group">
				<span class="text-danger">*</span>Options 2 : 
				<input type="text" placeholder="Enter Options 2 Here.." class="form-control" id="option_two" name="option_two" value="<?php echo $this->input->post('options'); ?>" />
				<span id="option_two_error" class="text-danger"><?php echo form_error('options');?></span>
			</div>
			<div class="col-sm-6 form-group">
				<span class="text-danger">*</span>Options 3 : 
				<input type="text" placeholder="Enter Options 3 Here.." class="form-control" id="option_three" name="option_three" value="<?php echo $this->input->post('options'); ?>" />
				<span id="option_three_error" class="text-danger"><?php echo form_error('options');?></span>
			</div>
			<div class="col-sm-6 form-group">
				<span class="text-danger">*</span>Options 4 : 
				<input type="text" placeholder="Enter Options 4 Here.." class="form-control" id="option_four" name="option_four" value="<?php echo $this->input->post('options'); ?>" />
				<span id="option_four_error" class="text-danger"><?php echo form_error('options');?></span>
			</div>
			<div class="col-sm-6 form-group">
				<span class="text-danger">*</span>Answer : 
				<input type="text" placeholder="Enter Answer Here.." class="form-control" id="answer" name="answer" value="<?php echo $this->input->post('answer'); ?>" />
				<span id="answer_error" class="text-danger"><?php echo form_error('answer');?></span>
			</div>
			<div class="col-sm-6 form-group">
				<span class="text-danger">*</span>Marks : 
				<input type="text" placeholder="Enter Marks for the question Here.." class="form-control" id="marks" name="marks" value="<?php echo $this->input->post('marks'); ?>" />
				<span id="marks_error" class="text-danger"><?php echo form_error('marks');?></span>
			</div>
			<div class=" col-sm-6 form-group">
				<span class="text-danger">*</span>Select Tasks : 
				<?php //print_r($training_model); ?>
				<select id="task_id" class="form-control" name="task_id">
				<option value="0">Select tasks</option>
				<?php 
				    foreach($training_model as $index => $option)
				    {
				       echo '<option value="'.$option->id.'">'.$option->title.'</option>';
				    }
				?>
				</select>
				<span id="task_error" class="text-danger"><?php echo form_error('task_id');?></span>
			</div>
	</div>

	<!-- <div class="col-sm-12 form-group" id="essay_div" style="display: none;">
		<div class="form-group">
			<span class="text-danger">*</span>Enter Essay : 
			<textarea id="essay" placeholder="Enter Essay Here.." name="essay" class="form-control"><?php //echo $this->input->post('essay'); ?>
			</textarea>
			<span id="essay_error" class="text-danger"><?php //echo form_error('essay');?></span>
		</div>
	</div> -->
	<div id="essay_div" style="display: none;">
	<div class="col-sm-6 form-group">
		<span class="text-danger">*</span>Select Tasks : 
		<?php //print_r($training_model); ?>
		<select id="task_id" class="form-control" name="task">
		<option value="0">Select tasks</option>
		<?php 
		    foreach($training_model as $index => $option)
		    {
		       echo '<option value="'.$option->id.'">'.$option->title.'</option>';
		    }
		?>
		</select>
		<span id="task_error" class="text-danger"><?php echo form_error('task_id');?></span>
	</div>
		<div class="col-sm-6 form-group">
				<span class="text-danger">*</span>Marks : 
				<input type="text" placeholder="Enter Marks for the question Here.." class="form-control" id="marks" name="mark" value="<?php echo $this->input->post('marks'); ?>" />
				<span id="marks_error" class="text-danger"><?php echo form_error('marks');?></span>
		</div>
	</div>

    </div>
    <div class="form-group" style="margin-left: 25px;">
        <button type="submit" id="sub" class="btn btn-success">Submit</button>
    </div>   
            </form>
        </div><!-- /.box -->
</div>



<script type="text/javascript">
$('body').on('change','#type',function(e) 
{
	//alert('dsad');
	$('#type_error').html("");
	var type = $('#type').val();
	if(type == 1)
	{
		$('#essay_div').hide();
		$('#mcq_div').show();
	}else{
		$('#mcq_div').hide();
		$('#essay_div').show();
	}
});

$("#question_form").submit(function(e) 
{
    e.preventDefault();

    var type = $('#type').val();
    var question = $('#question').val();

    if(type == 0)
    {
    	$('#type_error').html("plese select type");
    	return false;
    }

    if(question == "")
    {
    	$('#question_error').html("plese enter question");
    	return false;
    }

    if(type == 1)
    {
    	var task_id = $('#task_id').val();
		var option1 = $('#option_one').val();
		var option2 = $('#option_two').val();
		var option3 = $('#option_three').val();
		var option4 = $('#option_four').val();
		var answer = $('#answer').val();
		var marks = $('#marks').val();
		
		if(option1 == "")
	    {
	    	$('#option_one_error').html("plese enter option one");
	    	return false;
	    }
	    if(option2 == "")
	    {
			$('#option_two_error').html("plese enter option two");
	    	return false;
	    }
	    if(option3 == "")
	    {
			$('#option_three_error').html("plese enter option three");
	    	return false;
	    }
	    if(option4 == "")
	    {
			$('#option_four_error').html("plese enter option four");
	    	return false;
	    }
	    if(answer == "")
	    {
			$('#answer_error').html("plese enter answer");
	    	return false;
	    }
	    if(task_id == "")
	    {
	    	$('#task_error').html("plese select task");
	    	return false;
	    }
	    if(marks == "")
	    {
	    	$('#marks_error').html("plese enter marks");
	    	return false;
	    }

    }else{
    	if(task_id == "")
	    {
	    	$('#task_error').html("plese select task");
	    	return false;
	    }
	    if(marks == "")
	    {
	    	$('#marks_error').html("plese enter marks");
	    	return false;
	    }
    	//essay related validation
    }
    var data = $('#question_form').serializeArray();

    $.ajax({
        type:"POST",
         url: "<?php echo base_url(); ?>" + "/admin/manage_questions/save_questions",
       // url: <?php// echo baseurl()+"/admin/manage_questions/add" ?>,
        //crossDomain: true,
        //dataType:"json",
      	data:data,
        beforeSend: function() {
        },
        success: function(data) 
        { 
       // alert(data);
        if(data == 200)
        {
          window.location.replace("<?php echo base_url(); ?>" + "/admin/manage_questions");
        }else if(data == 100){
        	alert("something went wrong ");
        }
        },
        complete: function() {
        } 
      });  
});



$(function(){  
  	$('#question').keypress(function(){
      $('#question_error').html("");
    });
    $('#option_one').keypress(function(){
      $('#option_one_error').html("");
    });
    $('#option_two').keypress(function(){
      $('#option_two_error').html("");
    });
    $('#option_three').keypress(function(){
      $('#option_three_error').html("");
    });
    $('#option_four').keypress(function(){
      $('#option_four_error').html("");
    });
    $('#answer').keypress(function(){
      $('#answer_error').html("");
    });
    $('#essay').keypress(function(){
      $('#essay_error').html("");
    });


});

</script>