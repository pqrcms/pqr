<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
  <script>
  // tinymce.init({
  //   selector: '#essay'
  // });
</script>


<div class="content-wrapper">
	<div class="container">
      <h2 class="box-title">Edit Question</h2>
      <div class="col-lg-12 well">
        <div class="row">
<?php echo form_open('admin/manage_questions/edit/'.$manage_questions['id']); ?>
	
	<?php //print_r($manage_questions); ?>
	<?php //echo ($manage_questions['type']);  ?>
	<div>
		<span class="text-danger">*</span>Type : 
                  <select readonly name="type"  id="type" class="form-control">
                  <option value="">Select type</option>
                  <?php foreach($types as $t) { ?>
                    <option value="<?php echo $t->id?>" <?php  if($manage_questions['type'] == $t->id) echo "selected"; ?>><?php echo $t->type; ?></option>
                    <?php } ?>
                  </select>
                   <span id="type_error" class="text-danger"><?php echo form_error('type'); ?></span>
	</div>
	<div>
		<span class="text-danger">*</span>Question : 
		<input type="text" name="question" class="form-control" value="<?php echo ($this->input->post('question') ? $this->input->post('question') : $manage_questions['question']); ?>" />
		<span id="question" class="text-danger"><?php echo form_error('question');?></span>
	</div>
<?php  if($manage_questions['type'] == 1){ ?>

	<div id="mcq_div">
	<div>
		<span class="text-danger">*</span>Options 1: 
		<input type="text" name="option_one" class="form-control" value="<?php echo ($this->input->post('option_one') ? $this->input->post('option_one') : $manage_questions['option_one']); ?>" />
		<span id="option_one_error" class="text-danger"><?php echo form_error('option_one');?></span>
	</div>
	<div>
		<span class="text-danger">*</span>Options 2: 
		<input type="text" name="option_two" class="form-control" value="<?php echo ($this->input->post('option_two') ? $this->input->post('option_two') : $manage_questions['option_two']); ?>" />
		<span id="option_two_error" class="text-danger"><?php echo form_error('option_two');?></span>
	</div>
	<div>
		<span class="text-danger">*</span>Options 3: 
		<input type="text" name="option_three" class="form-control" value="<?php echo ($this->input->post('option_three') ? $this->input->post('option_three') : $manage_questions['option_three']); ?>" />
		<span id="option_three_error" class="text-danger"><?php echo form_error('option_three');?></span>
	</div>
	<div>
		<span class="text-danger">*</span>Options 4: 
		<input type="text" name="option_four" class="form-control" value="<?php echo ($this->input->post('option_four') ? $this->input->post('option_four') : $manage_questions['option_four']); ?>" />
		<span id="option_four_error" class="text-danger"><?php echo form_error('option_four');?></span>
	</div>
	<div>
		<span class="text-danger">*</span>Answer : 
		<input type="text" name="answer" class="form-control" value="<?php echo ($this->input->post('answer') ? $this->input->post('answer') : $manage_questions['answer']); ?>" />
		<span id="answer_error" class="text-danger"><?php echo form_error('answer');?></span>
	</div>
	<div>
		<span class="text-danger">*</span>Marks : 
		<input type="text" name="marks" class="form-control" value="<?php echo ($this->input->post('marks') ? $this->input->post('marks') : $manage_questions['marks']); ?>" />
		<span id="answer_error" class="text-danger"><?php echo form_error('marks');?></span>
	</div>
	</div>
<?php }else{ ?>

	<div id="essay">
		
	<!-- <div>
		<?php //echo ($this->input->post('essay') ? $this->input->post('essay') : $manage_questions['essay']); ?>
		<textarea id="essay" name="essay" class="form-control"></textarea>
		<span id="essay_error" class="text-danger"><?php //echo form_error('essay');?></span>
	</div>
	</div> -->
	<div>
		<span class="text-danger">*</span>Marks : 
		<input type="text" name="marks" class="form-control" value="<?php echo ($this->input->post('marks') ? $this->input->post('marks') : $manage_questions['marks']); ?>" />
		<span id="answer_error" class="text-danger"><?php echo form_error('marks');?></span>
	</div>



<?php } ?>	
	<div class="box-footer">
        <button type="submit" id="sub" class="btn btn-success">Save</button>
    </div>
	
<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
