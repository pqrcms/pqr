<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>

<div class="content-wrapper">
  <div class="container">
      <h1 class="well">Add Orientation Tasks</h1>
    <div class="col-lg-12 well">
    <div class="row">
               <form enctype="multipart/form-data" method="POST" id="news" class="news" action="<?php echo base_url();?>admin/ot_tasks/add">
                   <div class="box-body">

                    <div class="form-group" >
                          <label for="exampleInputPassword1">Enter Orientation Tasks Name *</label>
                          <input type="text" class="form-control" name="tasks" id="tasks" placeholder="Enter orientation tasks here..">
                        <?php echo form_error('tasks'); ?>
                    </div>

                  <div class="box-footer">
                    <button type="submit" id="sub" class="btn btn-success">Submit</button>
                  </div>
                  </div>

                </form>
        </div><!-- /.box -->
</div>
</div>
</div>