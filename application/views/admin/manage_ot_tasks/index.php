<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Orientation Tasks
    </h1>
  </section>
  <br>
 <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                                     <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">

            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>Tasks</td>  
                <td>Action</td>
              </tr>
          </thead>
<div class="box-footer">
  <a class="btn btn-success" href="<?php echo base_url('admin/ot_tasks/add'); ?>">Add Orientation Tasks</a>
</div>

        
        <tbody>
                <?php $i=0; ?>
          <?php foreach($ot_tasks as $r){ ?>
            <tr>
                <td><?php echo ++$i; ?></td> 
                <td> <?php echo $r->tasks; ?> </td>
                  <td>
                    <a href="<?php echo base_url('admin/ot_tasks/edit/'.$r->id); ?>">Edit</a> | 
                    <a href="<?php echo base_url('admin/ot_tasks/remove/'.$r->id); ?>">Delete</a>
                  </td>
             </tr>
          <?php } ?>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">  
               <td>Sr No</td>
                <td>Tasks</td>  
                <td>Action</td>

            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
