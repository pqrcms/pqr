<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Manage Users
          </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                                     <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>User Name</td> 
                <td>User Email</td> 
                <td>User Mobile</td> 
                <td>Status</td>    
                <td>Action</td>
              </tr>
          </thead>
          <tbody>
                <?php $i=0; ?>
          <?php foreach($user as $u){ ?>
            <tr>
                <td><?php echo ++$i; ?></td> 
                <td> <?php echo $u->user_name; ?> </td>
                <td> <?php echo $u->user_email; ?> </td>
                <td> <?php echo $u->user_mobile; ?> </td>  
                <td> 
                    <?php if($u->status == 1){ ?> 
                        <span class="badge bg-green">Active</span> 
                    <?php } else if($u->status == 2){ ?> 
                        <span class="badge bg-red">Not Active</span> 
                    <?php }?>
                    
                </td>        
              <td>
                  <?php if($u->status == 1){ ?> 
                      <a href='<?php echo base_url("admin/user/deactive/" . $u->user_id.'/2'); ?>' onClick ="return confirm('Do you want to Deactivate this user')"><button class=' btn btn-info'>Deactivate</button></a>
                  <?php } else if($u->status == 2){ ?> 
                      <a href='<?php echo base_url("admin/user/active/" . $u->user_id.'/1'); ?>' onClick ="return confirm('Do you want to Activate this user')"><button class=' btn btn-success'>Activate</button></a>
                   <?php }?>
                   <a href='<?php echo base_url("admin/user/view/" . $u->user_id); ?>'><button class=' btn btn-warning'>View Profile</button></a>
                </td>
             </tr>
          <?php } ?>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">  
               <td>Sr No</td>
                <td>Department Name</td> 
                <td>Added Date</td>     
                <td>Action</td>
            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
