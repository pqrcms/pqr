<body class="hold-transition skin-blue sidebar-mini">
      <div class="wrapper">

        <header class="main-header">
          <!-- Logo -->
          <a href="" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">PQR</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Emirates CMS Power Company</b></span>
          </a>
          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">                  
                    <span class="hidden-xs"><?php echo $this->session->userdata['admin_logged_in']['email_address']; ?></span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <img src="<?php echo base_url(); ?>assets_admin/image/avatar5.png" class="img-circle" alt="User Image">
                     
                    </li>
                 
                    <!-- Menu Footer-->
                    <li class="user-footer">
                      <div class="pull-left">   
                      </div>
                        <div class="pull-right">
                          <a href="<?php echo base_url(); ?>admin/logout" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                  </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                <!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
                </li>
              </ul>
            </div>
          </nav>
        </header>