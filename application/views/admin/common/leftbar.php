<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
        
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
        
          <li class="treeview active">
              <!-- <a href="#">
                <i class="fa fa-table"></i> <span>Menu</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a> -->
         
            <li class="treeview main_table">
            <a href="<?php echo base_url('admin/dashboard'); ?>">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              <span class="pull-right-container">
              </span>
            </a>
         

            <a href="<?php echo base_url('admin/manage_role'); ?>">
              <i class="fa fa-user"></i> <span>Manage Role</span>
              <span class="pull-right-container">
              </span>
            </a>

            <a href="<?php echo base_url('admin/manage_questions'); ?>">
              <i class="fa fa-cubes"></i> <span>Manage Questions</span>
              <span class="pull-right-container">
              </span>
            </a>
            <a href="<?php echo base_url('admin/training_models'); ?>">
              <i class="fa fa-cubes"></i> <span>Training Model</span>
              <span class="pull-right-container">
              </span>
            </a>
            <a href="<?php echo base_url('admin/coordinater'); ?>">
              <i class="fa fa-dashboard"></i> <span>Co-Ordinater</span>
              <span class="pull-right-container">
              </span>
            </a>
            <a href="<?php echo base_url('admin/complited_tasks'); ?>">
              <i class="fa fa-dashboard"></i> <span>Complited Tasks</span>
              <span class="pull-right-container">
              </span>
            </a>
            <a href="<?php echo base_url('admin/manage_feedback'); ?>">
              <i class="fa fa-dashboard"></i> <span>View FeedBacks</span>
              <span class="pull-right-container">
              </span>
            </a>
            <a href="<?php echo base_url('admin/add_material'); ?>">
              <i class="fa fa-paperclip"></i> <span>Add Trainings Materials</span>
              <span class="pull-right-container">
              </span>
            </a>
            
<!--             <a href="<?php //echo base_url('admin/add_multiple'); ?>">
              <i class="fa fa-dashboard"></i> <span>Add Multiple</span>
              <span class="pull-right-container">
              </span>

            </a> -->
<!--             <li class="treeview active">
                  <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>Manage Roles</span>
                    <i class="fa pull-right fa-angle-down"></i>
                  </a>
                <ul class="treeview-menu" style="display: block;">
                  <li><a href="<?php //echo base_url('admin/manage_role'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Manage Roles</a>
                  </li>
                </ul>
                <ul class="treeview-menu" style="display: block;">
                    <li><a href="<?php //echo base_url('admin/manage_role/add'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Add Roles</a>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="<?php //echo base_url('admin/Emp_details/add'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> New Employees</a>
                    </li>
                </ul>
            </li> -->




            <!-- <a href="<?php //echo base_url('admin/pqr_software_training'); ?>">
              <i class="fa fa-dashboard"></i> <span>PQR- Software Trainings</span>
              <span class="pull-right-container">
              </span>
            </a> -->
            
           <!--  <a href="<?php //echo base_url('admin/standard_normal_training'); ?>">
              <i class="fa fa-dashboard"></i> <span>Standard/Normal Training</span>
              <span class="pull-right-container">
              </span>
            </a> -->
          </li>  
        </section>
        <!-- /.sidebar -->
      </aside>
