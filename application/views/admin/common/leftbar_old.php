      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
 
         
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
        
            <li class="treeview active">
              <a href="#">
                <i class="fa fa-table"></i> <span>Menu</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/dashboard');  ?>"><i class="fa fa-circle-o"></i>Dashboard</a></li>
              </ul>

              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal');  ?>"><i class="fa fa-circle-o"></i>Manage Journal</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/student');  ?>"><i class="fa fa-circle-o"></i>Manage Students</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/area_of_interest');  ?>"><i class="fa fa-circle-o"></i>Manage Area of Interest</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/package');  ?>"><i class="fa fa-circle-o"></i>Manage Package</a></li>
              </ul>

               <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/package/price');  ?>"><i class="fa fa-circle-o"></i>Manage Price</a></li>
              </ul>

              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/package/publication');  ?>"><i class="fa fa-circle-o"></i>Manage Publication</a></li>
              </ul>

              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/teachers');  ?>"><i class="fa fa-circle-o"></i>Manage Teachers</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/about');  ?>"><i class="fa fa-circle-o"></i>Manage About Gsper</a></li>
              </ul>
               <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/statistics');  ?>"><i class="fa fa-circle-o"></i>Manage Statistics</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/research_topics');  ?>"><i class="fa fa-circle-o"></i>Manage Research Topics</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/editorial');  ?>"><i class="fa fa-circle-o"></i>Manage Editorial Topics</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/guidelines');  ?>"><i class="fa fa-circle-o"></i>Manage Guidelines</a></li>
              </ul>

              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/image_add');  ?>"><i class="fa fa-circle-o"></i>Manage Journal Images</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/home_image');  ?>"><i class="fa fa-circle-o"></i>Manage Journal Home Images</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/volume');  ?>"><i class="fa fa-circle-o"></i>Manage Volume</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/digital');  ?>"><i class="fa fa-circle-o"></i>Manage Digital Library</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/home');  ?>"><i class="fa fa-circle-o"></i>Manage Home Image</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/home/choose_us');  ?>"><i class="fa fa-circle-o"></i>Manage Why Choose Us</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/home/news');  ?>"><i class="fa fa-circle-o"></i>Manage News</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/digital/digital_home_image');  ?>"><i class="fa fa-circle-o"></i>Manage Digital Home page</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/journal/right_panel');  ?>"><i class="fa fa-circle-o"></i>Manage Right Panel</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/reviewer');  ?>"><i class="fa fa-circle-o"></i>Manage Reviewer</a></li>
              </ul>

              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/student/to_do_list');  ?>"><i class="fa fa-circle-o"></i>To do list</a></li>
              </ul>

              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/student/submit_paper');  ?>"><i class="fa fa-circle-o"></i>In Process</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/reviewer/question_answers');  ?>"><i class="fa fa-circle-o"></i>Manage Question Answers</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/publication');  ?>"><i class="fa fa-circle-o"></i>Manage Publication</a></li>
              </ul>
              <ul class="treeview-menu">
                <li ><a href="<?php echo base_url('admin/home/discount_coupon');  ?>"><i class="fa fa-circle-o"></i>Discount Coupon</a></li>
              </ul>

              
             
            
            </li>        
        </section>
        <!-- /.sidebar -->
      </aside>
     