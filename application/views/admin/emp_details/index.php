<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Employees Details
    </h1>
  </section>
  <br>
 <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>First Name</td> 
                <td>Last Name</td>
                <td>Address</td>
                <td>City</td>
                <td>State</td>
                <td>Zip</td>
                <td>Department</td>
                <td>Position</td>
                <td>DOB</td>
                <td>Employee ID</td>
                <td>DOJ</td>
                <td>Phone Number</td>
                <td>Email Id</td>
                <!-- <td>Password</td> -->
                <td>Skills</td>
                <td>Action</td>
              </tr>
          </thead>
<div class="box-footer">
  <a class="btn btn-success" href="<?php echo base_url('admin/emp_details/add'); ?>">Add Employee</a>
  <a class="btn btn-success" href="<?php echo base_url('admin/emp_details/add_file'); ?>">Add Black Employee</a>
</div>

        
        <tbody>
          <?php $i=0; ?>
          <?php foreach($emp_details as $r){ ?>     
            <?php if($r['status'] == 0) {?>
            <tr>
                <td><?php echo ++$i; ?></td> 
                <td><?php echo $r['emp_name']; ?></td>
                <td><?php echo $r['emp_last']; ?></td>
                <td><?php echo $r['address']; ?></td>
                <td><?php echo $r['city']; ?></td>
                <td><?php echo $r['state']; ?></td>
                <td><?php echo $r['zip']; ?></td>
                <td><?php echo $r['department']; ?></td>
                <td><?php echo $r['position']; ?></td>
                <td><?php echo $r['dob']; ?></td>
                <td><?php echo $r['employee_id']; ?></td>
                <td><?php echo $r['doj']; ?></td>
                <td><?php echo $r['phone_num']; ?></td>
                <td><?php echo $r['email']; ?></td>
                <td><?php echo $r['skills']; ?></td>
                  <td>
                    <a href="<?php echo base_url('admin/emp_details/edit/'.$r['emp_id']); ?>">Edit</a> | 
                    <a href="<?php echo base_url('admin/emp_details/remove/'.$r['emp_id']); ?>">Delete</a> |
                    <a href="<?php echo base_url('admin/emp_details/remove_list/'.$r['emp_id']); ?>">Hide</a>
                  </td>
             </tr>
            <?php } ?>
          <?php } ?>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">  
                <td>Sr No</td>
                <td>First Name</td> 
                <td>Last Name</td>
                <td>Address</td>
                <td>City</td>
                <td>State</td>
                <td>Zip</td>
                <td>Department</td>
                <td>Position</td>
                <td>DOB</td>
                <td>Employee ID</td>
                <td>DOJ</td>
                <td>Phone Number</td>
                <td>Email Id</td>
                <!-- <td>Password</td> -->
                <td>Skills</td>
                <td>Action</td>

            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
