<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>


<div class="content-wrapper">
<?php echo form_open('admin/emp_details/edit/'.$emp_details['emp_id']); ?>
<?php //print_r($emp_details); ?>
<?php //echo ($emp_details['emp_id']);  ?>
<!-- general form elements -->
<div class="container">
    <h1 class="well">Edit Employee Details</h1>
	<div class="col-lg-12 well">
	<div class="row">
				    <form enctype="multipart/form-data" method="POST" id="news" class="news" action="<?php echo base_url();?>admin/Emp_details/edit">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>Employee First Name </label>
								<input type="text" placeholder="Enter First Name Here.." name="emp_name" class="form-control" value="<?php echo ($this->input->post('emp_name') ? $this->input->post('emp_name') : $emp_details['emp_name']); ?>" />
								<span class="text-danger"><?php echo form_error('emp_name');?></span>
							</div>
							<div class="col-sm-6 form-group">
								<label>Employee Last Name</label>
								<input type="text" placeholder="Enter Last Name Here.." name="emp_last" class="form-control" value="<?php echo ($this->input->post('emp_last') ? $this->input->post('emp_last') : $emp_details['emp_last']); ?>" />
								<span class="text-danger"><?php echo form_error('emp_last');?></span>
							</div>
						</div>					
						<div class="form-group">
							<label>Address</label>
							<textarea name="address" placeholder="Enter Address Here.." rows="3" class="form-control" value="<?php echo ($this->input->post('address') ? $this->input->post('address') : $emp_details['address']); ?>"><?php echo ($this->input->post('address') ? $this->input->post('address') : $emp_details['address']); ?></textarea>
							<span class="text-danger"><?php echo form_error('address');?></span>
						</div>	
						<div class="row">
							<div class="col-sm-4 form-group">
								<label>City</label>
								<input type="text" placeholder="Enter City Name Here.." name="city" class="form-control" value="<?php echo ($this->input->post('city') ? $this->input->post('city') : $emp_details['city']); ?>" />
								<span class="text-danger"><?php echo form_error('city');?></span>
							</div>	
							<div class="col-sm-4 form-group">
								<label>State</label>
								<input type="text" placeholder="Enter State Name Here.." name="state" class="form-control" value="<?php echo ($this->input->post('state') ? $this->input->post('state') : $emp_details['state']); ?>" />
								<span class="text-danger"><?php echo form_error('state');?></span>
							</div>	
							<div class="col-sm-4 form-group">
								<label>Zip</label>
								<input type="text" placeholder="Enter Zip Code Here.." name="zip" class="form-control" value="<?php echo ($this->input->post('zip') ? $this->input->post('zip') : $emp_details['zip']); ?>" />
								<span class="text-danger"><?php echo form_error('zip');?></span>
							</div>		
						</div>
						<div class="row">
							<div class="col-sm-4 form-group">
								<label>Designation</label>
								<input type="text" placeholder="Enter Designation Here.." name="position" class="form-control" value="<?php echo ($this->input->post('position') ? $this->input->post('position') : $emp_details['position']); ?>" />
							<span class="text-danger"><?php echo form_error('position');?></span>
							</div>		
							<div class="col-sm-4 form-group">
								<label>Department</label>
								<input type="text" placeholder="Enter Department Here.." name="department" class="form-control" value="<?php echo ($this->input->post('department') ? $this->input->post('department') : $emp_details['department']); ?>" />
								<span class="text-danger"><?php echo form_error('department');?></span>
							</div>
							<div class="col-sm-4 form-group">
								<label>Date Of Birth</label>
								<input type="date" name="dob" class="form-control" value="<?php echo ($this->input->post('dob') ? $this->input->post('dob') : $emp_details['dob']); ?>" />
							<span class="text-danger"><?php echo form_error('dob');?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>Employee ID</label>
								<input type="text" placeholder="Enter Designation Here.." name="employee_id" class="form-control" value="<?php echo ($this->input->post('employee_id') ? $this->input->post('employee_id') : $emp_details['employee_id']); ?>" />
							<span class="text-danger"><?php echo form_error('employee_id');?></span>
							</div>	
							<div class="col-sm-6 form-group">
								<label>Date Of Join</label>
								<input type="date" name="doj" class="form-control" value="<?php echo ($this->input->post('doj') ? $this->input->post('doj') : $emp_details['doj']); ?>" />
							<span class="text-danger"><?php echo form_error('doj');?></span>
							</div>	
							
						</div>							
					<div class="form-group">
						<label>Phone Number</label>
						<input type="text" placeholder="Enter Phone Number Here.." name="phone_num" class="form-control" value="<?php echo ($this->input->post('phone_num') ? $this->input->post('phone_num') : $emp_details['phone_num']); ?>" />
						<span class="text-danger"><?php echo form_error('phone_num');?></span>
					</div>		
					<div class="form-group">
						<label>Email Address</label>
						<input type="email" placeholder="Enter Email Address Here.." name="email" class="form-control" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $emp_details['email']); ?>" />
							<span class="text-danger"><?php echo form_error('email');?></span>
					</div>	
					<div class="form-group">
						<label>Password</label>
						<input type="password" placeholder="Enter Password Here.." name="password" class="form-control" value="" />
							<span class="text-danger"><?php echo form_error('password');?></span>
					</div>
					<div class="form-group">
						<label>Skills</label>
						<input type="text" placeholder="Enter Skills Here.." name="skills" class="form-control" value="<?php echo ($this->input->post('skills') ? $this->input->post('skills') : $emp_details['skills']); ?>" />
							<span class="text-danger"><?php echo form_error('skills');?></span>
					</div>
					<button type="submit" class="btn btn-lg btn-info">Submit</button>					
					</div>
				</form> 
				</div>
	</div>
	</div>
	<?php echo form_close(); ?>
	</div>