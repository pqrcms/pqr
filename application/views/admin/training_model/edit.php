<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>


<div class="content-wrapper">
<?php echo form_open('admin/training_models/edit/'.$training_model['id']); ?>

<!-- general form elements -->
<div class="container">
    <h1 class="well">Edit Training Details</h1>
	<div class="col-lg-12 well">
	<div class="row">
				    <form enctype="multipart/form-data" method="POST" id="news" class="news" action="<?php echo base_url();?>admin/training_models/edit">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Title</label>
								<input type="text" placeholder="Enter First Name Here.." name="title" class="form-control" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $training_model['title']); ?>" />
								<span class="text-danger"><?php echo form_error('title');?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Discription</label>
								<textarea name="discription" placeholder="Enter discription Here.." rows="3" class="form-control" value="<?php echo ($this->input->post('discription') ? $this->input->post('discription') : $training_model['discription']); ?>"><?php echo ($this->input->post('discription') ? $this->input->post('discription') : $training_model['discription']); ?></textarea>
								<span class="text-danger"><?php echo form_error('discription');?></span>
							</div>
						</div>					
						
						<div class="row">
							<div class="col-sm-4 form-group">
								<label>Number Of Question</label>
								<input type="text" placeholder="Enter City Name Here.." name="no_question" class="form-control" value="<?php echo ($this->input->post('no_question') ? $this->input->post('no_question') : $training_model['no_question']); ?>" />
								<span class="text-danger"><?php echo form_error('no_question');?></span>
							</div>	
							<div class="col-sm-4 form-group">
								<label>Start Date</label>
								<input type="date" name="start_date" class="form-control" value="<?php echo date('Y-m-d',strtotime($training_model['start_date']));?>" />
								<span class="text-danger"><?php echo form_error('start_date');?></span>
							</div>	
							<div class="col-sm-4 form-group">
								<label>Dead line date</label>
								<input type="date" name="dead_line" class="form-control" value="<?php echo date('Y-m-d',strtotime($training_model['dead_line'])); ?>" />
								<span class="text-danger"><?php echo form_error('dead_line');?></span>
							</div>		
						</div>
						<div class="row">
							<div class="col-sm-4 form-group">
								<label>Assign By</label>
								<select id="assign_by" class="form-control" name="assign_by">
								<?php //print_r($coordinater); ?>
								<?php foreach($coordinater as $c) { ?>
								    <option value="<?php echo $c['id'];?>"<?php if($training_model['assign_by'] == $c['id']) echo"selected";?>><?php echo $c['name'];?></option>
								    <?php } ?>
								</select>
							<span class="text-danger"><?php echo form_error('assign_by');?></span>
							</div>		
							<div class="col-sm-4 form-group">
								<label>Department</label>
								<input type="text" placeholder="Enter Department Here.." name="department" class="form-control" value="<?php echo ($this->input->post('department') ? $this->input->post('department') : $training_model['department']); ?>" />
								<span class="text-danger"><?php echo form_error('department');?></span>
							</div>
							<div class="col-sm-4 form-group">
								<label>Designation</label>
								<input type="text" name="designation" class="form-control" value="<?php echo ($this->input->post('designation') ? $this->input->post('designation') : $training_model['designation']); ?>" />
							<span class="text-danger"><?php echo form_error('designation');?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Total Marks</label>
								<input type="text" placeholder="Enter Department Here.." name="total_marks" class="form-control" value="<?php echo ($this->input->post('total_marks') ? $this->input->post('total_marks') : $training_model['total_marks']); ?>" />
								<span class="text-danger"><?php echo form_error('total_marks');?></span>
							</div>
						</div>							
					<button type="submit" class="btn btn-lg btn-info">Submit</button>					
					</div>
				</form> 
				</div>
	</div>
	</div>
	<?php echo form_close(); ?>
	</div>