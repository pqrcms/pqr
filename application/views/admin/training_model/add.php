<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="content-wrapper">
<div class="container">
    <h1 class="well">Training</h1>
	<div class="col-lg-12 well">
	<div class="row">
				    <form enctype="multipart/form-data" method="POST" id="news" class="news" action="<?php echo base_url();?>admin/training_models/add">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Title</label>
								<input type="text" placeholder="Enter title Name Here.." name="title" class="form-control" value="<?php echo $this->input->post('title'); ?>" />
								<span class="text-danger"><?php echo form_error('title');?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Discription</label>
								<textarea name="discription" placeholder="Enter discription Here.." rows="3" class="form-control" value="<?php echo $this->input->post('discription'); ?>"></textarea>
							<span class="text-danger"><?php echo form_error('discription');?></span>
							</div>
						</div>					
						<div class="row">
							<div class="col-sm-4 form-group">
								<label>Number Of Question</label>
								<input type="text" placeholder="Enter Number Of Question Here.." name="no_question" class="form-control" value="<?php echo $this->input->post('no_question'); ?>" />
								<span class="text-danger"><?php echo form_error('no_question');?></span>
							</div>	
							<div class="col-sm-4 form-group">
								<label>Start Date</label>
								<input type="date" name="start_date" class="form-control" value="<?php echo $this->input->post('start_date'); ?>" />
								<span class="text-danger"><?php echo form_error('start_date');?></span>
							</div>	
							<div class="col-sm-4 form-group">
								<label>Dead Line Date</label>
								<input type="date" name="dead_line" class="form-control" value="<?php echo $this->input->post('dead_line'); ?>" />
								<span class="text-danger"><?php echo form_error('dead_line');?></span>
							</div>		
						</div>
						<div class="row">
							<div class="col-sm-4 form-group">
								<label>Assign By</label>
								<?php //print_r($coordinater); ?>
								<select id="assign_by" class="form-control" name="assign_by">
								<option value="0">Select type</option>
								<?php 
								    foreach($coordinater as $index => $option)
								    {
								       echo '<option value="<?php echo $this->input->post('assign_by'); ?>">'.$option->name.'</option>';
								    }
								?>
								</select>
							<span class="text-danger"><?php echo form_error('assign_by');?></span>
							</div>		
							<div class="col-sm-4 form-group">
								<label>Department</label>
								<input type="text" placeholder="Enter Department Here.." name="department" class="form-control" value="<?php echo $this->input->post('department'); ?>" />
								<span class="text-danger"><?php echo form_error('department');?></span>
							</div>
							<div class="col-sm-4 form-group">
								<label>Designation</label>
								<input type="text" placeholder="Enter Designation Here.." name="designation" class="form-control" value="<?php echo $this->input->post('designation'); ?>" />
							<span class="text-danger"><?php echo form_error('designation');?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Total Marks</label>
								<input type="text" placeholder="Enter total marks Here.." name="total_marks" class="form-control" value="<?php echo $this->input->post('total_marks'); ?>" />
								<span class="text-danger"><?php echo form_error('total_marks');?></span>
							</div>
						</div>
					<button type="submit" class="btn btn-lg btn-info">Submit</button>					
					</div>
				</form> 
				</div>
	</div>
	</div>
	</div>

