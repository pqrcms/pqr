<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Training Details
    </h1>
  </section>
  <br>
 <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>Title</td> 
                <td>Discription</td>
                <td>No_question</td>
                <td>Start_date</td>
                <td>Dead_line</td>
                <td>Assign_by</td>
                <td>Department</td>
                <td>Designation</td>
                <td>Total_marks ID</td>
                <td>Action</td>
              </tr>
          </thead>
<div class="box-footer">
  <a class="btn btn-success" href="<?php echo base_url('admin/training_models/add'); ?>">Add Training</a>
</div>

        
        <tbody>
          <?php $i=0; ?>
          <?php foreach($training_model as $r){ ?>     
                <tr>
                <td><?php echo ++$i; ?></td> 
                <td><?php echo $r['title']; ?></td>
                <td><?php echo $r['discription']; ?></td>
                <td><?php echo $r['no_question']; ?></td>
                <td><?php echo $r['start_date']; ?></td>
                <td><?php echo $r['dead_line']; ?></td>
                <td><?php echo $r['assign_by']; ?></td>
                <td><?php echo $r['department']; ?></td>
                <td><?php echo $r['designation']; ?></td>
                <td><?php echo $r['total_marks']; ?></td>
                  <td>
                    <a href="<?php echo base_url('admin/training_models/edit/'.$r['id']); ?>">Edit</a> | 
                    <a href="<?php echo base_url('admin/training_models/delete/'.$r['id']); ?>">Delete</a>
                  </td>
             </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">  
                <td>Sr No</td>
                <td>Title</td> 
                <td>Discription</td>
                <td>No_question</td>
                <td>Start_date</td>
                <td>Dead_line</td>
                <td>Assign_by</td>
                <td>Department</td>
                <td>Designation</td>
                <td>Total_marks ID</td>
                <td>Action</td>
            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
