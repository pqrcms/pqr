<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- Content Wrapper. Contains page content -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
	<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
</head>
<script type="text/javascript">
	$('body').on('change', '#file', function(event)
    {
    	if (this.files && this.files[0]) 
	    {
	        var file_type = this.files[0].type;
	        if(file_type != "application/vnd.ms-excel")
	        {
	          $('#file').val('');
	          alert("please upload .csv file");
	          return false;
	        }
	    }
    });
///////
function readURL(input) {
    
}
</script>
<body>
<div class="content-wrapper">
  <div class="container">
      <h1 class="well">Upload CSV File For Essay</h1>
    <div class="col-lg-12 well">
    <div class="box-body">
		<form class="form-horizontal well" action="<?php echo base_url(); ?>admin/add_multiple_essay/import" method="post" name="upload_excel" enctype="multipart/form-data">
		Select File:<input type="file" name="file" id="file" class="input-large">Allowed files: .CSV file only
		</br>
		<button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload File</button>
		</form>
	</div>
                <!-- /.box -->
</div>
<!-- /.col --
<!-- /.content-wrapper -->
</body>
</html>