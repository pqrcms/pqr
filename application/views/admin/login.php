  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo base_url(); ?>"><b>Admin</b>&nbsp;PQR</a>
      </div><!-- /.login-logo -->
      <span id="login_error"></span>
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p> 
        
        <form method="POST" name="login_form">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="email_address" placeholder="Email" name="email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <span class="error" id="email_error"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" id="password" placeholder="Password" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <span class="error" id="password_error"></span>
          </div>
          <div class="row">
            <div class="col-xs-8"> 
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>
      
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

  <script>
  $(document).ready(function(){
       $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });

       $("form[name='login_form']").on('submit',function(e){
        e.preventDefault();
            $(".error").html('');
            if($("#email_address").val() == '')
            {
              $("#email_address").focus().val('');
              $("#email_error").html("Please Enter Your Email Id...");
              return false;
            }
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(!(emailReg.test($("#email_address").val())))
            {
              $("#email_address").focus().val('');
              $("#email_error").html("Please Enter valid Email id...");
              return false;
            }
            if($("#password").val() == '')
            {
              $("#password").focus().val('');
              $("#password_error").html("Please Enter Your Password");
              return false;
            }

            $.ajax({ 
              type:"POST",
              url:"<?php echo base_url(); ?>admin/login/signin_form",
              data:$("form[name='login_form']").serialize(),
              success:function(result)
              {
                if(result == 2)
                {
                  $("#login_error").html("Username or Password Is Invalid");
                }
                else if(result == 1)
                { alert('success');
                  window.location="<?php echo base_url(); ?>admin/dashboard";
                }
              }
          });
       });
  });

  </script>
