<!-- Content Wrapper. Contains page content -->
 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<div class="content-wrapper">
<!-- general form elements -->
              <div class="box box-primary col-lg-1 col-md-1">
                <div class="box-header with-border">
                  <h2 class="box-title">Add Gallery</h2>
                </div><!-- /.box-header -->
                <!-- form start -->
               <form enctype="multipart/form-data" method="POST" id="gallery" class="gallery" action="<?php echo base_url();?>admin/gallery/ins">
                   <div class="box-body">
                   

                    
                    <div class="form-group" >
                          <label for="exampleInputPassword1"> Upload Images *</label>
                          <input type="file" multiple class="form-control" name="gallery_image[]">
                      
                    </div>

                 

                   
                  <div class="box-footer">
                    <button type="submit" id="sub" class="btn btn-success">Submit</button>
                  </div>
                  </div>

                </form>
              </div><!-- /.box -->
			</div>
      