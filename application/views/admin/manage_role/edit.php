<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>

<div class="content-wrapper">
  <div class="container">
      <h2 class="box-title">Edit Role</h2>
      <div class="col-lg-12 well">
        <div class="row">
               <form enctype="multipart/form-data" method="POST" id="news" class="news" action='<?php echo base_url('admin/manage_role/edit/').$role['id'];?>');>
                <div class="box-body">


                <div>
                <span class="text-danger">*</span>Role : 
                <input type="text" name="role" class="form-control" value="<?php echo ($this->input->post('role') ? $this->input->post('role') : $role['role']); ?>" />
                <span class="text-danger"><?php echo form_error('role');?></span>
                </div>
 
                  <div class="box-footer">
                    <button type="submit" id="sub" class="btn btn-success">Save</button>
                  </div>
                  </div>

                </form>
              </div><!-- /.box -->
        </div>
    </div>
</div>
