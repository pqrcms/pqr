<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Roles
    </h1>
  </section>
  <br>
 <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                  <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>Role</td>  
                <td>Action</td>
              </tr>
          </thead>
<div class="box-footer">
  <a class="btn btn-success" href="<?php echo base_url('admin/ot_tasks'); ?>">Orientation Tasks</a>
  <a class="btn btn-success" href="<?php echo base_url('admin/manage_role/add'); ?>">Add Role</a>
  <a class="btn btn-success" href="<?php echo base_url('admin/Emp_details'); ?>">Employee Details</a>
</div>

        
        <tbody>
                <?php $i=0; ?>
          <?php foreach($roles as $r){ ?>
            <tr>
                <td><?php echo ++$i; ?></td> 
                <td> <?php echo $r->role; ?> </td>
                  <td>
                    <a href="<?php echo base_url('admin/manage_role/edit/'.$r->id); ?>">Edit</a> | 
                    <a href="<?php echo base_url('admin/manage_role/remove/'.$r->id); ?>">Delete</a>
                  </td>
             </tr>
          <?php } ?>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">  
               <td>Sr No</td>
                <td>Role</td>  
                <td>Action</td>

            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
