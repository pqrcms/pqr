<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Complited Tasks
    </h1>
  </section>
  <br>
 <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                  <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>Emp ID</td>
                <td>Completed Time</td> 
                <td>Time Taken</td> 
                <td>Max Marks</td> 
                <td>Mark Obtained</td>
                <td>Total Question Attended  </td>
              </tr>
          </thead> 
        <tbody>
                <?php $i=0; ?>
          <?php foreach($emp_tasks as $r){ ?>
            <tr>
                <td><?php echo ++$i; ?></td> 
                <td> <?php echo $r['emp_id']; ?> </td>
                <td> <?php echo $r['completed_time'];?> </td>
                <td> <?php echo $r['total_time'];?> </td>
                <td> <?php echo $r['max_marks'];?> </td>   
                <td> <?php echo $r['mark_obtained'];?> </td>
                <td> <?php echo $r['total_question_attended'];?> </td>
             </tr>
          <?php } ?>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">  
                <td>Sr No</td>
                <td>Emp ID</td>
                <td>Completed Time</td> 
                <td>Time Taken</td> 
                <td>Max Marks</td> 
                <td>Mark Obtained</td>
                <td>Total Question Attended  </td>
            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
