<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Coordinater Details
    </h1>
  </section>
  <br>
 <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>Emp ID</td> 
                <td>Name</td>
                <td>Email</td>
                <td>Action</td>
              </tr>
          </thead>
<div class="box-footer">
  <a class="btn btn-success" href="<?php echo base_url('admin/coordinater/add'); ?>">Add Coordinater</a>
</div>

        
        <tbody>
          <?php $i=0; ?>
          <?php foreach($coordinater as $r){ ?>     
                <tr>
                <td><?php echo ++$i; ?></td> 
                <td><?php echo $r['emp_id']; ?></td>
                <td><?php echo $r['name']; ?></td>
                <td><?php echo $r['email']; ?></td>
                  <td>
                    <a href="<?php echo base_url('admin/coordinater/edit/'.$r['id']); ?>">Edit</a> | 
                    <a href="<?php echo base_url('admin/coordinater/delete/'.$r['id']); ?>">Delete</a>
                  </td>
             </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">  
                <td>Sr No</td>
                <td>Emp ID</td> 
                <td>Name</td>
                <td>Email</td>
                <td>Action</td>
            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
