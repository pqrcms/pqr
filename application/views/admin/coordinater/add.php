<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="content-wrapper">
<div class="container">
    <h1 class="well">Coordinater Details</h1>
	<div class="col-lg-12 well">
	<div class="row">
				   <form enctype="multipart/form-data" method="POST" id="news" class="news" action="<?php echo base_url();?>admin/coordinater/add">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Enter Coordinater ID</label>
								<input type="text" placeholder="Enter Emp ID Here.." name="emp_id" class="form-control" value="<?php echo $this->input->post('emp_id'); ?>" />
								<span class="text-danger"><?php echo form_error('emp_id');?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Enter Coordinater Name</label>
								<input type="text" placeholder="Enter Coordinater Name Here.." name="name" class="form-control" value="<?php echo $this->input->post('name'); ?>" />
								<span class="text-danger"><?php echo form_error('name');?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Enter Email ID</label>
								<input type="email" placeholder="Enter Email ID Here.." name="email" class="form-control" value="<?php echo $this->input->post('email'); ?>" />
								<span class="text-danger"><?php echo form_error('email');?></span>
							</div>
						</div>
					<button type="submit" class="btn btn-lg btn-info">Submit</button>					
					</div>
				</form> 
	</div>
	</div>
	</div>
	</div>