<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>

<div class="content-wrapper">
  <div class="container">
      <h2 class="box-title">Edit Coordinater</h2>
      <div class="col-lg-12 well">
        <div class="row">
               <form enctype="multipart/form-data" method="POST" id="news" class="news" action='<?php echo base_url('admin/coordinater/edit/').$coordinater['id'];?>');>
                <div class="box-body">
                <div class="col-sm-12 form-group">
                  <label>Enter Emp Id</label>
                  <input type="text" placeholder="Enter First Name Here.." name="emp_id" class="form-control" value="<?php echo ($this->input->post('emp_id') ? $this->input->post('emp_id') : $coordinater['emp_id']); ?>" />
                  <span class="text-danger"><?php echo form_error('emp_id');?></span>
                </div>
                <div class="col-sm-12 form-group">
                  <label>Enter Name</label>
                  <input type="text" placeholder="Enter Name Here.." name="name" class="form-control" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $coordinater['name']); ?>" />
                  <span class="text-danger"><?php echo form_error('name');?></span>
                </div>
                <div class="col-sm-12 form-group">
                  <label>Enter Email ID</label>
                  <input type="email" placeholder="Enter Email Here.." name="email" class="form-control" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $coordinater['email']); ?>" />
                  <span class="text-danger"><?php echo form_error('email');?></span>
                </div>
                  <div class="box-footer">
                    <button type="submit" id="sub" class="btn btn-success">Submit</button>
                  </div>
                  </div>
                </form>
              </div><!-- /.box -->
        </div>
    </div>
</div>
