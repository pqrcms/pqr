<!DOCTYPE html>
<html>
<head>
	<title></title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets_admin/css/style.css">
  <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container">
      <h1 class="well">Upload Files</h1>
    <div class="col-lg-12 well">
    <div class="box-body">
		<form class="form-horizontal well" action="<?php echo base_url(); ?>admin/add_material/do_upload" method="post" name="upload_excel" enctype="multipart/form-data">
		Select File<input multiple="multiple" name="userfile" type="file" />Allowed files: gif, png, png, pdf
		</br>
		<button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload</button>
		</form>
	</div>
                <!-- /.box -->
</div>
</body>
</html>