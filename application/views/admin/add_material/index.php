Content Wrapper. Contains page content -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Materials
    </h1>
  </section>
  <br>
  
  <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                                     <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">

            <thead style="font-weight: bold">
              <tr>
                <td>Sr No</td>
                <td>File Name</td>
                <td>Action</td>
              </tr>
          </thead>
<div class="box-footer">
<a class="btn btn-success" href="<?php echo base_url('admin/add_material/add_files'); ?>">Add Materials</a>
</div>    
		    <tbody>
         <?php $i=0; ?>
          <?php foreach($materials as $r){ ?> 
            <tr>
                <td><?php echo ++$i; ?></td> 
                <td> <a href="<?php echo base_url().$r->name; ?>" target="_blank"  >view material</a></td>
                  <td>
                    <a href="<?php echo base_url('admin/add_material/remove/'.$r->id); ?>">Delete</a>
                  </td>
             </tr>
          <?php } ?>
        </tbody>

        <tfoot>
            <tr style="font-weight: bold">  
                <td>Sr No</td>
                <td>File Name</td>
                <td>Action</td>

            </tr>
      
        </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper