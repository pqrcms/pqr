<?php include 'header.php' ?>
<section class="common_banner_section no_mar_pad">
	<figure class="pos-relation">
		<img src="<?php echo base_url(); ?>assets/images/banner/guideline-banner.jpg" alt="">
		<figcaption>
			<h1>Guidelines</h1>
		</figcaption>
	</figure>
</section>
<section id="guideline" class="common_padding no_mar_pad white_bg_clor">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php foreach ($guidelines as $g) { ?>
					
				<article>
					<div class="row">
						<div class="col-md-7 col-sm-7">
							<h1 class="title SourceBold"><?php echo $g->guidelines_title;?></h1>
							<!-- <h3 class="title">Helping readers find your article</h3> -->
							<div class="custom_para">
								<?php echo $g->guidelines;?>								<!-- <p>
									One simple thing you can do to improve your article’s visibility and ensure proper indexing and cross-linking is to provide full names for all authors. Please refer to our guidelines for author names, prepared in consultation with Google Scholar, for more information.
								</p> -->
							</div>
						</div>
						<div class="col-md-5 col-sm-5">
							<figure>
								<img src="<?php echo base_url($g->guidelines_image); ?>" class="img-responsive" alt="">
							</figure>
						</div>
					</div>
				</article>
				<?php } ?>
				<!-- <article>
					<div class="row">
						<div class="col-md-12">
							<h1 class="title SourceBold">Submitting your article</h1>
							<div class="custom_para">
								<p>
									Each SAGE journal has its own editorial office and its own instructions for authors. To submit your article, visit your chosen journal’s homepage and click on the manuscript submission guidelines link. View the list of all our journals here.
								</p>
								<p>
									Our general guidance for authors can be found below. Please be sure to read your chosen journal’s guidelines as each journal will have its own specific requirements. Please direct queries on the submission process to the journal’s editorial office;
								</p>
								<p>
									Details can be found within each journal’s submission guidelines. Other queries may be sent to authorqueries@sagepub.co.uk
								</p>
								<p>
									SAGE is a member of the Committee on Publication Ethics (COPE) and follows their best practice guidelines.
								</p>
								<p>
									For authors submitting to medical journals, SAGE recommends that authors follow the Uniform Requirements for Manuscripts Submitted to Biomedical Journals formulated by the International Committee of Medical Journal Editors (ICMJE).
								</p>
							</div>
						</div>
					</div>
				</article> -->
				
			</div>
		</div>
	</div>
</section>
<?php include 'footer.php'; ?>