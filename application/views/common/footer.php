<footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="left">
                            <p>Copyright &copy; <span id="current-year"></span> Emirates CMS Power Company. All Rights Reserved.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right">
                            <ul class="inline">
                                <li><a href="">About us</a></li>
                                <li><a href="">Help Centre</a></li>
                                <li><a href="">Contact Us</a></li>
                                <li><a href="">FAQ’s</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>