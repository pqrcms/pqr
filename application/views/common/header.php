<!DOCTYPE html>
<html>
    <head lang="en">
        <title>Emirates CMS Power Company</title>
        <script>var is_mobile = /symbian|tizen|midp|uc(web|browser)|MSIE (5.0|6.0|7.0|8.0)|tablet/i.test(navigator.userAgent);  if(is_mobile && window.location.hostname != "www1.cricbuzz.com") window.location.hostname = "m.cricbuzz.com";</script>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/resources/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/animate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_user/css/style.css">
        <link rel="stylesheet" type="text.css" href="<?php echo base_url(); ?>assets_user/css/chart-style.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/resources/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/wow.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/sweetalert.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/custom.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/chart.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/semichart.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/raphael-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_user/js/justgage.js"></script>

    </head>
    <body>
        <header id="after-login">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <figure>
                            <a href="javascript:void(0);"><img width="273" src="<?php echo base_url(); ?>assets_user/images/logo.png" alt=""></a>
                        </figure>
                    </div>
                    <div class="col-md-8 right">
                        <div class="menu-click">
                            <ul class="inline">
                                <li>
                                    <a href="javascript:void(0);" id="notification"><img src="<?php echo base_url(); ?>assets_user/images/icons/notification.png" width="30" alt=""><span><?php echo sizeof($notifications);?></span></a>
                                    <div class="megamenu">
                                        <!-- <ul>
                                            <li>
                                                <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                            </li>
                                            <li>
                                                <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                            </li>
                                            <li>
                                                <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                            </li>
                                            <li>
                                                <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                            </li>
                                        </ul> -->
                                        <ul>
                                        <?php if(!empty($notifications)){
                                            foreach($notifications as $n){?>
                                            <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);"><?php echo $n['notification'];?></a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p><?php echo date('d M',strtotime($n['created_at']));?></p>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php } }?>
                                            <!-- <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>13 Aug</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>13 Aug</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>13 Aug</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>13 Aug</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>13 Aug</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>13 Aug</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>13 Aug</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row noti">
                                                    <div class="col-md-9">
                                                        <p><a href="javascript:void(0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</a></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>13 Aug</p>
                                                    </div>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown">
                                        <div class="dropdown-toggle" data-toggle="dropdown">
                                            <div><img src="<?php echo base_url(); ?>assets_user/images/default-user.png" alt=""></div>
                                            <div>
                                                <h5><?php echo $_SESSION['is_userlogged_in']['email'];?></h5>
                                                <p>Emp id : <?php echo $_SESSION['is_userlogged_in']['user_id'];?></p>
                                            </div>
                                        </div>
                                        <ul class="dropdown-menu">
                                            <?php if($_SESSION['is_userlogged_in']['active']!=0){?>
                                            <li>
                                                <a href="<?php echo base_url(); ?>user/user/dashboard">My Dashboard</a>
                                            </li>
                                            <?php }?>
                                            <li><a href="<?php echo base_url(); ?>login/logout">Logout</a></li>
                                            <li><a href="<?php echo base_url(); ?>user/user/my_profile">My Profile</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>