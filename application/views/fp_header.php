<!DOCTYPE html>
<html>
<head>
    <title>GSPER</title> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="763390043517-4b8mec11ivc6lj7a8cuui1ukltbir1tf.apps.googleusercontent.com">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/meanmenu.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hover.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliderstyle.css">    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <!--link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/countdown/css/jquery.countdownTimer.css" /-->
    <style type="text/css">
      
    #customBtn {
      display: inline-block;
      background: white;
      color: #444;
      width: 190px;
      border-radius: 5px;
      border: thin solid #888;
      box-shadow: 1px 1px 1px grey;
      white-space: nowrap;
    }
    #customBtn:hover {
      cursor: pointer;
    }
    span.label {
      font-family: serif;
      font-weight: normal;
    }
    span.icon {
      background: url('/identity/sign-in/g-normal.png') transparent 5px 50% no-repeat;
      display: inline-block;
      vertical-align: middle;
      width: 42px;
      height: 42px;
    }
    span.buttonText {
      display: inline-block;
      vertical-align: middle;
      padding-left: 42px;
      padding-right: 42px;
      font-size: 14px;
      font-weight: bold;
      /* Use the Roboto font that is loaded in the <head> */
      font-family: 'Roboto', sans-serif;
    }

    </style>
</head>
<body>
<section id="account-create">
      
        <div class="modal fade bs-example-modal-lg" id="RegisterModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-keyboard="false" data-backdrop="static">
          <div class="modal-dialog " role="document">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                      <div class="hidden-xs col-sm-4 col-md-4" id="accout-img">
                        <img src="<?php echo base_url(); ?>assets/images/Vector-Smart-Object2.png" alt="">
                      </div>
                      <div class="col-xs-8 col-sm-8 col-md-8">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><img src="<?php echo base_url(); ?>assets/images/close.png" alt=""></button>
                          <h3 class="modal-title">Create Account</h3>
                        </div>
                        <form action="" id="register" name="register">
                          <input type="hidden" disabled class="sent_num" value="">
                          <div class="form-group">
                            <input type="text" id="firstname" name="firstname" class="form-control" placeholder="First name">
                            <span class="error err_firstname"></span>
                          </div>
                          <div class="form-group">
                            <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Last name">
                            <span class="error err_lastname"></span>
                          </div>
                          <div class="form-group">
                            <input type="text" placeholder="Email" name="uemail" id="uemail" class="form-control">
                    <span class="error err_uemail"></span>
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control pwd" id="passwords" name="password" placeholder="Enter Password">
                            <span class="error err_password"></span>
                          </div>
                           <div class="form-group">
                            <input type="password" class="form-control c_password" id="c_password" name="c_password" placeholder="Confirm Password">
                            <span class="error err_c_password"></span>
                          </div>
                          <div class="form-group">
                            <input type="text" name="phone" class="form-control" id="phone"  placeholder="Phone number">
                            <span class="error err_phone"></span>
                            <div class="verify-content" >
                              <a class="verify" href="#">verify</a>
                            </div>
                            <!-- <span class="error err_verify" id="ms_timer"></span> -->
                            <div id="ms_timer"></div>
                          </div>
                          <div class="form-group">
                            <input type="text" name="otp" id="otp" class="form-control otps" placeholder="Enter OTP">
                            <span class="error err_otp"></span>
                          </div>
                          
                          <!-- <div class="form-group">
                            <input type="file" class="form-control file-choose">
                          </div> -->
                          <div>
                              <input id="checkbox-2" class="checkbox-custom terms_check" name="keep_me" type="checkbox">
                              <label for="checkbox-2" class="checkbox-custom-label">I am agree to the GSPER <a href="<?php echo base_url('home/privacy'); ?>" target="_blank" >Terms and Privacy Policy.</a> </label>
                              <span class="error err_terms"></span>
                          </div>
                          
                          <button type="submit" class="btn" id="sub-mit">CREATE ACCOUNT</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</section>
 <a href="javascript:void(0);" data-toggle="modal" data-target="#RegisterModal">Click Here</a>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script type="text/javascript">
  // $(document).ready(function() {
  //   $('#RegisterModal').modal('show'); 
  // });
   $(window).on('load',function(){
    alert("ddd");
        $('#RegisterModal').modal('show');
    });
</script>