<footer>
	<div class="container">
		<div class="footer_top">
			<div class="row">
				<div class="col-xs-12 col-sm-5 col-md-5">
					<div class="left_cont">
						<h3>About us</h3>
						<p>
							GSPER-Global Institute of Scientific Publication in Engineering and Research was founded with an single thought in mind to change the way research work are promoted. We at GSPER believes that every research work can contribute to the society. For doing this we have introduce new schemes and interface which will change the publishing, reviewing and indexing of research work... <br><br><br>
							<a href="<?php echo base_url('user/home/about'); ?>">Read more...</a>
						</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-7">
					<div class="right_cont">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3">
								<div class="activity_list">
									<h3>activities</h3>
									<ul class="list-unstyled">
										<li>
											<a href="<?php echo base_url('conference'); ?>">conferences</a>
										</li>
										<li>
											<a href="<?php echo base_url('user/home/news'); ?>">news</a>
										</li>
										<li>
											<a href="<?php echo base_url('user/home/blog'); ?>">blogs</a>
										</li>
										<li>
											<a href="<?php echo base_url('user/home/guidelines'); ?>">guidelines</a>
										</li>
										<li>
											<a href="<?php echo base_url('user/digital-library'); ?>">digital library</a>
										</li>
										<!-- <li>
											<a href="">publication</a>
										</li> -->
									</ul>
								</div>
							</div>
							<div class="col-xs-12 col-sm-3 col-md-3">
								<div class="other_list">
									<h3>other links</h3>
									<ul class="list-unstyled">
										<li>
											<a href="<?php echo base_url('user/home/career'); ?>">careers</a>
										</li>
										<li>
                      <?php if($this->session->userdata('is_logged_in')) { ?>
											<a href="<?php echo base_url('user/student'); ?>">paper submission</a>
                      <?php } else  { ?>
                      <a herf=""><p data-toggle="modal" data-target="#LoginModal">paper submission</a>
                        <?php } ?>
										</li>
										<li>
											<a href="">sitemap</a>
										</li>
										<li>
											<a href="<?php echo base_url('user/home/contact'); ?>">contact us</a>
										</li>
                    <li>
                      <a href="<?php echo base_url('user/home/policy'); ?>">Privacy &amp; Policy</a>
                    </li>
									</ul>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="contact_details">
									<h3>contact us</h3>
									<p>
										1st Floor, No. 60 and 61, <br> Dr Rajkumar Road, <br> 
                    HOUSE NO 36, 2 <sup>ND</sup> MAIN, 16<sup>TH</sup>CROSS,<br>
                    GOPALAPPA LAYOUT, LAKKASANDRA <br>
										BANGALORE-560030.
									</p>
									<br>
									<!-- <p>
										080-23426161 <br>
										website designing  9741117750 <br>
										website designing  info@indglobal.in
									</p> -->
								</div>
							</div>
						</div>
					</div> <!-- End of right cont -->
				</div>
			</div>
		</div>
		<div class="footer_bottom">
			<div class="row martop1">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<p>Copyrights &copy; GSPER PVT. LTD. 2017 All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- js -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.12.2.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/waypoints.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.meanmenu.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/sweetalert.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slider.jquery.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom_santhosh.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom_view.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom_slider.js"></script>
   <!--script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/countdown/jquery-2.0.3.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/countdown/jquery.countdownTimer.js"></script-->


  	<script type="text/javascript">
    var login=0;
    $(document).ready(function(){

      $('#RegisterModal').on('hidden.bs.modal', function(){
          $(this).find('form')[0].reset();
      })
      $('#LoginModal').on('hidden.bs.modal', function(){
          $(this).find('form')[0].reset();
      })

    	$( "#otp" ).prop( "disabled", true );

    	   $('.verify').click( function (event) {
         
/*document.getElementById('verify').style.pointerEvents = 'none';
setTimeout(function(){
  document.getElementById('verify').style.pointerEvents = 'auto';
}, 300000);*/
                   var phone = $('#phone').val();
                   $('.error').html('');
                 $('.error').css('color','red');

                       if(phone == '')
                  {
                          $("#phone").focus();
                          $(".err_phone").html("Enter phone number");
                          return false;
                  }
                    
                  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                  if(!(phoneno.test(phone)))
                  {
                          $(".phone").focus();
                          $(".err_phone").html("Enter 10 digit phone number");
                          return false;
                  }
                              
                  event.preventDefault();
              
                 $.ajax({
                       type:"post",
                       data:$("#phone").serialize(),
                       url:"<?php echo base_url('user/student/check_phone')?>",
                     dataType: 'json',
                       success:function(res){
                           if(res == 1){
                               $(".err_phone").html("Phone number Already Registered");
                           }
                           else{
//                         $(function(){
//   $("#ms_timer").countdowntimer({
//     minutes : 05‚
//                 seconds : 00‚
//                 size : "lg"
//   });
// });
                       // var data = $.parseJSON(res);
                               $(".verify").prop('disabled',true);

                       console.log(res.otp_data);
                        $('.sent_num').val(res.otp_data.otpp);
                        $( "#otp" ).prop( "disabled", false ); 
                        var str = `OTP expired in <span id="time">01:00</span> minutes!`;
                        var strss = "Resend OTP";
                       $("#ms_timer").html(str);
                     function startTimer(duration, display) {
                        var timer = duration, minutes, seconds;
                        setInterval(function () {
                            minutes = parseInt(timer / 60, 10);
                            seconds = parseInt(timer % 60, 10);

                            minutes = minutes < 10 ? "0" + minutes : minutes;
                            seconds = seconds < 10 ? "0" + seconds : seconds;

                            display.text(minutes + ":" + seconds);

                            if (--timer < 0) {
                                //timer = duration;
                            }
                        }, 1000);
                    }

                  jQuery(function ($) {
                      var fiveMinutes = 60 * 1,
                          display = $('#time');
                      startTimer(fiveMinutes, display);
                  });


                  setTimeout(function(){
                    // $('#ms_timer').remove();
                    $(".verify").prop('disabled',false);
                    $('#ms_timer').html('');
                    $(".verify").html(strss);
                    //$("#otp").prop('disabled',true);
                  }, 60000);



                 $("#dcacl").attr('disabled','disabled');
        }
      }
     })

        }); 




       $('form[name="register"]').on('submit',function(e){ 
              e.preventDefault();    
              var fname = $('#firstname').val();    
              var lname = $('#lastname').val();  
              var email = $('#uemail').val();  
              var phone = $('#phone').val();                
              //var otp = $('.otps')    
              var password = $('.pwd').val();       
              var cpassword = $('#c_password').val();  
              var verify = $('.verify').val(); 
              var otp = $('#otp').val(); 
              var otp_code = $('.sent_num').val(); 
              //console.log(otp_code);    
              

              $('.error').html('');
              $('.error').css('color','red');

              if(fname == '')
                {
                  $("#firstname").focus();
                  $(".err_firstname").html("Enter First Name");
                  return false;
                }


                var nameReg = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
                if(!(nameReg.test($("#firstname").val())))
                {
                  $("#firstname").focus();
                  $(".err_firstname").html("Enter Only Alphabets");
                  return false;
                }

                 if(lname == '')
                {
                  $("#lastname").focus();
                  $(".err_lastname").html("Enter Last Name");
                  return false;
                }
            
                var lnameReg = /^[a-zA-Z\s]+$/;
                if(!(lnameReg.test($("#lastname").val())))
                {
                  $("#lastname").focus();
                  $(".err_lastname").html("Enter Only Alphabets");
                  return false;
                }

                if(email == '')
                {
                  $("#uemail").focus();
                  $(".err_uemail").html("Enter email");
                  return false;
                }

                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  		          if(!(emailReg.test($("#uemail").val())))
  		          {
  		          	$("#uemail").focus();
  		            $(".err_uemail").html("Enter valid email");
  		            return false;
  		          }

		         //var pass = /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])[a-zA-Z0-9@#$%^&+=]*$/;
                if(password == '')
                {
                  $(".pwd").focus();
                  $(".err_password").html("Enter Password");
                  return false;
                }
              
                // if(!(pass.test(password)))
                // {
                //   $(".upassword").focus();
                //   $(".err_upass").html("Enter atleast 8 characters, at least 1 number,at least 1 lowercase character (a-z),at least 1 uppercase character (A-Z)");
                //   return false;
                // }

                if(cpassword == ''){
                   $("#c_password").focus();
                   $(".err_c_password").html("Enter Confirm Password");
                   return false;
                }         
            
                if(cpassword != password){
                   $("#c_password").focus();
                   $(".err_c_password").html("Password does not match");
                   return false;
                }


                if(phone == '')
                {
                  $("#phone").focus();
                  $(".err_phone").html("Enter phone number");
                  return false;
                }
            
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                if(!(phoneno.test(phone)))
                {
                  $(".phone").focus();
                  $(".err_phone").html("Enter 10 digit phone number");
                  return false;
                }

                if(otp == '')
                {
                  $("#otp").focus();
                  $(".err_otp").html("Enter OTP");
                  return false;
                }

                if(otp != otp_code)
                {
                  $("#otp").focus();
                  $(".err_otp").html("Invalid OTP");
                  return false;
                }
                if (!$(".terms_check").is(":checked")) {
                    $(".err_terms").html("Accept Terms and Conditions");
                    return false;
                }
                if(!$('input[name=keep_me]:checked').val())
                {
                  $('input[name=keep_me]').focus();
                  $(".err_terms").html("Select Terms and Privacy policy");
                  return false;
                }
               
                  $.ajax({            
                    type:"POST",
                    data:$("form[name='register']").serialize(),
                    url:"<?php echo base_url();?>user/student/save_user/"+login ,
                    success:function(res){
                      if(res == 2){
                        $(".err_otp").html("Otp does not match");
                      }else if (res == 3){
                        $(".err_uemail").html("This email id already registered");
                      } else if (res == 5){
                        swal("Oops!", "Something went wrong", "error");
                      } else if (res == 8){
                        $("#phone").focus();
                         $(".err_phone").html("This mobile already registered");
                      }
                      else if(res == 4) {     
                         swal({
                                title: "Success!",
                                text: "Activation link has been sent to your email",
                                type: "success",
                                confirmButtonColor: '#aedef4',
                                confirmButtonText: 'OK',
                    },
                    function(isConfirm) {
                      if(isConfirm) {
                        //$('#LoginModal').modal('hide');
                        $('#RegisterModal').modal('hide');
                        $( "#login" ).trigger( "click" );
                      }
                    });

                        
                             
                      }
                      else if(res == 7) {     
                         swal({
                                title: "Success!",
                                text: "Successfully Registered",
                                type: "success",
                                confirmButtonColor: '#aedef4',
                                confirmButtonText: 'OK',
                    },
                    function(isConfirm) {
                      if(isConfirm) {
                        //$('#LoginModal').modal('hide');
                         window.location.href="<?php echo base_url(); ?>"
                      }
                    });

                        
                             
                      }
                    }
                  })
            });

        $('form[name="login_form"]').on('submit',function(e){  
          e.preventDefault();  
          var email = $('#email').val();          
              var password = $('#password').val();
              var change_url = $('#change_url').val();

              $('.error').html('');
              $('.error').css('color','red'); 

              if(!$('input[name=login_as]:checked').val())
                {
                  $("input[name=login_as]").focus();
                  $(".err_login_as").html("Select User Type");
                  return false;
                }

              if(email == '')
              {
              	$("#email").focus();
                  $(".err_email").html("Enter email");
                  return false;
              }
            
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(!(emailReg.test($("#email").val())))
                {
                	$("#email").focus();
                  $(".err_email").html("Enter valid email");
                  return false;
                }  

              if(password == '')
              {
              	$("#password").focus();
                  $(".err_password").html("Enter Password");
                  return false;
              }
              // if(!$('input[name=keep_me]:checked').val())
              //   {
              //     $('input[name=keep_me]').focus();
              //     $(".err_keep_me").html("Select Terms and Privacy policy");
              //     return false;
              //   }

              $.ajax({            
                    type:"POST",
                    data:$("form[name='login_form']").serialize(),
                    url:"<?php echo site_url('user/student/check_user') ?>" ,
                    success:function(res){

                      if(res == 2){
                        swal("Oops!", "Invalid Email or Password!", "error");
                        // $(".err_submit").html("Incorrect email and password");
                      } else if (res == 1){
                        swal({
                                title: "Success!",
                                text: "Successfully Logged-In",
                                type: "success",
                                confirmButtonColor: '#aedef4',
                                confirmButtonText: 'OK',
                    },
                    function(isConfirm) {
                      if(isConfirm) {
                        //$('#LoginModal').modal('hide');
                         window.location.href="<?php echo base_url('user/student'); ?>"
                      }
                    });
                       
                    
                   
                      } 
                      else if (res == 22){
                        swal({
                                title: "Success!",
                                text: "Successfully Logged-In",
                                type: "success",
                                confirmButtonColor: '#aedef4',
                                confirmButtonText: 'OK',
                    },
                    function(isConfirm) {
                      if(isConfirm) {
                        //$('#LoginModal').modal('hide');
                         window.location.href="<?php echo base_url('user/student/status'); ?>"
                      }
                    });
                       
                    
                   
                      } 
                      else if(res == 4){
                        swal("Oops!", "Invalid Email or Password!", "error");
                        //$(".err_submit").html("Incorrect email and password");
                      }
                      else if(res == 3){
                        swal({
            title: "Success!",
            text: "Successfully Logged-In",
            type: "success",
            confirmButtonColor: '#aedef4',
            confirmButtonText: 'OK',
        },
        function(isConfirm) {
            if(isConfirm) {
                window.location.href="<?php echo base_url('reviewer_panel'); ?>"
            }
        }
    );
                        
                      }
                      else if(res == 5){
                        window.location.href="<?php echo base_url('reviewer_reset_password'); ?>"
                      }
                      else if(res == 6){
                        window.location.href="<?php echo base_url('select_available_days'); ?>"
                      }
                      else if(res == 7){
                        window.location.href="<?php echo base_url('select_keywords'); ?>"
                      }
                      else if(res == 8){
                        swal("", "Your Account is not yet Activated , Please Contact Admin", "warning");
                      }
                    }
                  })
        });
    });

 $('form[name="comment"]').on('submit',function(e){  
          //alert(678);
          e.preventDefault();  
          var name = $('#name').val();          
              var message = $('#message').val();

              $('.error').html('');
              $('.error').css('color','red'); 

              if(name == '')
              {
                $("#name").focus();
                  $(".err_name").html("Enter Name");
                  return false;
              }
            
              if(message == '')
              {
                $("#message").focus();
                  $(".err_message").html("Enter Comment");
                  return false;
              }

              $.ajax({            
                    type:"POST",
                    data:$("form[name='comment']").serialize(),
                    url:"<?php echo site_url('user/home/add_comment') ?>" ,
                    success:function(res){

                      if(res == 2){
                         $(".err_submit").html("Something went wrong");
                      } else if (res == 1){
                       location.reload();                   
                      } 
                     
                    }
                  })
        });

$('form[name="job_application"]').on('submit',function(e){  
          //alert(678);
          e.preventDefault();  
           data = new FormData($(this)[0]);
          
              $.ajax({            
                    type:"POST",
                    data: data, 
                     async: false,  
                     cache: false,
        contentType: false,
        processData: false,
                    url:"<?php echo site_url('user/home/job_application') ?>" ,
                    success:function(res){

                      if(res == 2){
                        // alert('Wrong format');
                         swal("", "Wrong format", "warning");
                         location.reload();
                      } else if (res == 1){
                       location.reload();                   
                      } 
                     
                    }
                  })
        });
  </script>
  <script type="text/javascript">
   $('form[name="change_pwd"]').on('submit',function(e){ 
              e.preventDefault();    
              var old = $('#old_pwd').val();    
              var new_pwd = $('#new_pwd').val();  
              var confirm = $('#c_pwd').val();  
             
              

              $('.error').html('');
              $('.error').css('color','red');

              if(old == '')
                {
                  $("#old_pwd").focus();
                  $(".err_old_pwd").html("Enter Old Password");
                  return false;
                }
            
                
            
            
                if(new_pwd == '')
                {
                  $("#new_pwd").focus();
                  $(".err_new_pwd").html("Enter Password");
                  return false;
                }
              
                

                if(confirm == ''){
                   $("#c_pwd").focus();
                   $(".err_c_pwd").html("Enter Confirm Password");
                   return false;
                }         
            
                if(confirm != new_pwd){
                   $("#c_pwd").focus();
                   $(".err_c_pwd").html("Password does not match");
                   return false;
                }


            

                  $.ajax({            
                    type:"POST",
                    data:$("form[name='change_pwd']").serialize(),
                    url:"<?php echo base_url('user/student/change_password') ?>" ,
                    success:function(res){
                      if(res == 1){
                        $(".err_old_pwd").html("Old Password Incorrect");
                      } 
                      else {     
                        //alert("Successfully Changed Password");
                        swal({
            title: "Success!",
            text: "Successfully Changed Password",
            type: "success",
            confirmButtonColor: '#aedef4',
            confirmButtonText: 'OK',
        },
        function(isConfirm) {
            if(isConfirm) {
                window.location.href="<?php echo base_url('user/student'); ?>"
            }
        }
    );
                        
                             
                      }
                    }
                  })
            });
  </script>
  <script type="text/javascript">
  $('form[name="search_details"]').on('submit',function(e){ 
  
              e.preventDefault();    
              var search = $('#title').val();    
              
              

              $('.error').html('');
              $('.error').css('color','red');

              if(search == '')
                {
                  $("#title").focus();
                  $(".err_title").html("Enter title or DOI or keywords...");
                  return false;
                }           

                  $.ajax({            
                    type:"POST",
                    data:$("form[name='search_details']").serialize(),
                    url:"<?php echo base_url('user/digital_library/search_digital') ?>" ,
                    success:function(res){
                      if(res == 1){
                        $(".err_title").html("No records found");
                      } 
                      else if(res==3)
                      {
                        $(".err_title").html("Enter title or DOI or keywords...");
                      }
                      else {     
                        
                        window.location.href="<?php echo base_url('user/digital_library/digital_details'); ?>"
                             
                      }
                    }
                  })
            });

$('form[name="search_digital_details"]').on('submit',function(e){ 
  
              e.preventDefault();    
              var search = $('#title').val();    
              
              $('.error').html('');
              $('.error').css('color','red');

              if(search == '')
                {
                  $("#title").focus();
                  $(".err_title").html("Enter title or DOI or keywords...");
                  return false;
                }
            
                  $.ajax({            
                    type:"POST",
                    data:$("form[name='search_digital_details']").serialize(),
                    url:"<?php echo base_url('user/digital_library/search_digital') ?>" ,
                    success:function(res){
                      if(res == 1){
                        $(".err_title").html("No records found");
                      } 
                      else if(res==3)
                      {
                        $(".err_title").html("Enter title or DOI or keywords...");
                      }
                      else {     
                        
                        window.location.href="<?php echo base_url('user/digital_library/digital_details'); ?>"
                             
                      }
                    }
                  })
            });

  </script>

   <script type="text/javascript">
  //alert(123);
  $('#select_year').on('change',function(){
    //alert(12);
        var year = $(this).val();
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/digital_library/get_volume') ?>", 
            data:{year:year},
            dataType: 'json',
            success:function(res){
              console.log(res);
              var str=`<option value="">Select Volume</option>`;
                $.each(res.value1, function(key,val)
                  {
                    
                    str +=  `<option value="${val.volume}">${val.volume}</option>`;
                  });
                $('#select_volume').html(str);

            }
        });
    });

  $('#select_volume').on('change',function(){
    //alert(12);
        var volume = $(this).val();
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/digital_library/get_issue') ?>",
            data:{volume:volume},
            dataType: 'json',
            success:function(res){
              console.log(res);
              var str=`<option value="">Select Issue</option>`;
                $.each(res.value1, function(key,val)
                  {
                    
                    str +=  `<option value="${val.issue}">${val.issue}</option>`;
                  });
                $('#select_issue').html(str);
            }
        });
    });

  $('#select_issue').on('change',function(){
    //alert(12);
        var issue = $(this).val();
        var year = $('#select_year').val();
        var volume = $('#select_volume').val();
        var j_id = $('#j_id').val();
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/digital_library/get_digital_by_yvi') ?>",
            data:{volume:volume,issue:issue,year:year,j_id:j_id},
            dataType: 'json',
            success:function(res){
              if(res == 1){
                //alert('No records found');
                swal("Oops!", "No records found", "error");
              }
              else if(res == 2){
                window.location.href="<?php echo base_url('digital/get-digital-detail'); ?>" 
              }

            }
        });
    });


  $('.doc').on('click',function(){
    //alert(12);
       // var id = $('#imgg').val();
        var id =  $(this).attr('attr-id');
        //alert(ids);
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/digital_library/get_abstract_by_id') ?>",
            data:{id:id},
            dataType: 'json',
            success:function(res){
              if(res == 2){
                 $('#docModal').modal('hide');
                
              }
              else{
                //alert(res.vi);
                console.log(res.details);
                $(".doc_views_"+id).html(res.vi);
                $(".journal_name").html(res.details.title);
                $(".published_in").html(res.details.journal_name);
                $(".author").html(res.details.author);
                $(".doii").html(res.details.doi);
                $(".abs").html(res.details.abstract);
               $("#docModal").modal('show');
              }

            }
        });
    });
  // $('.t_img').on('click',function(){
   
  //       //var id = $('#t_img').val();
  //     var id =  $(this).attr('attr-id');
  //       //alert(id);
  //       $.ajax({
  //           type:"POST",
  //           url: "<?php echo base_url('generate-table-content') ?>",
  //           data:{id:id},
  //           dataType: 'json',
            
  //       });
  //   });

$('.paper_submit').on('click',function(){
    //alert(12);
       // var id = $('#imgg').val();
        var id =  $(this).attr('attr-id');
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/conference/check_session') ?>",
            data:{id:id},
            dataType: 'json',
            success:function(res){
              if(res == 1){
                 $('#LoginModal').modal('show');
                
              }
              else{
               window.location.href="<?php echo base_url('user/student'); ?>"
              }

            }
        });
    });

$('.paper_submits').on('click',function(){
    //alert(12);
       // var id = $('#imgg').val();
        var id =  $(this).attr('attr-id');
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/conference/check_session') ?>",
            data:{id:id},
            dataType: 'json',
            success:function(res){
              if(res == 1){
                 $('#LoginModal').modal('show');
                
              }
              else{
               window.location.href="<?php echo base_url('user/student'); ?>"
              }

            }
        });
    });


$('#loadmore_journal').click(function(){
    //alert(12);
        var loaded = $(this).attr('loaded');
        //var j_id = $(this).attr('attr-id');
        var to = +5 + +loaded;
        //alert(to);

        $.ajax({
        url:"<?php echo base_url('user/journal/get_search_show_more')?>" ,
        type:'get',
        data:{from:loaded},
        dataType:"json",
        success: function (res) {
          if(res.count.count <= to){
            document.getElementById("loadmore_journal").disabled = true;
          }
          //console.log(res.journal_show);
          var str="";
           // var categories = $.parseJSON(res);
           // categories.each(function() {
            $.each(res.journal_show, function(key,val)
                  { 
                    //alert(val.journal_id);
                    //var j_code =  ${val.journal_code}.'_'.${val.id};  

              str +=`
            
          <div class="jour_details">
            <a href="<?php echo base_url('user/journal/journal_details/') ?>${val.journal_code}_${val.id}">
              <div class="jour_details_cont martop2">
                <div class="row">
                  <div class="col-xs-12 col-sm-5 col-md-4">
                    <div class="jour_details_img">
                      <img src="<?php echo base_url();?>${val.journal_image}" class="img-responsive" alt="">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-7 col-md-8">
                    <div class="jour_details_txt">
                      <h1>${val.title}</h1>
                      <hr class="border_bottom marbot">
                      <div class="txt_inner">
                        <span>Journal Code</span>
                        <p>${val.journal_code}</p>
                        <span>Current Volume</span>
                        <p>${val.current_volume}</p>
                        <!-- <span>Issue</span>
                        <p><?php //echo $j->issue;?></p> -->
                        <span>Editor-in-Chief:</span>
                        <p>${val.editor_chief}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div> <!-- End of jour_details_cont -->
            </a>
            </div>`;
                //$('#categories').append('<ul>'+this+'</ul>');
            });
            $('.results').append(str);
            $('#loadmore_journal').attr('loaded',+loaded + +5);
            
        }
    });
});

  



  $('#loadmore').click(function(){
    //alert(12);
        var loaded = $(this).attr('num_loaded');
        var j_id = $(this).attr('attr-id');
        var to = +5 + +loaded;
        //alert(to);

        $.ajax({
        url:"<?php echo base_url('user/digital_library/get_search_show_more')?>" ,
        type:'get',
        data:{from:loaded,j_id:j_id},
        dataType:"json",
        success: function (res) {
          console.log(res.search);
          var str="";
           // var categories = $.parseJSON(res);
           // categories.each(function() {
            $.each(res.search, function(key,val)
                  { 
                    //alert(val.journal_id);

              str +=`<div class="search_result">
                  <div class="search_result_main">
                   <div class="search_result_cont">
                      <h2>${val.title}</h2>
                       <input type="hidden" value=${val.id} name="imgg" >
                        <img src="<?php echo base_url(); ?>assets/images/doc_icon.png" id="doc" alt="" >
                      </a>
                      <!-- <img src="<?php echo base_url(); ?>assets/images/pdf_icon.png" alt="" class="pdf_option"> -->
                      <a href="<?php echo base_url('user/digital_library/download_digital_pdf/');?>">
                        <img src="<?php echo base_url(); ?>assets/images/pdf_icon.png" alt="">
                      </a>
                      <div class="published_details_table">
                        <table>
                          <tr>
                            <td>Pages</td>
                            <td>:</td>
                            <td>${val.page_no}</td>
                          </tr>
                          <tr>
                            <td>Published in</td>
                            <td>:</td>
                            <td>${val.journal_title}</td>
                          </tr>
                          <tr>
                            <td>Author</td>
                            <td>:</td>
                            <td>${val.author}</td>
                          </tr>
                          <tr>
                            <td>DOI number</td>
                            <td>:</td>
                            <td>${val.doi}</td>
                          </tr>
                        </table>
                      </div>
                      
                    </div>

                    <div class="search_result_download">
                      <div class="res_down_cont">
                        <p>${val.download}</p>
                        <span>Downloads</span>
                      </div>
                      <div class="res_abs_cont">
                        <p>${val.views}</p>
                        <span>Abstract Views</span>
                      </div>
                    </div>
                  </div>
                  
                </div>`;
                //$('#categories').append('<ul>'+this+'</ul>');
            });
            $('.results').append(str);
            $('#loadmore').attr('num_loaded',+loaded + +5);
        }
    });
});

  </script>

  <script type="text/javascript">
   $('form[name="upload_abstract_form"]').on('submit',function(e){ 

              e.preventDefault();  
               var form = $('#fileUploadForm')[0];

    // Create an FormData object
        data = new FormData($(this)[0]);

              var title = $('#title').val();  
              var author1 = $('#1_author').val();  
              var author2 = $('#author2').val();
              var author3 = $('#author3').val();
              var topics = $('#topics').val();
              var keywords = $('#keywords').val();
              var ug = $('#ug').val();
              var pg = $('#pg').val();
              var phd = $('#phd').val();
              var ip = $('#ip').val();
              var nationality = $('#nationality').val();
              var mode_of_publication = $('#mode_of_publication').val();
              var refer_by = $('#refer_by').val();
              var j_id = $('#j_id').val();
              var college_name = $('#college_name').val();
              var place = $('#place').val();
              var abstract = $('#abstract').val();
              
             
              

              $('.error').html('');
              $('.error').css('color','red');

              if(title == '')
                {
                  $("#title").focus();
                  $(".err_title").html("Enter Title");
                  return false;
                }
            
                if(author1 == '')
                {
                  $("#1_author").focus();
                  $(".err_author1").html("Enter First Author");
                  return false;
                }

                if(author2 == '')
                {
                  $("#author2").focus();
                  $(".err_author2").html("Enter Second Author");
                  return false;
                }

                if(author3 == '')
                {
                  $("#author3").focus();
                  $(".err_author3").html("Enter Third Author");
                  return false;
                }

                if(topics == '')
                {
                  $("#topics").focus();
                  $(".err_topics").html("Enter Topics");
                  return false;
                }

                if(keywords == '')
                {
                  $("#keywords").focus();
                  $(".err_keywords").html("Enter Keywords");
                  return false;
                }

                if(!$('input[name=qualification]:checked').val())
                {
                  $("#ip").focus();
                  $(".err_qualification").html("Select Any Qualification");
                  return false;
                }

                if(!$('input[name=nationality]:checked').val())
                {
                  $("#nationality").focus();
                  $(".err_nationality").html("Select Nationality");
                  return false;
                }

                if(!$('input[name=mode_of_publication]:checked').val())
                {
                  $('input[name=mode_of_publication]').focus();
                  $(".err_mode_of_publication").html("Select Mode of publication");
                  return false;
                }

                if(!$('input[name=refer_by]:checked').val())
                {
                  $("#refer_by").focus();
                  $(".err_refer_by").html("Select Any one");
                  return false;
                }
                if(j_id == '')
                {
                  $("#j_id").focus();
                  $(".err_j_id").html("Select Journal");
                  return false;
                }

                // if($('input[name=j_id]:checkbox:checked').length == 0)
                // {
                //   $("#j_id").focus();
                //   $(".err_j_id").html("Select Any one Journal");
                //   return false;
                // }

                if(college_name == '')
                {
                  $("#college_name").focus();
                  $(".err_college_name").html("Enter College Name");
                  return false;
                }

                if(place == '')
                {
                  $("#place").focus();
                  $(".err_place").html("Enter Place");
                  return false;
                }

                if(abstract == '')
                {
                  $("#abstract").focus();
                  $(".err_abstract").html("Enter Abstract");
                  return false;
                }
                 if(!$('input[name=type_of_paper]:checked').val())
                 {
                     $("#abstract").focus();
                    $(".err_type").html("Select any one");
                    return false;
                 }

           

                  $.ajax({            
                    type:"POST",
                    data: data, 
                     async: false,  
                     cache: false,
        contentType: false,
        processData: false,    
                     
                    //data:$("form[name='upload_abstract_form']").serialize(),
                    url:"<?php echo base_url('user/student/upload_abstract_form') ?>" ,
                    success:function(res){
                      if(res == 2){
                        $(".err_upload").html("Something went wrong");
                      } 
                      else if(res == 1) {     
                        //alert("Successfully Uploaded a paper");
                        swal({
            title: "Success!",
            text: "Successfully Uploaded a paper",
            type: "success",
            confirmButtonColor: '#aedef4',
            confirmButtonText: 'OK',
        },
        function(isConfirm) {
            if(isConfirm) {
                window.location.href="<?php echo base_url('user/student'); ?>"
            }
        }
    );
                        
                             
                      }

                    }
                    
                  })
            });
  
   $('form[name="reviewer_registration"]').on('submit',function(e){ 
              e.preventDefault(); 
              data = new FormData($(this)[0]);   
              var name = $('#name').val();  
              var city = $('#city').val();
              var state = $('#state').val();
              var country = $('#country').val();
              var organisational_mail = $('#organisational_mail').val();
              var personal_mail = $('#personal_mail').val();
              var institutional_website = $('#institutional_website').val();
              var phone_no = $('#phone_no').val();
              var linkedin = $('#linkedin').val();
              var fb_url = $('#fb_url').val();
              var cv = $('#cv').val();
          
              $('.error').html('');
              $('.error').css('color','red');

              if(name == '')
                {
                  $("#name").focus();
                  $(".err_name").html("Enter Name");
                  return false;
                }
                if(!$('input[name=qualification]:checked').val())
                {
                  $("#ip").focus();
                  $(".err_qualification").html("Select Any Qualification");
                  return false;
                }
            
                if(country == '')
                {
                  $("#country").focus();
                  $(".err_country").html("Enter Country");
                  return false;
                }
                if(state == '') 
                {
                  $("#state").focus();
                  $(".err_state").html("Enter State");
                  return false;
                }
                if(city == '')
                {
                  $("#city").focus();
                  $(".err_city").html("Enter City");
                  return false;
                }



                if(organisational_mail == '')
                {
                  $("#organisational_mail").focus();
                  $(".err_organisational_mail").html("Enter Your Organisational mail");
                  return false;
                }
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(!(emailReg.test($("#organisational_mail").val())))
                {
                  $("#organisational_mail").focus();
                  $(".err_organisational_mail").html("Enter valid email");
                  return false;
                }

                if(personal_mail == '')
                {
                  $("#personal_mail").focus();
                  $(".err_personal_mail").html("Enter Personal mail");
                  return false;
                }
                var emailRegs = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(!(emailRegs.test($("#personal_mail").val())))
                {
                  $("#personal_mail").focus();
                  $(".err_personal_mail").html("Enter valid email");
                  return false;
                }
                if(institutional_website == '')
                {
                  $("#institutional_website").focus();
                  $(".err_institutional_website").html("Enter Your Institutional Website");
                  return false;
                }
                if(phone_no == '')
                {
                  $("#phone_no").focus();
                  $(".err_phone_no").html("Enter Your Phone Number");
                  return false;
                }


                // var phonenumber = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                // if(!(phonenumber.test(phonenumber)))
                // {
                //   $("#phone_no").focus();
                //   $(".err_phone_no").html("Enter 10 digit phone number");
                //   return false;
                // }

                if(linkedin == '')
                {
                  $("#linkedin").focus();
                  $(".err_linkedin").html("Enter Linked in Url");
                  return false;
                }
                if(fb_url == '')
                {
                  $("#fb_url").focus();
                  $(".err_fb_url").html("Enter Your Facebook url");
                  return false;
                }

                


            

                  $.ajax({            
                    type:"POST",
                    data: data, 
                    async: false,  
                    cache: false,
                    contentType: false,
                    processData: false,
                    //data:$("form[name='reviewer_registration']").serialize(),
                    url:"<?php echo base_url('user/reviewer/save_reviewer') ?>" ,
                    success:function(res){
                      if(res == 1){
                        $(".err_personal_mail").html("This Email Already Registered");
                      } 
                      else if(res == 2)
                      {
                        $(".err_phone").html("This Phone Number Already Registered");
                      }
                      else if(res == 4)
                      {
                        $(".err_submit").html("Something went wrong");
                      }
                      else if(res == 3) {     
                        //alert("Your Application is due for validation. Kindly contact us through your official mail id at validate@gsper.com to activate your account. For further details contact us at");
                       swal({
            title: "Success!",
            text: "Your Application is due for validation. Kindly contact us through your official mail id at validate@gsper.com to activate your account. For further details contact us at",
            type: "success",
            confirmButtonColor: '#aedef4',
            confirmButtonText: 'OK',
        },
        function(isConfirm) {
            if(isConfirm) {
                window.location.href="<?php echo base_url('reviewer_registration'); ?>"
            }
        }
    );
                        //window.location.href="<?php echo base_url('reviewer_registration'); ?>"
                             
                      }
                    }
                  })
            });


 $('#topics').on('change',function(){
        var topics = $(this).val();
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/student/get_keywords_by_topics') ?>",
            data:{topics:topics},
            dataType: 'json',
            success:function(res){
              console.log(res.value1);
              var strs=`<option value="">Choose Atleast 5 Keywords</option>`;
                $.each(res.value1, function(key,val)
                  {
                   
                    strs +=  `<option value="${val}">${val}</option>`;

                  });
                $('#keywor').html(strs);
            }
        });
    });


  $('form[name="teacher_reset_pwd"]').on('submit',function(e){ 
              e.preventDefault();    
              var old = $('#old_pwd').val();    
              var new_pwd = $('#new_pwd').val();  
              var confirm = $('#c_pwd').val();  
             
              

              $('.error').html('');
              $('.error').css('color','red');

              if(old == '')
                {
                  $("#old_pwd").focus();
                  $(".err_old_pwd").html("Enter Old Password");
                  return false;
                }
            
                
            
            
                if(new_pwd == '')
                {
                  $("#new_pwd").focus();
                  $(".err_new_pwd").html("Enter Password");
                  return false;
                }
              
                

                if(confirm == ''){
                   $("#c_pwd").focus();
                   $(".err_c_pwd").html("Enter Confirm Password");
                   return false;
                }         
            
                if(confirm != new_pwd){
                   $("#c_pwd").focus();
                   $(".err_c_pwd").html("Password does not match");
                   return false;
                }


            

                  $.ajax({            
                    type:"POST",
                    data:$("form[name='teacher_reset_pwd']").serialize(),
                    url:"<?php echo base_url('user/reviewer/save_change_password') ?>" ,
                    success:function(res){
                      if(res == 1){
                        $(".err_old_pwd").html("Old Password Incorrect");
                      } 
                      else {     
                        //alert("Successfully Changed Password");
                        swal({
            title: "Success!",
            text: "Successfully Changed Password",
            type: "success",
            confirmButtonColor: '#aedef4',
            confirmButtonText: 'OK',
        },
        function(isConfirm) {
            if(isConfirm) {
                window.location.href="<?php echo base_url('user/student'); ?>"
            }
        }
    );
                        
                             
                      }
                    }
                  })
            });

 $('form[name="contact_us"]').on('submit',function(e){ 
              e.preventDefault();    
              var fname = $('#fname').val();    
              var lname = $('#lname').val();  
              var mobile = $('#mobile').val();
              var emailid = $('#emailid').val();
              var message = $('#message').val();
             
              

              $('.error').html('');
              $('.error').css('color','red');

              if(fname == '')
                {
                  $("#fname").focus();
                  $(".err_fname").html("Enter First name");
                  return false;
                }
                        
                if(lname == '')
                {
                  $("#lname").focus();
                  $(".err_lname").html("Enter Last name");
                  return false;
                }               

                if(mobile == ''){
                   $("#mobile").focus();
                   $(".err_mobile").html("Enter Mobile");
                   return false;
                }     
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                if(!(phoneno.test(mobile)))
                {
                  $("#mobile").focus();
                  $(".err_mobile").html("Enter 10 digit phone number");
                  return false;
                }  
                if(emailid == '')
                {
                  $("#emailid").focus();
                  $(".err_email").html("Enter mail id");
                  return false;
                }
                var emailRegs = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(!(emailRegs.test($("#emailid").val())))
                {
                  $("#emailid").focus();
                  $(".err_email").html("Enter valid email");
                  return false;
                }  
            
               

            

                  $.ajax({            
                    type:"POST",
                    data:$("form[name='contact_us']").serialize(),
                    url:"<?php echo base_url('user/home/send_contact') ?>" ,
                    success:function(res){
                      if(res == 1){
                        $(".err_submit").html("Something went wrong");
                      } 
                      else {     
                        //alert("Your message sent. Admin will Respond you soon.");
                        swal({
            title: "Success!",
            text: "Your message sent. Admin will Respond you soon.",
            type: "success",
            confirmButtonColor: '#aedef4',
            confirmButtonText: 'OK',
        },
        function(isConfirm) {
            if(isConfirm) {
                location.reload();
            }
        }
    );
                        
                             
                      }
                    }
                  })
            });


$('form[name="subscribe"]').on('submit',function(e){ 
              e.preventDefault();    

              var emailid = $('#s_email').val();

             
              

              $('.error').html('');
              $('.error').css('color','red');

                
               
                if(emailid == '')
                {
                  $("#s_email").focus();
                  $(".err_email").html("Enter mail id");
                  return false;
                }
                var emailRegs = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(!(emailRegs.test($("#s_email").val())))
                {
                  $("#s_email").focus();
                  $(".err_email").html("Enter valid email");
                  return false;
                }  
            
               

            

                  $.ajax({            
                    type:"POST",
                    data:$("form[name='subscribe']").serialize(),
                    url:"<?php echo base_url('user/home/subscribe') ?>" ,
                    success:function(res){
                      if(res == 1){
                        $(".err_submit").html("Something went wrong");
                      } 
                      else {     
                        //alert("Your message sent. Admin will Respond you soon.");
                        swal({
            title: "Success!",
            text: "Subscribe Request sent. Admin will Respond you soon.",
            type: "success",
            confirmButtonColor: '#aedef4',
            confirmButtonText: 'OK',
        },
        function(isConfirm) {
            if(isConfirm) {
                location.reload();
            }
        }
    );
                        
                             
                      }
                    }
                  })
            });
$('#paper_img').click(function(){
  alert(123);
  var id = $(this).data('id');
  alert(id);
    $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/home/check_session') ?>",
            data:{id:id},
            dataType: 'json',
            success:function(res){
              
              console.log(res);
              
              if(res == 1)
              {
                  $('#LoginModal').modal('show'); 
              }
              
              else
              {
                window.location.href="<?php echo base_url('user/student'); ?>";
              }
                
            }
        });
    //});
})


$('#track_paper').click(function(){
        var id = $(this).val();
        $.ajax({
            type:"POST",
            url: "<?php echo base_url('user/student/get_student_status') ?>",
            data:{id:id},
            dataType: 'json',
            success:function(res){
              console.log(res.value1.user_id);
              
              
              if(res.value1.status != 1)
              {
                str = '<p class="success_msg">Full paper successfully submitted</p>';
                 
              }
              else
              {
                str = '<p class="pink_msg">Submit Full paper Here</p>';
                //$('#full_paper').html(str);
              }

              if(res.value1.status == 3 || res.value1.status == 1 || res.value1.status ==2)
              {
                due = '<p class="pink_msg">Paper is due for acceptance</p>';
                 
              }
              else if(res.value1.status == 4)
              {
                due = '<p class="pink_msg">Paper is due for acceptance</p>';
              }
              else if(res.value1.status == 5)
              {
                due = '<p class="success_msg">Paper Request accepted</p>';
              }
              else
              {
                due = '<p class="success_msg">Paper Request accepted</p>';
                //$('#full_paper').html(str);
              }
              if(res.value1.status == 7 || res.value1.status == 8)
              {
                rev = '<p class="success_msg">Review Completed by teacher</p>'
              }
              else
              {
                rev = '<p class="pink_msg">Under Process</p>';
              }

              if(res.value1.status == 8)
              {
                publ = '<p class="success_msg">Published</p>'
              }
              else
              {
                publ = '<p class="pink_msg">Under Process</p>';
              }
              //alert()
                $(".full").html(str);
                $(".accep").html(due);
                $(".proce").html(rev);
                $(".pub").html(publ);

                //alert(strs);
                
            }
        });
    });

//Facebook Integration
 window.fbAsyncInit = function() {
            FB.init({
                appId: '464853037188599', // App ID
                channelUrl: 'http://localhost/gsper_edit/', // Channel File
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true // parse XFBML
            });


            FB.Event.subscribe('auth.authResponseChange', function(response) {
                if (response.status === 'connected') {
                 //   document.getElementById("message").innerHTML += "<br>Connected to Facebook";
                    //SUCCESS

                } else if (response.status === 'not_authorized') {
                  //  document.getElementById("message").innerHTML += "<br>Failed to Connect";

                    //FAILED
                } else {
                  //  document.getElementById("message").innerHTML += "<br>Logged Out";

                    //UNKNOWN ERROR
                }
            });

        };

        function Login() {
            FB.login(function(response) {
                if (response.authResponse) {

                    FB.api('/me', function(response) {
                     // console.log(response.name);
  var name = response.name;

    

         var fb_id=response.id; 
        
          $.ajax({            
                    type:"POST",
                    data:{id:fb_id,name:name},
                    url:"<?php echo base_url('user/student/fb_login') ?>" ,
                    success:function(res){
                      if(res == 1){
                       // alert("Check your Username password");
                        swal("", "Check your Username password", "warning");
                        $location.reload();
                      } 
                      else {     
                        //alert("Successfully Changed Password");
                        window.location.href="<?php echo base_url('user/student/'); ?>"
                             
                      }
                    }
                  })

        // var password=$("#login_password").val();
        


       // document.getElementById("status").innerHTML=str;
              
    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, {
                scope: 'email,user_photos,user_videos'
            });


        }

       

// Load the SDK asynchronously
        (function(d) {
            var js, id = 'facebook-jssdk',
                ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));

</script>
   

<script type="text/javascript" src="https://platform.linkedin.com/in.js">
//onLoad: onLinkedInLoad
api_key: 77oho3shi9jymk
authorize: true
// onLoad: onLinkedInLoad
scope: r_basicprofile r_emailaddress
//authorize:true
</script>

<script type="text/javascript">
 function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);
    }

//Handle the successful return from the API call
  function onSuccess(data) {
    console.log(data);
  }

  // Handle an error response from the API call
  function onError(error) {
    console.log(error);
  }

  
function onLinkedInAuth() {
IN.API.Profile("me")
.fields("firstName", "lastName", "industry", "location:(name)", "picture-url", "headline", "summary", "num-connections", "public-profile-url", "distance", "positions", "educations", "date-of-birth","emailAddress")
.result(displayProfiles);






}


function displayProfiles(profiles) {
member = profiles.values[0];
console.log(member);
// Build the JSON payload containing the content to be shared
var fname = member.firstName;
var lname = member.lastName;
var industry = member.industry;
var location = member.location.name;
var current = "";
var previous = "";
var email = member.emailAddress;
var linked = member.publicProfileUrl;
var pic = member.pictureUrl;
alert(email);
   
$.ajax({            

                    type:"POST",
                    data:{fname:fname,lname:lname,industry:industry,location:location,current:current,previous:previous,email:email,linked:linked,pic:pic},
                    url:"<?php echo base_url('user/reviewer/linkedin_register') ?>" ,
                    success:function(res){
                      if(res == 1){
                        
                        window.location.href="<?php echo base_url('reviewer_registration'); ?>"
                      } 
                      else {     
                        //alert("Check your Username password");
                        swal("", "Check your Username password", "warning");
                        $location.reload();
                             
                      }
                    }
                  })
    

}
jQuery(document).ready(function(){
jQuery("#linkdinlog").click(function(){
  IN.UI.Authorize().place();
IN.Event.on(IN, "auth", onLinkedInAuthCreate);
});
});


  </script>

<!-- <a href="javascript:void(0)" id="linkdinlog">link123</a> -->
 
 
<script type="text/javascript">
  $(document).ready(function(){
    $('#register_linkedin').on('click',function(){
        IN.UI.Authorize().place();
      IN.Event.on(IN, "auth", onLinkedInAuthCreate);
    });


    $('#register_fb').on('click',function(){
      Loginc();
    });
    $('#register_google').on('click',function(){
      // renderButton();
      startApp();
    });


  });

function onLinkedInAuthCreate() {
IN.API.Profile("me")
.fields("id","firstName", "lastName", "industry", "location:(name)", "picture-url", "headline", "summary", "num-connections", "public-profile-url", "distance", "positions", "educations", "date-of-birth","emailAddress")
.result(createAccount_linkedin);
}


function createAccount_linkedin(profiles) {
  member = profiles.values[0];
  console.log(member);
  // Build the JSON payload containing the content to be shared
  var app_id=member.id;
  var fname = member.firstName;
  var lname = member.lastName;
  var email = member.emailAddress;
  var pic = member.pictureUrl;
  $("#firstname").val(fname);
  $("#lastname").val(lname);
  $("#uemail").val(email);
  $("#app_id").val(app_id);
  login=1;
}



//Facebook Integration
 window.fbAsyncInit = function() {
            FB.init({
                appId: '464853037188599', // App ID
                channelUrl: 'http://www.gsper.com', // Channel File
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true // parse XFBML
            });


            FB.Event.subscribe('auth.authResponseChange', function(response) {
                if (response.status === 'connected') {

                } else if (response.status === 'not_authorized') {
                } else {
                }
            });

        };

        function Loginc() {
            FB.login(function(response) {
            if (response.authResponse) {
            FB.api('/me',{ locale: 'en_US', fields: 'name, email' }, function(response) {
            var name = response.name;
            var fb_id=response.id; 
            var email=response.email;
            var array = name.split(" ");
            $("#firstname").val(array[0]);
            $("#lastname").val(array[1]);
            $("#uemail").val(email);
            $("#app_id").val(fb_id);
            login=1;
            // console.log(response);
             });
            } 
            }, 
            {
                scope: 'email,user_photos,user_videos'
            });
        }

       

        // Load the SDK asynchronously
        (function(d) {
            var js, id = 'facebook-jssdk',
                ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));

</script>
    <script src="https://apis.google.com/js/client:platform.js" async defer></script>
<script type="text/javascript">

  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '763390043517-4b8mec11ivc6lj7a8cuui1ukltbir1tf.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('register_google'));
    });
  };

  function attachSignin(element) {
    // console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {

        var profile = googleUser.getBasicProfile();
            $("#firstname").val(profile.getGivenName());
            $("#lastname").val(profile.getFamilyName());
          $("#uemail").val(profile.getEmail());
          $("#app_id").val(profile.getId());
          login=1;

        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
  }
</script>
<!-- =========================== Side Menu ============================ -->
<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
<script type="text/javascript">
  function forgot_password()
  {
    $("#LoginModal").hide();
  }
   $('form[name="fp_form"]').on('submit',function(e){  
          e.preventDefault();  
          var email = $('#forgot_email').val();  

              $('.error').html('');
              $('.error').css('color','red'); 

              if(!$('input[name=fp_as]:checked').val())
                {
                  $("input[name=fp_as]").focus();
                  $(".fp_err_fp_as").html("Select User Type");
                  return false;
                }

              if(email == '')
              {
                $("#forgot_email").focus();
                  $(".fp_err_email").html("Enter email");
                  return false;
              }
            
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(!(emailReg.test($("#forgot_email").val())))
                {
                  $("#forgot_email").focus();
                  $(".fp_err_email").html("Enter valid email");
                  return false;
                }  

             

              $.ajax({            
                    type:"POST",
                    data:$("form[name='fp_form']").serialize(),
                    url:"<?php echo site_url('Home/forgot_password') ?>" ,
                    success:function(res){

                      if(res == 2){
                        swal("Oops!", "Invalid Email !", "error");
                        // $(".err_submit").html("Incorrect email and password");
                      } else if (res == 1){
                        swal({
                                title: "Success!",
                                text: "Reset link has been sent to Your Email id",
                                type: "success",
                                confirmButtonColor: '#aedef4',
                                confirmButtonText: 'OK',
                    },
                      function(isConfirm) {
                        if(isConfirm) {
                          //$('#LoginModal').modal('hide');
                           window.location.href="<?php echo base_url(); ?>"
                        }
                      });
                       
                    
                   
                      } 
                    }
                  })
        });
</script>

<script type="text/javascript">
   $('form[name="change_password"]').on('submit',function(e){  
          e.preventDefault();  
          var password = $('#cpassword').val();  
          var repassword = $('#ccpassword').val();

              $('.error').html('');
              $('.error').css('color','red'); 


              if(password == '')
              {
                $("#cpassword").focus();
                  $(".err_cpassword").html("Enter password");
                  return false;
              }
               if(repassword == '')
              {
                $("#ccpassword").focus();
                  $(".err_ccpassword").html("Enter again password");
                  return false;
              }
               if(repassword != password)
              {
                $("#ccpassword").focus();
                  $(".err_ccpassword").html("Password not matching");
                  return false;
              }

             

              $.ajax({            
                    type:"POST",
                    data:$("form[name='change_password']").serialize(),
                    url:"<?php echo site_url('Home/change_password') ?>" ,
                    success:function(res){

                      if(res == 2){
                        swal("Oops!", "Invalid Email or Password!", "error");
                        // $(".err_submit").html("Incorrect email and password");
                      } else if (res == 1){
                        swal({
                                title: "Success!",
                                text: "Successfully Changed",
                                type: "success",
                                confirmButtonColor: '#aedef4',
                                confirmButtonText: 'OK',
                    },
                    function(isConfirm) {
                      if(isConfirm) {
                        //$('#LoginModal').modal('hide');
                         window.location.href="<?php echo base_url(); ?>"
                      }
                    });
                       
                    
                   
                      } 
                      else if(res == 4){
                        swal("Oops!", "Invalid Email or Password!", "error");
                        //$(".err_submit").html("Incorrect email and password");
                      }
                      else if(res == 3){
                        swal({
            title: "Success!",
            text: "Successfully Changed",
            type: "success",
            confirmButtonColor: '#aedef4',
            confirmButtonText: 'OK',
        },
        function(isConfirm) {
            if(isConfirm) {
                window.location.href="<?php echo base_url('reviewer_panel'); ?>"
            }
        }
    );
                        
                      }
                      else if(res == 5){
                        window.location.href="<?php echo base_url('reviewer_reset_password'); ?>"
                      }
                      else if(res == 6){
                        window.location.href="<?php echo base_url('select_available_days'); ?>"
                      }
                      else if(res == 7){
                        window.location.href="<?php echo base_url('select_keywords'); ?>"
                      }
                      else if(res == 8){
                        swal("", "Your Account is not yet Activated , Please Contact Admin", "warning");
                      } 
                    }
                  })
        }); 
   $(".close").on('click',function(){
    $(".err_otp").html('');
    $("#email").val(''); 
    $(".err_email").html('');
    $("#password").val('');
    $(".err_password").html('');
                    $('#ms_timer').html(''); 
                    $("#otp").prop('disabled',true);
   }); 

$(".j_title").on('change',function(){
    //alert($(this).val());
   // return false;
   $.ajax({            
                    type:"POST",
                    data:{topic:$(this).val()},
                    url:"<?php echo base_url('user/journal/get_journal') ?>" ,
                    dataType:'JSON',
                    success:function(res){
                      // alert("aaa");
                      // console.log(res);
                      var str ="";
                      $('.jour_details').remove();
                                           
 $.each(res.value1, function(key,val)
                  {
                      
   str +=`<div class="jour_details">
            <a href="<?php echo base_url('user/journal/journal_details/')?>${val.id}">
              <div class="jour_details_cont martop2">
                <div class="row">
                  <div class="col-xs-12 col-sm-5 col-md-4">
                    <div class="jour_details_img">
                      <img src="<?php echo base_url(); ?>${val.journal_image}" class="img-responsive" alt="">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-7 col-md-8">
                    <div class="jour_details_txt">
                      <h1>${val.title}</h1>
                      <hr class="border_bottom marbot">
                      <div class="txt_inner">
                        <span>Journal Code</span>
                        <p>${val.journal_code}</p>
                        <span>Current Volume</span>
                        <p>${val.current_volume}</p>
                        <!-- <span>Issue</span>
                        <p><?php //echo $j->issue;?></p> -->
                        <span>Editor-in-Chief:</span>
                        <p>${val.editor_chief}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div> 
            </a>
            </div>`;
                    
             });
            // console.log(text);
            $('.topic_list').append(str);
                       
                    }
                  });
   }); 

</script>
<!-- =========================== Side Menu ============================ -->
</body>
</html>