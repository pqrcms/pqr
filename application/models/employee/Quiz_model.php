<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Quiz_model extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*** Check Login Credentials ***/

    function get_all_roles()
    { 
        $query = $this->db
                          ->get('roles');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    /*
     * Get role by id
     */
    function get_role($id)
    {
        return $this->db->get_where('roles',array('id'=>$id))->row_array();
    }

    function get_All_Types()
    {
        /*
        $query = $this->db->get('types');

        foreach ($query->result() as $row)
        {
            echo $row->type;
        }*/

        $query = $this->db->query('SELECT type FROM types');

        //prient_r($query)die();
        return $query->result();

        //echo 'Total Results: ' . $query->num_rows();
    }

    /*** End Login Check ***/
	
}