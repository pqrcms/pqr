<?php

Class User_model extends CI_Model {

	public function send_mail($from,$to,$subject,$message,$cc='')
    {
		// $headers = "From: no-reply@pqr.com\r\n";
		// $headers .='X-Mailer: PHP/' . phpversion();
		//$headers .= "MIME-Version: 1.0\r\n";
		//$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";   
		// return mail($to,$subject,$message,$headers);
		$headers = 'From: '.$from."\r\n".
		'Reply-To: '.$from."\r\n" .
		'X-Mailer: PHP/' . phpversion();
		return mail($to, $subject, $message, $headers);  
	}
//Send Notification
	function send_notification($emp_id,$params)
	{
		$this->db->insert('notification',$params);
        return $this->db->insert_id();
	}

	function get_all_ot_tasks(){
		$query = $this->db->get('ot_tasks'); 
		return $query->result_array();
	}
    function get_trainee_feedback($user_id){
        $query = $this->db->get_where('trainee_feedback', array('emp_id' => $user_id))->row();
        return $query;
    }

	function get_all_notification($id){
	//$this->db->select('ot_tasks.id');
        $this->db->from('notification');
    //$this->db->join('notification', 'notification.type = ot_tasks.id', 'left');
        $this->db->where('emp_id', $id);
        $query= $this->db->get();
		return $query->result_array();
	}
	/*
     * Get emp_detail by emp_id
     */
    function get_emp_detail($user_id)
    {
    	$this->db->where('emp_id', $user_id);
    	$query = $this->db->get('emp_details'); 
    	return $query->result_array();
    }

	function get_tasks_name($id){
		$this->db->select('tasks');
		$this->db->where('id', $id);
		$q = $this->db->get('ot_tasks')->row();
		// print_r($q);exit(); 
		return $q;
	}

	function validate($email, $password) 
    { 
        $this->db->where('email', $email); 
        $this->db->where('password', $password); 
        $query = $this->db->get('emp_details'); 

        if($query->num_rows() == 1)
        { 
        	$row=$query->row();

            $data=array(
            'email'=>$row->email,
            'emp_id'=>$row->emp_id);

            $this->session->set_userdata($data);
          return $query->row(); 
        }else{
            return false;
       	}    
    }
// Check email id is there in DB or not
    function validate_email($email){
    	$this->db->where('email', $email); 
        $query = $this->db->get('emp_details'); 
        
        if($query->num_rows() == 1)
        { 
          return true; 
        }
    }
	// Read data using email and password
	public function login($data) {

	$condition = "email =" . "'" . $data['email'] . "' AND " . "password =" . "'" . $data['password'] . "'";
	$this->db->select('*');
	$this->db->from('emp_details');
	$this->db->where($condition);
	$this->db->limit(1);
	$query = $this->db->get();

	if ($query->num_rows() == 1) {
	return true;
	} else {
	return false;
	}
	}
	
	// Read data from database to show data in admin page
	public function read_user_information($email) {

	$condition = "email =" . "'" . $email . "'";
	$this->db->select('*');
	$this->db->from('emp_details');
	$this->db->where($condition);
	$this->db->limit(1);
	$query = $this->db->get();

	if ($query->num_rows() == 1) {
	return $query->result();
	} else {
	return false;
	}
	}
	function get_tasks_count(){
		$this->db->select('count(*) as tot' );
		$q = $this->db->get('ot_tasks')->row();
		return $q;
	}
    function get_completed_count($user_id)
    {
    	$this->db->select('task_count');
    	$this->db->where('employee_id', $user_id);
    	$query = $this->db->get('emp_details');
    	// echo $this->db->last_query();exit(); 
    	return $query->row_array();
    }
    function increament_task_count($user_id)
    {
    	$this->db->set('task_count','task_count+1', FALSE);
    	$this->db->where(' employee_id', $user_id);
    	$this->db->update('emp_details');
    	// echo $this->db->last_query();exit();
    	return 1;
    }
    function change_active($user_id)
    {
    	$this->db->set('active','1');
    	$this->db->where(' employee_id', $user_id);
    	$this->db->update('emp_details');
    	// echo $this->db->last_query();exit();
    	return 1;
    }
    function get_module($user_id)
    {
    	$this->db->select('department,position');
    	$this->db->where(' employee_id', $user_id);
    	$query=$this->db->get('emp_details');
    	$result=$query->row_array();
    	$department=$result['department'];
    	$designation=$result['position'];
    	
    	$this->db->distinct('t.id');
    	$this->db->select('e.*,t.*,t.id as tid,t.status as task_status,c.name as assigned_by');
    	$this->db->from('training_model t');
        $this->db->join('emp_tasks e','e.task_id=t.id','left');
        $this->db->join('coordinater c','c.id=t.assign_by','left');
    	$this->db->where('t.department', $department);
    	$this->db->where('t.designation', $designation);
        $this->db->order_by('t.status','ASC');
    	$this->db->group_by('t.id');
    	$query=$this->db->get();
    	// echo $this->db->last_query();exit;
    	$results=$query->result_array();
    	return $results;
    	// print_r($results);exit();
    }
    function get_result($user_id)
    {
    	$this->db->select('e.*,e.status as result_status,e.created_at as time,t.*,c.name as assigned_by');
    	$this->db->from('emp_tasks e');
        $this->db->join('training_model t','e.task_id=t.id','left');
        $this->db->join('coordinater c','c.id=t.assign_by','left');
    	$this->db->where('e.emp_id', $user_id);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$results=$query->result_array();
    	return $results;
    	// print_r($results);exit();
    }
    function get_trainer($user_id)
    {
    	$this->db->distinct('c.id');
    	$this->db->select('c.name as trainer,c.id as tid');
    	$this->db->from('emp_details e');
        $this->db->join('training_model t','e.department=t.department and e.position=t.designation');
        $this->db->join('coordinater c','c.id=t.assign_by');
    	$this->db->where('e.employee_id', $user_id);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$results=$query->result_array();
    	return $results;
    	// print_r($results);exit();
    }
    function get_task($tid)
    {
    	$this->db->select('t.title,t.id');
    	$this->db->from('training_model t');
    	$this->db->where('t.assign_by', $tid);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$results=$query->result_array();
    	return $results;
    	// print_r($results);exit();
    }
	function add_feedback($params)
	{
		$this->db->insert('emp_feedback',$params);
        return $this->db->insert_id();
	}
    //
    function get_tasks_circle($user_id){
        $this->db->select('emp_id,sum(max_marks) as max_marks,sum(mark_obtained) as mark_obtained');
        $this->db->from('emp_tasks');
        $this->db->where('emp_id', $user_id);
        $query=$this->db->get();
        //echo $this->db->last_query();
        $results=$query->row();
        //$query = $this->db->get_where('emp_tasks', array('emp_id' => $user_id))->row();
        return $results;
    }

    function get_task_count($user_id)
    {
    	$this->db->distinct('t.id');
    	$this->db->select('count(t.id) as total_count');
    	$this->db->from('emp_details e');
        $this->db->join('training_model t','e.department=t.department and e.position=t.designation');
    	$this->db->where('e.employee_id', $user_id);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$res=$query->row_array();
    	$results['total_count']=$res['total_count'];

    	$this->db->select('count(e.id) as completed_count');
    	$this->db->from('emp_tasks e');
    	$this->db->where('e.emp_id', $user_id);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$res=$query->row_array();
    	$results['completed_count']=$res['completed_count'];
    	return $results;
    	// print_r($results);exit();
    }
    function get_question($tid,$no)
    {
    	$this->db->select('mq.*');
    	$this->db->from('manage_questions mq');
    	$this->db->where('mq.task_id', $tid);
    	$this->db->limit(1,$no);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$results=$query->row_array();
    	return $results;
    	// print_r($results);exit();
    }
    function get_count_question($tid)
    {
    	$this->db->select('count(mq.id) as total');
    	$this->db->from('manage_questions mq');
    	$this->db->where('mq.task_id', $tid);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$results=$query->row_array();
    	return $results;
    	// print_r($results);exit();
    }
    function save_answer($data,$tid,$qid,$emp_id)
    {
    	$this->db->select('*');
		$this->db->from('emp_answers');
		$this->db->where('task_id',$tid);
		$this->db->where('qid',$qid);
		$this->db->where('emp_id',$emp_id);
		$query = $this->db->get();
		if ($query->num_rows() >= 1) {
			$data['updated_on']=date('Y-m-d H:i');
	    	$this->db->where('emp_id', $emp_id);
	    	$this->db->where('task_id', $tid);
	    	$this->db->where('qid', $qid);
	    	$this->db->update('emp_answers',$data);
	    	return 1;
		} else {
			$data['created_at']=date('Y-m-d H:i');
			$this->db->insert('emp_answers',$data);
        return $this->db->insert_id();
		}
    }
    function submit_task($data)
    {
		$this->db->insert('emp_tasks',$data);
        return $this->db->insert_id();
    }
    function get_score($tid,$emp_id)
    {
		$this->db->select('count(e.id) as count,sum(m.marks) mark');
    	$this->db->from('emp_answers e');
        $this->db->join('manage_questions m','m.task_id=e.task_id and m.id=e.qid and m.answer=e.answer');
    	$this->db->where('e.emp_id', $emp_id);
    	$this->db->where('e.task_id', $tid);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$res=$query->row_array();
    	return $res;
    }
    function get_answered_count($tid,$emp_id)
    {
    	$this->db->select('count(e.id) as total');
    	$this->db->from('emp_answers e');
    	$this->db->where('e.task_id', $tid);
    	$this->db->where('e.emp_id', $emp_id);
    	$this->db->where('e.answer <>','');
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$results=$query->row_array();
    	return $results;
    	// print_r($results);exit();
    }
    function get_task_details($tid)
    {
    	$this->db->select('*');
    	$this->db->from('training_model as t');
    	$this->db->where('t.id', $tid);
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$results=$query->row_array();
    	return $results;
    	// print_r($results);exit();
    }
    function add_daily_log($data)
    {
		$this->db->insert('daily_log',$data);
        return $this->db->insert_id();
    }
    function get_daily_log($emp_id,$date)
    {
    	$this->db->select('d.*');
    	$this->db->from('daily_log as d');
    	$this->db->where('d.emp_id', $emp_id);
    	$this->db->where('d.date', $date);
    	$this->db->where('d.status','0');
    	$query=$this->db->get();
    	// echo $this->db->last_query();
    	$results=$query->result_array();
    	return $results;
    	// print_r($results);exit();
    }
    function delete_log($lid)
    {
    	$this->db->set('status','1');
    	$this->db->where(' id', $lid);
    	$this->db->update('daily_log');
    	// echo $this->db->last_query();exit();
    	return 1;
    }
    function get_notification($emp_id)
    {
        $this->db->select('*');
        $this->db->from('emp_notifications as e');
        $this->db->where('e.emp_id', $emp_id);
        $this->db->where('e.status', '0');
        $query=$this->db->get();
        // echo $this->db->last_query();
        $results=$query->result_array();
        return $results;
        // print_r($results);exit();
    }
    function add_notification($data)
    {
        $this->db->insert('emp_notifications',$data);
        return $this->db->insert_id();
    }
}