<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pqr_software_training_model extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*** Check Login Credentials ***/

    function get_all_training()
    { 
        $query = $this->db
                          ->get('pqr_training');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    /*
     * Get role by id
     */
    function get_training($id)
    {
        return $this->db->get_where('pqr_training',array('id'=>$id))->row_array();
    }

    function get_All_Traning()
    {
        $query = $this->db->query('SELECT training FROM pqr_training');

        //prient_r($query)die();
        return $query->result();

        //echo 'Total Results: ' . $query->num_rows();
    }

    /*** End Login Check ***/
	
}