<?php

class Training_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
    /*
     * Get all training_model
     */
    function get_all_training()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('training_model')->result_array();
    }
     /*
     * function to add new training_model
     */
    function add_training($params)
    {
        $this->db->insert('training_model',$params);
        return $this->db->insert_id();
    }
    /*
    Get coordinater
    */
    function get_all_coordinater(){
        $query = $this->db->get('coordinater');
        if($query->num_rows() > 0){
            return $query->result_array();
        }
    }
    /*
     * function to update training_model
     */
    function update_training($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('training_model',$params);
    }
    /*
     * Get training_model by training_model
     */
    function get_training($id)
    {
        return $this->db->get_where('training_model',array('id'=>$id))->row_array();
    }
    /*
     * function to delete training_model
     */
    function delete_training($id)
    {
        return $this->db->delete('training_model',array('id'=>$id));
    }
    /*
     * function to get selected values
     */
    function get_assignby($id){
        
        $this->db->where('id',$id);
        return $this->db->get('coordinater')->result_array();
    }
}