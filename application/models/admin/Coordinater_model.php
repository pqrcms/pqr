<?php

class Coordinater_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
    /*
     * Get all get_all_coordinater
     */
    function get_all_coordinater()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('coordinater')->result_array();
    }

    function add_coordinater($params){
        $this->db->insert('coordinater',$params);
        return $this->db->insert_id();
    }

    /*
     * Get coordin by id
     */
    function get_coordinater($id)
    {
        return $this->db->get_where('coordinater',array('id'=>$id))->row_array();
    }

    /*
     * function to update coordinater
     */
    function update_coordinater($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('coordinater',$params);
    }
    /*
     * function to delete role
     */
    function delete_coordinater($id)
    {
        return $this->db->delete('coordinater',array('id'=>$id));
    }
}