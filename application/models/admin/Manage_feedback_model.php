<?php

class Manage_feedback_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
    /*
     * Get all complited_tasks
     */
    function get_all_feedback()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('emp_feedback')->result_array();
    }
}