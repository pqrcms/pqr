<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roles_model extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*** Check Login Credentials ***/

    function get_all_roles()
    { 
        $query = $this->db
                          ->get('roles');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    /*
     * Get role by id
     */
    function get_role($id)
    {
        return $this->db->get_where('roles',array('id'=>$id))->row_array();
    }

    /*
     * function to add new role
     */
    function add_role($params)
    {
        $this->db->insert('roles',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update role
     */
    function update_role($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('roles',$params);
    }

     /*
     * function to delete role
     */
    function delete_role($id)
    {
        return $this->db->delete('roles',array('id'=>$id));
    }
    
    /*** End Login Check ***/
}