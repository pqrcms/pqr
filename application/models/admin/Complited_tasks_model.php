<?php

class Complited_tasks_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
    /*
     * Get all complited_tasks
     */
    function get_all_tasks()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('emp_tasks')->result_array();
    }
}