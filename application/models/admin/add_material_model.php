<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Add_material_model extends CI_Model {
 
	function __construct() {
        parent::__construct();
    }
 
	function M_notice (){
		parent::Model();
	}
	
	// function insertNotices($arrayOfNoticeFiles){
	// 	$tableName  = "materials";
	// 	$inputArray = $arrayOfNoticeFiles;
 
	// 	$data = array(
	// 		'document_foldername'				=> $inputArray["document_foldername"],
	// 		'document_filename'					=> $inputArray["document_filename"]
	// 	);
 
	// 	$this->db->insert($tableName, $data); 
	// }

	public function add_material($data)
            {
                $this->db->insert('materials', $data);
                return $this->db->insert_id();
            }

    function get_All_Materials()
    { 
        $query = $this->db
                          ->get('materials');
        //if($query->num_rows() > 0){
            return $query->result();
        //}
    }

    /*
     * Get manage_question by id
     */
    function get_materials($id)
    {
        return $this->db->get_where('materials',array('id'=>$id))->row_array();
    }
    /*
     * function to delete manage_question
     */
    function delete_materials($id)
    {
        return $this->db->delete('materials',array('id'=>$id));
    }
 
 
}
 
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */