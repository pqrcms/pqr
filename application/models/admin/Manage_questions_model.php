<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manage_questions_model extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_All_Questions()
    { 
        $query = $this->db
                          ->get('manage_questions');
        //if($query->num_rows() > 0){
            return $query->result();
        //}
    }

    /*
     * function to add new manage_question
     */
    function add_manage_question($params)
    {
        $this->db->insert('manage_questions',$params);
        return $this->db->insert_id();
    }

    /*
     * Get all manage_question
     */
    function get_all_manage_question()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('manage_questions')->result_array();
    }

     /*
     * Get manage_question by id
     */
    function get_manage_question($id)
    {
        return $this->db->get_where('manage_questions',array('id'=>$id))->row_array();
    }

     /*
     * function to update manage_question
     */
    function update_manage_question($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('manage_questions',$params);
    }

    /*
    Get Type
    */
    function get_type(){
        $query = $this->db->get('types');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }
    /*
    Get training model
    */
    function get_tasks(){
        $query = $this->db->get('training_model');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    /*
     * function to delete manage_question
     */
    function delete_manage_question($id)
    {
        return $this->db->delete('manage_questions',array('id'=>$id));
    }
    /*** End Login Check ***/
	
}