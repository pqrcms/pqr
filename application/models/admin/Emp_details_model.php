<?php

class Emp_details_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get emp_detail by emp_id
     */
    function get_emp_detail($emp_id)
    {
        return $this->db->get_where('emp_details',array('emp_id'=>$emp_id))->row_array();
    }
        
    public function insertCSV($data)
    {
        $this->db->insert('emp_details', $data);
        return $this->db->insert_id();
    }
    /*
     * Get all emp_details
     */
    function get_all_emp_details()
    {
        $this->db->order_by('emp_id', 'desc');
        return $this->db->get('emp_details')->result_array();
    }
        
    /*
     * function to add new emp_detail
     */
    function add_emp_detail($params)
    {
        $this->db->insert('emp_details',$params);
        return $this->db->insert_id();
    }

    function deactivate($id) {
     // echo $id; die();
        $this->db->set('status', '1');
        $this->db->where('emp_id', $id);
        $this->db->update('emp_details');
        //echo $this->db->last_query(); die();
        //$this->db->set('status',1);
        //$this->db->where('emp_id !=', $id);
        //$this->db->update('emp_details');
    }
    
    /*
     * function to update emp_detail
     */
    function update_emp_detail($emp_id,$params)
    {
        $this->db->where('emp_id',$emp_id);
        return $this->db->update('emp_details',$params);
    }
    
    /*
     * function to delete emp_detail
     */
    function delete_emp_detail($emp_id)
    {
        return $this->db->delete('emp_details',array('emp_id'=>$emp_id));
    }

    /*** Check Login Credentials For Employee***/
    function check_credentials($email_id,$pass)
    { 
        $password = md5($pass);
        $query = $this->db->where('email_id',$email)
                          ->where('password',$password)
                          ->get('emp_details');
        if($query->num_rows() > 0){
        return $query->row();
        }
    }
}