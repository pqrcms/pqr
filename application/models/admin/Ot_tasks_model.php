<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ot_tasks_model extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*** Check Login Credentials ***/

    function get_all_tasks()
    { 
        $query = $this->db
                          ->get('ot_tasks');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    /*
     * Get role by id
     */
    function get_tasks($id)
    {
        return $this->db->get_where('ot_tasks',array('id'=>$id))->row_array();
    }

    /*
     * function to add new role
     */
    function add_tasks($params)
    {
        $this->db->insert('ot_tasks',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update role
     */
    function update_tasks($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('ot_tasks',$params);
    }

     /*
     * function to delete role
     */
    function delete_tasks($id)
    {
        return $this->db->delete('ot_tasks',array('id'=>$id));
    }
    
    /*** End Login Check ***/
}