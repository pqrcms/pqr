<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*** Check Login Credentials ***/

    function check_credentials($email,$pass)
    { 
        $password = md5($pass);
        $query = $this->db->where('email',$email)
                          ->where('password',$password)
                          ->get('admin_credentials');
        if($query->num_rows() > 0){
            return $query->row();
        }
    }
    
    /*** End Login Check ***/
	
}