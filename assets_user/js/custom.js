$(document).ready(function(){
     var year = (new Date).getFullYear();
     $("footer #current-year").html(year);
     // STRICT P LENGTH
     $(".megamenu li p a").each(function(){
        var len=$(this).text().length;
        if(len>10){
            $(this).text($(this).text().substr(0,50)+'...');
        }
    });
     $('.menu-active').on("click",function(){
       
        $('.menu-active').removeClass('active');
        $(this).addClass('active');
        $('.active-now').hide();
        
        var attid = $(this).attr('data-attr');
        $('#'+attid).css("display","block");
     })




});
// WINDOW ON LOAD FUNCTION
$(window).on("load",function(){
	$("#step-second-main .pdf-file").mCustomScrollbar();
    $(".megamenu").mCustomScrollbar();
});
