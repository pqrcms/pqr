/**
 * Define source data
 */
var chartData = [{
    "country": "Lithuania",
    "litres": 501.9
}, {
    "country": "Czech Republic",
    "litres": 301.9
}, {
    "country": "Ireland",
    "litres": 201.1
}, {
    "country": "Germany",
    "litres": 165.8
}, {
    "country": "Australia",
    "litres": 139.9
}, {
    "country": "Austria",
    "litres": 128.3
}, {
    "country": "UK",
    "litres": 99
}, {
    "country": "Belgium",
    "litres": 60
}, {
    "country": "The Netherlands",
    "litres": 50
}];

/**
 * Add a 50% slice
 */

var sum = 0;
for ( var x in chartData ) {
  sum += chartData[x].litres;
}
chartData.push({
  "litres": sum,
  "alpha": 0
});

/**
 * Create the chart
 */