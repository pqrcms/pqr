$(function () {

        
        var attitude = document.getElementById('attitude').value;
        attitude = parseFloat(attitude);
        var confidence = document.getElementById('confidence').value;
        confidence = parseFloat(confidence);
        var traine_feedback = document.getElementById('traine_feedback').value;
        traine_feedback = parseFloat(traine_feedback);
        //alert(feedback);
        var progress = document.getElementById('progress').value;
        progress = parseFloat(progress);
        var relationship = document.getElementById('relationship').value;
        relationship = parseFloat(relationship);
        var self_confidence = document.getElementById('self_confidence').value;
        self_confidence = parseFloat(self_confidence);

        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Graph For Each User Graph-Types Month Range '
            },
            subtitle: {
                text: 'A4S User Graph Activity'
            },
            xAxis: {
                categories: [
                    'Attitude',
                    'Confidence',
                    'Feedback',
                    'Progress',
                    'RelationShip',
                    'Self Confidence',
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0 50px">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
          credits: {
                enabled: false
              },
            series: [{
                name: 'Attitude',
                data : [attitude]
    
            }, {
                name: 'Confidence',
                data: [confidence]
    
            }, {
                name: 'Traine Feedback',
                data: [traine_feedback]
    
            }, {
                name: 'Progress',
                data: [progress]
    
            }
            , {
                name: 'RelationShip',
                data: [relationship]
    
            }
            ,{
                name: 'Self Confidence',
                data: [self_confidence]

            }]
        });
    });